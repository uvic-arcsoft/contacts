# Run all tests
test_all: test_backend test_frontend

# Run all tests for backend
test_backend:
	cd backend && tests/test-all

# Run all tests for frontend
test_frontend:
	cd frontend && npm run lint
	cd frontend && ./test.sh
