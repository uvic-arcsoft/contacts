# Set up development server
Assumptions:
* You have [Python 3.10+](https://www.python.org/downloads/) installed
* You have [Node.js](https://nodejs.org/en/download) installed

## Backend
### Retrieving submodule(s)

There following submodules are called from this repository:

- the `intest` module, which handles inspection testing (linting)

The intest module needs to be available for testing, but not for execution and
should not be included in deployments.

This should pull the submodule(s) down. From the root directory:

    git submodule init
    git submodule sync
    git submodule update --init --recursive

Though there is almost certainly a more compact syntax available to carry this
out.

### Initial setup
Install required packages:

    $ cd backend
    $ python3 -m venv venv                  # Create virtual env
    $ . venv/bin/activate                   # Activate virtual env
    $ pip install --upgrade pip             # Upgrade pip
    $ pip install -r requirements.txt       # Install neceesary packages
    $ pip install -r tests/requirements.txt # Install testing packages

From root directory, create a file with path `backend/rcdb/rcdb/.env`:

    SECRET_KEY=jdhfsa*dgfg37|fgwey!efw
    DEBUG=True
    ALLOWED_HOSTS=localhost:8000
    CSRF_TRUSTED_ORIGINS=http://localhost:3000,http://localhost:8000
    CORS_ALLOWED_ORIGINS=http://localhost:3000
    SESSION_COOKIE_DOMAIN=localhost
    CSRF_COOKIE_DOMAIN=localhost
    SECURE_COOKIES=True
    GITLAB_PRIVATE_TOKEN=your_api_token
    # Enable if want to use Postgres
    # POSTGRES_HOST=db.rcs.uvic.ca
    # POSTGRES_USER=rcdb_owner
    # POSTGRES_DB=rcdb_prod
    # POSTGRES_PASSWORD=#DB_OWNER_PW#

### Run development server
Run the development server on port **8000**:

    $ python3 rcdb/manage.py runserver
    Watching for file changes with StatReloader
    Performing system checks...

    System check identified no issues (0 silenced).
    April 15, 2024 - 14:03:18
    Django version 5.0.3, using settings 'rcdb.settings'
    Starting development server at http://127.0.0.1:8000/
    Quit the server with CONTROL-C.

### Test
The Django backend currently has 2 types of testing implemented: linting and unit testing. To run both:

    $ make test_backend

To run linting tests only:

    $ cd backend
    $ tests/linting/intest yamllint pylint

To run unit tests only

    $ cd backend/rcdb
    $ ./test.sh

## Frontend
### Run development server
Install required packages:
    
    $ cd frontend
    $ npm i

From root directory, create a file with path `frontend/.env`:

    VITE_BACKEND_URL=http://localhost:8000          # URL of your Django backend

Run the development server:

    $ npm run dev

### Test
To run component test (or unit test) for frontend, from root:
```
cd frontend
npm run coverage
```
or just simply:
```
make test_frontend
```

# Containerize app
Structure-wise, Recon needs 3 containers:
- An Nginx container: a reverse proxy to forward requests from a route to the correct container.
- A Django container: app's backend - API endpoints, talks directly to the database.
- A Solidjs container: app's frontend - UI. It's what users see when using the app.

Run the following commands to build all 3 containers, from the root directory:
    
    make build

To stand the containers up, run:

    docker-compose up -d

The app should now run at `your_host:9001`
