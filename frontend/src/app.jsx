import { MetaProvider, Title, Link } from "@solidjs/meta";
import { Router } from "@solidjs/router";
import { FileRoutes } from "@solidjs/start/router";
import "./app.css";

import { UserProvider } from "./contexts/UserContext";
import { FeedbackProvider } from "./contexts/FeedbackContext";

import Navbar from "./components/Navbar";
import Sidebar from "./components/Sidebar";
import FeedbackToast from "./components/toasts/FeedbackToast";

export default function App() {
  return (
    <Router
      root={props => (
        <>
          <MetaProvider>
            <Title>Recon</Title>
            <Link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.6.0/css/all.min.css" integrity="sha512-Kc323vGBEqzTmouAECnVceyQqyqdsSiqLQISBL29aUW4U/M7pSPA/gEUZQqv1cwx4OnYxTxve5UMg5GT6L4JJg==" crossOrigin="anonymous" referrerPolicy="no-referrer" />
          </MetaProvider>
          <UserProvider>
            <FeedbackProvider>
              <Navbar />
              <Sidebar>
                {props.children}
                {/* Feedback message display */}
                <FeedbackToast />
              </Sidebar>
            </FeedbackProvider>
          </UserProvider>
        </>
      )}
    >
      <FileRoutes />
    </Router>
  );
}
