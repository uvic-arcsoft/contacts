export const testUser = {
  'name': 'Testy McTestface',
  'username': 'testy@example.org',
  'first_name': 'Testy',
  'last_name': 'McTestface',
  'email': 'testy@example.org',
  'bio': 'Hello, I am a test user',
  'image': '/img/logo.png'
}

export const researcherJohn = {
  id: 1,
  name: "John Doe",
  email: "john.doe@example.org",
  contact_url: "https://john-doe.example.org"
}

export const researcherJane = {
  id: 2,
  name: "Jane Doe",
  email: "jane-doe@example.org",
  contact_url: "https://jane-doe.example.org"
}

export const researcherSam = {
  id: 3,
  name: "Sam Liu",
  email: "sam-liu@example.org",
  contact_url: "https://sam-liu.example.org"
}

export const projectX = {
  id: 1,
  name: "Project X",
  project_url: "https://project-x.example.org",
  description: "Project X description",
  researchers: [researcherJohn]
}

export const projectY = {
  id: 2,
  name: "Project Y",
  project_url: "https://project-y.example.org",
  description: "Project Y description",
  researchers: [researcherJane, researcherSam]
}

export const projectZ = {
  id: 3,
  name: "Project Z",
  project_url: "https://project-z.example.org",
  description: "Project Z description",
  researchers: [researcherSam]
}

export const memberTestUser = { id: 0, name: "Testy McTestface", username: "testy@example.org" }
export const memberA = { id: 1, name: "Member A", username: "member-a@example.org" }
export const memberB = { id: 2, name: "Member B", username: "member-b@example.org" }
export const memberC = { id: 3, name: "Member C", username: "member-c@example.org" }
export const memberD = { id: 4, name: "Member D", username: "member-d@example.org" }

export const tagTodo = { id: 1, name: "TODO", description: "Tasks that need to be done", last_updated: "2024-10-05T14:48:00.000Z" }
export const tagInProgress = { id: 2, name: "In progress", description: "Tasks that are in progress", last_updated: "2024-10-05T14:48:00.000Z" }
export const tagDone = { id: 3, name: "Done", description: "Tasks that are done", last_updated: "2024-10-05T14:48:00.000Z" }

export const contactA = {
  id: 1,
  name: "John Doe on Sep 03, 2024",
  synopsis: "## Meeting with John",
  synopsis_html: "<h2>Meeting with John</h2>",
  status: "active",
  created_at: "2024-09-03T14:48:00.000Z",
  last_updated: "2024-10-05T14:48:00.000Z",
  researchers: [researcherJohn],
  projects: [projectX],
  rcs_members: [memberTestUser, memberA, memberB],
  tags: [tagTodo],
}

export const contactB = {
  id: 2,
  name: "Jane Doe on Sep 03, 2024",
  synopsis: "## Discussion with Jane",
  synopsis_html: "<h2>Discussion with Jane</h2>",
  status: "initial/building",
  created_at: "2024-09-03T14:48:00.000Z",
  last_updated: "2024-10-05T14:48:00.000Z",
  researchers: [researcherJane, researcherSam],
  projects: [projectY, projectZ],
  rcs_members: [memberTestUser, memberC, memberD],
  tags: [tagInProgress, tagDone],
}

export const contactC = {
  id: 3,
  name: "Sam Liu on Sep 03, 2024",
  synopsis: "## Meeting with Sam",
  synopsis_html: "<h2>Meeting with Sam</h2>",
  status: "active",
  created_at: "2024-09-03T14:48:00.000Z",
  last_updated: "2024-10-05T14:48:00.000Z",
  researchers: [researcherSam],
  projects: [projectZ],
  rcs_members: [memberTestUser, memberA, memberC],
  tags: [tagDone],
}
