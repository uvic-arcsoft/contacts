import { http, HttpResponse } from 'msw'
import { testUser, 
          projectX, projectY, projectZ,
          researcherJohn, researcherJane, researcherSam,
          memberA, memberB, memberC, memberD,  memberTestUser,
          contactA, contactB, contactC
        } from './data'
 
export const handlers = [
  // Log user in
  http.get(`${import.meta.env.VITE_BACKEND_URL}/auth/login`, () => {
    return HttpResponse.json({
      'message': 'User logged in',
      'user': testUser
    })
  }),

  http.get(`${import.meta.env.VITE_BACKEND_URL}/api/contacts`, ({ request }) => {
    const contacts = [contactA, contactB, contactC]
    
    // Get the searched query if any
    const url = new URL(request.url)
    const query = url.searchParams.get('query')
    if (query) {
      return HttpResponse.json({
        'contacts': contacts.filter(contact => contact.synopsis.toLowerCase().replace(/\s+/g, "").includes(query.toLowerCase().replace(/\s+/g, "")))
      })
    }

    const projectIds = url.searchParams.get('projects')
    if (projectIds) {
      let matchedContacts = []
      for (const contact of contacts) {
        if (contact.projects.find(project => projectIds.includes(project.id))) {
          matchedContacts.push(contact)
        }
      }
      return HttpResponse.json({
        'contacts': matchedContacts
      })
    }

    return HttpResponse.json({
      'contacts': contacts
    })
  }),

  http.get(`${import.meta.env.VITE_BACKEND_URL}/api/contacts/:contact_id`, ({ params }) => {
    const contactId = params.contact_id;
    const contacts = [contactA, contactB, contactC]

    return HttpResponse.json({
      'contact': contacts.find(contact => contact.id == contactId)
    })
  }),

  http.get(`${import.meta.env.VITE_BACKEND_URL}/api/projects`, ({ request }) => {
    const projects = [projectX, projectY, projectZ]

    // Get the searched query if any
    const url = new URL(request.url)
    const query = url.searchParams.get('query')

    if (query) {
      return HttpResponse.json({
        'projects': projects.filter(project => project.name.toLowerCase().replace(/\s+/g, "").includes(query.toLowerCase().replace(/\s+/g, "")))
      })
    }

    return HttpResponse.json({
      'projects': projects
    })
  }),

  http.put(`${import.meta.env.VITE_BACKEND_URL}/api/projects/:project_id/update/`, () => {
    return HttpResponse.json({
      'message': 'Project updated'
    })
  }),

  http.get(`${import.meta.env.VITE_BACKEND_URL}/api/researchers`, ({ request }) => {
    const researchers = [researcherJohn, researcherJane, researcherSam]

    // Get the searched query if any
    const url = new URL(request.url)
    const query = url.searchParams.get('query')

    if (query) {
      return HttpResponse.json({
        'researchers': researchers.filter(researcher => researcher.name.toLowerCase().replace(/\s+/g, "").includes(query.toLowerCase().replace(/\s+/g, "")))
      })
    }
    
    return HttpResponse.json({
      'researchers': researchers
    })
  }),

  http.put(`${import.meta.env.VITE_BACKEND_URL}/api/researchers/:researcher_id/update/`, () => {
    return HttpResponse.json({
      'message': 'Researcher updated'
    })
  }),

  http.get(`${import.meta.env.VITE_BACKEND_URL}/auth/search`, ({ request }) => {
    const users = [memberA, memberB, memberC, memberD, memberTestUser]

    // Get the searched query if any
    const url = new URL(request.url)
    const query = url.searchParams.get('query')

    // Return users that have the query in their name or username
    if (query) {
      const matches = users.filter(user => user.name.toLowerCase().replace(/\s+/g, "").includes(query.toLowerCase().replace(/\s+/g, "")) || user.username.toLowerCase().replace(/\s+/g, "").includes(query.toLowerCase().replace(/\s+/g, "")));
      return HttpResponse.json({
        'users': matches
      })
    }
    
    return HttpResponse.json({
      'users': [memberA, memberB, memberC, memberD, memberTestUser]
    })
  }),

  http.post(`${import.meta.env.VITE_BACKEND_URL}/api/convert_markdown_to_html/`, async ({ request }) => {
    const { markdown } = await request.json()
    const contacts = [contactA, contactB, contactC]

    const matchedContact = contacts.find(contact => contact.synopsis == markdown)

    return HttpResponse.json({
      'html': matchedContact.synopsis_html
    })
  })
]
