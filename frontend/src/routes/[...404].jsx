import { HttpStatusCode } from "@solidjs/start";

export default function NotFound() {
  return (
    <main>
      <HttpStatusCode code={404} />
      <h1 class="text-4xl font-semibold">Page Not Found</h1>
      <p class="text-gray-600 mt-2">The page you are looking for does not exist. Instead, you can check out your contacted researchers <a href="/" class="link text-blue-700">here</a>.</p>
    </main>
  );
}
