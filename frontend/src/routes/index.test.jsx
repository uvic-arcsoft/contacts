import { test, expect } from "vitest";
import { render } from "@solidjs/testing-library";
import Home from "./index";

test("Home page should redirect to Researchers page where a table of contacted researchers is displayed", () => {
  const { container } = render(() => <Home />);
  expect(container.innerHTML).toContain("Redirecting to Researchers page...");
});
