import { test, expect, vi } from "vitest";
import { render } from "@solidjs/testing-library";
import { Router } from "@solidjs/router";

import { server } from "~/mocks/node";

import { UserProvider } from "~/contexts/UserContext";
import { FeedbackProvider } from "~/contexts/FeedbackContext";
import Page from "./index";

server.listen()

const wrapper = (props) => (
  <Router
    root={() => (
      <UserProvider>
        <FeedbackProvider>
          {props.children}
        </FeedbackProvider>
      </UserProvider>
    )}
   />
);

test("Test new contact create page", async() => {
  const { getByText } = render(() => <Page />, { wrapper });
  await vi.waitUntil(() => !document.querySelector('.loading'));

  // Test breadcrumbs
  const contactsLink = document.querySelector('.breadcrumbs a');
  expect(contactsLink.getAttribute("href")).toContain('/contacts');
  expect(contactsLink.textContent).toContain('Recent activities');
  expect(document.querySelector('.breadcrumbs').textContent).toContain('New');

  expect(getByText('New activity')).toBeInTheDocument();
  expect(getByText('Fill in the below to create a new activity')).toBeInTheDocument();
});
