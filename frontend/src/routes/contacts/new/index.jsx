import ContactEditor from "~/components/contacts/ContactEditor"

export default function Page() {
  return (
    <main class="pb-60">
      <div class="breadcrumbs text-sm">
        <ul>
          <li><a href="/contacts">Recent activities</a></li>
          <li>New</li>
        </ul>
      </div>
      <h1 class="text-gray-900 text-3xl font-semibold mt-4">New activity</h1>
      <p class="mt-2 text-gray-600">Fill in the below to create a new activity</p>

      <ContactEditor />
    </main>
  )
}
