import { test, expect } from "vitest";
import { render } from "@solidjs/testing-library";
import Page from "./index";

test("Contacts page should render Contacts component", () => {
  const { container } = render(() => <Page />);
  expect(container.innerHTML).toContain("Recent activities");
});
