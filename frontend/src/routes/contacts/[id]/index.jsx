import { createResource, For, Switch, Match, Show } from "solid-js";
import { useParams } from "@solidjs/router";
import "github-markdown-css/github-markdown-light.css";
import DeleteContactModal from "~/components/contacts/DeleteContactModal";

const getContact = async (id) => {
  try {
    const res = await fetch(`${import.meta.env.VITE_BACKEND_URL}/api/contacts/${id}`, {
      credentials: "include",
      headers: {
        "Content-Type": "application/json",
      },
    });
    const data = await res.json();

    if (!res.ok) {
      throw new Error(data.error);
    }
    return data.contact;
  } catch (err) {
    throw new Error(err.message);
  }
}

export default function Page(props) {
  const getId = () => {
    const { id } = !props.testContactId ? useParams() : { id: props.testContactId };
    return id;
  }
  const [contact] = createResource(() => getContact(getId()));

  return (
    <main class="pb-60">
      <div class="breadcrumbs text-sm">
        <ul>
          <li><a href="/contacts">Recent activities</a></li>
          <li>{contact.loading ? "Loading..." : contact()?.name}</li>
        </ul>
      </div>

      <Switch>
        <Match when={contact.loading}>
          <div class="flex flex-col items-center justify-center h-96">
            <p class="text-gray-500 flex gap-2 items-center text-xl p-32">
              <span class="loading loading-spinner loading-md" />
              Loading activity...
            </p>
          </div>
        </Match>
        <Match when={contact()}>
          <div class="flex flex-col-reverse md:flex-row md:justify-between md:items-center">
            <h1 class="text-gray-900 text-3xl font-semibold">{contact().name}</h1>
            <div class="flex my-2 md:my-0 gap-1 items-center">
              <a href={`/contacts/${contact().id}/edit`} class="btn btn-blue btn-sm md:btn-md"><i class="fa-solid fa-pencil" /> Edit</a>
              <div class="dropdown dropdown-end">
                <div tabIndex={0} role="button" class="btn btn-ghost btn-md m-1"><i class="fa-solid fa-ellipsis-vertical" /></div>
                <ul tabIndex={0} class="dropdown-content menu bg-base-100 rounded-box z-[1] w-52 p-2 shadow">
                  <li><a onClick={() => document.getElementById(`delete-contact-modal-${contact()?.id}`).showModal()}><i class="fa-solid fa-trash mr-1" /> Delete activity</a></li>
                </ul>
              </div>
            </div>
          </div>
          <p class="text-sm text-gray-500 mt-1">Last updated on {contact().last_updated}</p>

          <div class="my-6 md:hidden">
            <ContactInfo contact={contact} />
          </div>

          <div class="divider" />
          <section class="flex flex-row gap-4 w-full">
            {/* Synopsis */}
            <div class="markdown-body px-6 w-full md:w-3/4">
              {/* eslint-disable solid/no-innerhtml */}
              {/* Disable solid/no-innerhtml: The HTML is fetched from Gitlab API so the HTML is sanitized properly. */}
              <div innerHTML={contact().synopsis_html} />
              {/* eslint-enable solid/no-innerhtml */}
            </div>

            {/* Other details */}
            <div class="w-1/4 hidden md:block">
              <ContactInfo contact={contact} />
            </div>
            {/* Modal to delete a contact */}
            <DeleteContactModal contact={contact} onDeleteContact={() => window.location.href = "/contacts"} />
          </section>
        </Match>
      </Switch>
    </main>
  );
}

const ContactInfo = (props) => {
  return (
    <div>
      {/* Tags */}
      <div>
        <label class="text-base font-semibold">Tags:</label>
        <Show when={props.contact().tags.length > 0} fallback={
          <div class="text-gray-500 mt-2">No tags added yet</div>
        }>
          <div class="flex flex-row gap-1 flex-wrap mt-3">
            <For each={props.contact().tags}>
              {(tag) => (
                <div class="bg-primary text-gray-100 rounded-md px-2 py-1 text-sm flex items-center gap-1">
                  <span class="flex-grow">{tag?.name}{tag?.description ? ":" : ""} {tag?.description}</span>
                </div>
              )}
            </For>
          </div>
        </Show>
      </div>

      {/* Status */}
      <div class="mt-4 md:mt-6 flex gap-1 items-center">
        <label class="text-base font-semibold">Status:</label>
        <Switch>
          <Match when={props.contact().status === "initial/building"}>
            <div class="badge badge-yellow">Initial building</div>
          </Match>
          <Match when={props.contact().status === "active"}>
            <div class="badge badge-green">Active</div>
          </Match>
          <Match when={props.contact().status === "stalled"}>
            <div class="badge badge-orange">Stalled</div>
          </Match>
          <Match when={props.contact().status === "dormant"}>
            <div class="badge badge-gray">Dormant</div>
          </Match>
        </Switch>
      </div>

      {/* Researchers */}
      <div class="mt-4 md:mt-6">
        <label class="text-base font-semibold">Researchers:</label>
        <Show when={props.contact().researchers.length > 0} fallback={
          <div class="text-gray-500 mt-2">No researchers added yet</div>
        }>
          <ul id="researchers" class="mt-2 text-gray-700 list-disc ml-2">
            <For each={props.contact().researchers}>
              {(researcher) => (
                <li>{researcher.name}</li>
              )}
            </For>
          </ul>
        </Show>
      </div>

      {/* Projects */}
      <div class="mt-4 md:mt-6">
        <label class="text-base font-semibold">Projects:</label>
        <Show when={props.contact().projects.length > 0} fallback={
          <div class="text-gray-500 mt-2">No projects added yet</div>
        }>
          <ul id="projects" class="mt-2 text-gray-700 list-disc ml-2">
            <For each={props.contact().projects}>
              {(project) => (
                <li>{project.name}</li>
              )}
            </For>
          </ul>
        </Show>
      </div>

      {/* RCS members */}
      <div class="mt-4 md:mt-6">
        <label class="text-base font-semibold">RCS members:</label>
        <Show when={props.contact().rcs_members.length > 0} fallback={
          <div class="text-gray-500 mt-2">No RCS members added yet</div>
        }>
          <ul id="rcs-members" class="mt-2 text-gray-700 list-disc ml-2">
            <For each={props.contact().rcs_members}>
              {(rcs_member) => (
                <li>{rcs_member.name}</li>
              )}
            </For>
          </ul>
        </Show>
      </div>
    </div>
  )
}
