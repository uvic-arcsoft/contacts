import { createResource, Show} from "solid-js";
import { useParams } from "@solidjs/router";
import "github-markdown-css/github-markdown-light.css";
import ContactEditor from "~/components/contacts/ContactEditor"
import DeleteContactModal from "~/components/contacts/DeleteContactModal";

const getContact = async (id) => {
  try {
    const res = await fetch(`${import.meta.env.VITE_BACKEND_URL}/api/contacts/${id}`, {
      credentials: "include",
      headers: {
        "Content-Type": "application/json",
      },
    });
    const data = await res.json();

    if (!res.ok) {
      throw new Error(data.error);
    }
    return data.contact;
  } catch (err) {
    throw new Error(err.message);
  }
}

export default function Page(props) {
  const getId = () => {
    const { id } = !props.testContactId ? useParams() : { id: props.testContactId };
    return id;
  }
  const [contact] = createResource(() => getContact(getId()));

  return (
    <main class="pb-60">
      <div class="breadcrumbs text-sm">
        <ul>
          <li><a href="/contacts">Recent activities</a></li>
          <li><a href={`/contacts/${contact()?.id}`}>{contact.loading ? "Loading..." : contact()?.name}</a></li>
          <li>Edit</li>
        </ul>
      </div>

      <div class="flex flex-row justify-between items-center">
        <h1 class="text-gray-900 text-3xl font-semibold mt-4">Edit activity</h1>
        <div class="dropdown dropdown-end">
          <div tabIndex={0} role="button" class="btn btn-ghost btn-md m-1"><i class="fa-solid fa-ellipsis-vertical" /></div>
          <ul tabIndex={0} class="dropdown-content menu bg-base-100 rounded-box z-[1] w-52 p-2 shadow">
            <li><a onClick={() => document.getElementById(`delete-contact-modal-${contact()?.id}`).showModal()}><i class="fa-solid fa-trash mr-1" /> Delete activity</a></li>
          </ul>
        </div>
      </div>
      <p class="mt-2 text-gray-600">Fill in the below to edit the activity</p>

      <Show when={contact()} fallback={
        <div class="flex flex-col items-center justify-center h-96">
          <p class="text-gray-500 flex gap-2 items-center text-xl p-32">
            <span class="loading loading-spinner loading-md" />
            Loading activity...
          </p>
        </div>
      }>
        <ContactEditor contact={contact} />
        <DeleteContactModal contact={contact} onDeleteContact={() => window.location.href = "/contacts"} />
      </Show>
    </main>
  )
}
