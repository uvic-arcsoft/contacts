import { test, expect, vi } from "vitest";
import { render } from "@solidjs/testing-library";
import { Router } from "@solidjs/router";

import { server } from "~/mocks/node";

import { contactA } from "~/mocks/data";
import { UserProvider } from "~/contexts/UserContext";
import { FeedbackProvider } from "~/contexts/FeedbackContext";
import Page from "./index";

server.listen()

const wrapper = (props) => (
  <Router
    root={() => (
      <UserProvider>
        <FeedbackProvider>
          {props.children}
        </FeedbackProvider>
      </UserProvider>
    )}
   />
);

test("Test individual contact edit page", async () => {
  render(() => <Page testContactId={contactA.id} />, { wrapper });
  await vi.waitUntil(() => !document.querySelector('.loading'));

  // Test breadcrumbs
  const links = Array.from(document.querySelectorAll('.breadcrumbs a'));
  expect(links.length).toBe(2);
  expect(links[0].getAttribute("href")).toContain('/contacts');
  expect(links[0].textContent).toContain('Recent activities');
  expect(links[1].getAttribute("href")).toContain(`/contacts/${contactA.id}`);
  expect(links[1].textContent).toContain(contactA.name);

  // Contact details are tested in ContactEditor and DeleteContactModal
});
