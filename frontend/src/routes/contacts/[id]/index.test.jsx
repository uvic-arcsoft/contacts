import { test, expect, vi } from "vitest";
import { render } from "@solidjs/testing-library";
import { Router } from "@solidjs/router";

import { server } from "~/mocks/node";

import { contactA } from "~/mocks/data";
import { UserProvider } from "~/contexts/UserContext";
import { FeedbackProvider } from "~/contexts/FeedbackContext";
import Page from "./index";

server.listen()

const wrapper = (props) => (
  <Router
    root={() => (
      <UserProvider>
        <FeedbackProvider>
          {props.children}
        </FeedbackProvider>
      </UserProvider>
    )}
   />
);

test("Test an individual contact page. Should render the contact details", async() => {
  render(() => <Page testContactId={contactA.id}/>, { wrapper });
  await vi.waitUntil(() => document.querySelector('h1')?.textContent.includes(contactA.name));
  
  // Test breadcrumbs
  const recentActivitiesLink = document.querySelector('.breadcrumbs a');
  expect(recentActivitiesLink.getAttribute("href")).toContain('/contacts');
  expect(recentActivitiesLink.textContent).toContain('Recent activities');
  expect(document.querySelector('.breadcrumbs').textContent).toContain(contactA.name);

  // Test contact last updated
  expect(document.querySelector('p.text-sm.text-gray-500.mt-1').textContent).toContain(`Last updated on ${contactA.last_updated}`);
  
  // Test contact synopsis
  expect(document.querySelector('.markdown-body').innerHTML).toContain(contactA.synopsis_html);
  
  // Test contact edit button
  expect(document.querySelector(`a[href="/contacts/${contactA.id}/edit"]`).textContent).toContain('Edit');

  // Test contact tags
  const tags = Array.from(document.querySelectorAll('.hidden .bg-primary.text-gray-100.rounded-md'));
  expect(tags.length).toBe(contactA.tags.length);
  for (let i = 0; i < tags.length; i++) {
    expect(tags[i].textContent).toContain(contactA.tags[i].name);
    expect(tags[i].textContent).toContain(contactA.tags[i].description);
  }

  // Test contact status
  const status = document.querySelector('.badge');
  expect(status.textContent.toLowerCase().replace(/\s+/g, "")).toContain(contactA.status.toLowerCase().replace(/\s+/g, ""));

  // Test contact researchers
  const researchers = Array.from(document.querySelectorAll('.hidden #researchers li'));
  expect(researchers.length).toBe(contactA.researchers.length);
  for (let i = 0; i < researchers.length; i++) {
    expect(researchers[i].textContent).toContain(contactA.researchers[i].name);
  }

  // Test contact projects
  const projects = Array.from(document.querySelectorAll('.hidden #projects li'));
  expect(projects.length).toBe(contactA.projects.length);
  for (let i = 0; i < projects.length; i++) {
    expect(projects[i].textContent).toContain(contactA.projects[i].name);
  }

  // Test contact RCS members
  const rcsMembers = Array.from(document.querySelectorAll('.hidden #rcs-members li'));
  expect(rcsMembers.length).toBe(contactA.rcs_members.length);
  for (let i = 0; i < rcsMembers.length; i++) {
    expect(rcsMembers[i].textContent).toContain(contactA.rcs_members[i].name);
  }
});
