import { test, expect } from "vitest";
import { render } from "@solidjs/testing-library";
import NotFound from "./[...404]";

test("Not found page should display a 404 status code and a message", () => {
  const { container } = render(() => <NotFound />);
  expect(container.innerHTML).toContain("Page Not Found");
  expect(container.innerHTML).toContain("The page you are looking for does not exist. Instead, you can check out your contacted researchers <a href=\"/\" class=\"link text-blue-700\">here</a>.");
});
