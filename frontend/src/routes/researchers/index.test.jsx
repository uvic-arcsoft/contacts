import { test, expect } from "vitest";
import { render } from "@solidjs/testing-library";
import Page from "./index";

test("Researchers page should render Researchers component", () => {
  const { container } = render(() => <Page />);
  expect(container.innerHTML).toContain("Researchers");
});
