import { test, expect } from "vitest";
import { render } from "@solidjs/testing-library";
import Page from "./index";

test("Projects page should render Projects component", () => {
  const { container } = render(() => <Page />);
  expect(container.innerHTML).toContain("Projects");
});
