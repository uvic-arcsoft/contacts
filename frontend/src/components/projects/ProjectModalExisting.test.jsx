import { test, expect, vi } from "vitest";
import { render } from "@solidjs/testing-library"
import userEvent from "@testing-library/user-event";

import { server } from "~/mocks/node";

import ProjectModal from "./ProjectModal";
import { FeedbackProvider } from "~/contexts/FeedbackContext";

import { contactB, projectY } from "~/mocks/data";

const user = userEvent.setup();
server.listen();

const wrapper = (props) => (
  <FeedbackProvider>
    {props.children}
  </FeedbackProvider>
);

const project = () => projectY;
test("render project modal with an existing project", async () => {
  const { getByText } = render(
  () => <ProjectModal
      onSaveProject={() => {}}
      project={project}
      initialActiveTab={() => "info"}
      disableResearcherCreation={true} />,
  { wrapper });
  
  // Wait for the modal to load project
  // The cue is the modal has project's name as the heading
  await vi.waitUntil(() => document.querySelector('h3').textContent.includes(project().name));
  expect(getByText(project().name)).toBeInTheDocument();

  // The project modal should have a side bar
  expect(document.getElementById('project-modal').innerHTML).toContain('<h5 class="text-sm font-semibold text-gray-500">View</h5>');
  expect(document.getElementById('project-modal').innerHTML).toContain('<h5 class="text-sm font-semibold text-gray-500 mt-5">Actions</h5>');

  // Check information of the info tab
  const nameInput = document.querySelector('input[name="name"]');
  expect(nameInput.value).toBe(project().name);

  const projectUrlInput = document.querySelector('input[name="url"]');
  expect(projectUrlInput.value).toBe(project().project_url);

  const researcherTags = document.querySelectorAll('.tag');
  expect(researcherTags.length).toBe(2);

  const descriptionInput = document.querySelector('textarea[name="description"]');
  expect(descriptionInput.value).toBe(project().description);

  // Check contacts of the contacts tab
  await user.click(getByText('Activities'));
  await vi.waitUntil(() => document.body.textContent.includes(contactB.name));

  // Test saving the project info
  await user.click(getByText('Info'));
  await vi.waitUntil(() => document.querySelector('form.info'));
  await user.click(getByText('Save'));
});
