import { createResource, Switch, Match, Show, createSignal } from "solid-js";
import ProjectsTable from "./ProjectsTable";
import ProjectModal from "./ProjectModal";
import DeleteProjectModal from "./DeleteProjectModal";

const getProjects = async () => {
  try {
    const res = await fetch(`${import.meta.env.VITE_BACKEND_URL}/api/projects/`, {
      credentials: "include",
      headers: {
        "Content-Type": "application/json",
      },
    });
    const data = await res.json();

    if (!res.ok) {
      throw new Error(data.error);
    }
    return data.projects;
  } catch (err) {
    throw new Error(err.message);
  }
}

export default function Projects() {
  const [projects, { mutate: setProjects }] = createResource(getProjects);
  const [selectedProject, setSelectedProject] = createSignal(null);
  const [modalActiveTab, setModalActiveTab] = createSignal("info");

  const addProject = (newProject) => {
    setProjects([newProject, ...projects()]);
  }

  const updateProject = (updatedProject) => {
    const updatedProjects = projects().map(project => {
      if (project.id === updatedProject.id) {
        return updatedProject;
      }
      return project;
    });
    setProjects(updatedProjects);
  }

  const deleteProject = (deletedProject) => {
    const updatedProjects = projects().filter(project => project.id !== deletedProject.id);
    setSelectedProject(null);
    setProjects(updatedProjects);
  }

  return (
    <main>
      <div class="flex flex-row justify-between">
        <h1 class="text-gray-900 text-xl md:text-3xl font-semibold">Projects</h1>
        <button onClick={()=> {
          setSelectedProject(null);
          document.getElementById('project-modal').showModal();
        }} class="btn btn-md btn-blue"><i class="fa-solid fa-plus" /> New Project</button>
      </div>
      <p class="text-gray-700 text-sm">Hover on the table header to sort and filter projects</p>

      <Switch>
        <Match when={projects.loading}>
          <div class="flex flex-col items-center justify-center h-96">
            <p class="text-gray-500 flex gap-2 items-center text-xl p-32">
              <span class="loading loading-spinner loading-md" />
              Loading projects...
            </p>
          </div>
        </Match>
        <Match when={projects.error}>
          <div class="flex flex-col items-center justify-center h-96">
            <p class="text-red-500">{projects.error.message}</p>
          </div>
        </Match>
        <Match when={projects()?.length === 0}>
          <div class="flex flex-col items-center justify-center h-96">
            <p class="text-gray-500">No projects found. Click on the <strong>+ New project</strong> button to create one.</p>
          </div>
        </Match>
        <Match when={projects().length > 0}>
          <section class="flex flex-col gap-8 mt-4">
            <ProjectsTable projects={projects} setSelectedProject={setSelectedProject} setModalActiveTab={setModalActiveTab}/>
            <DeleteProjectModal project={selectedProject} onDeleteProject={deleteProject}/>
          </section>
        </Match>
      </Switch>

      <Show when={!projects.loading && !projects.error}>
        <Show when={selectedProject()} fallback={
          <ProjectModal onSaveProject={(r) => addProject(r)}/>
        }>
          <ProjectModal project={selectedProject} onSaveProject={(r) => updateProject(r)} initialActiveTab={modalActiveTab}/>
        </Show>
      </Show>
    </main>
  )
}
