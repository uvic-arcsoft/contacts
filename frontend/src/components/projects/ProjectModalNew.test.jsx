import { test, expect } from "vitest";
import { render } from "@solidjs/testing-library"

import ProjectModal from "./ProjectModal";
import { FeedbackProvider } from "~/contexts/FeedbackContext";

const wrapper = (props) => (
  <FeedbackProvider>
      {props.children}
  </FeedbackProvider>
);

test("render project creation modal", async () => {
  // Test project modal for project creation
  const { getByText } = render(() => <ProjectModal />, { wrapper });
  expect(getByText('New project')).toBeInTheDocument();

  // The project creation modal should not have a side bar
  expect(document.getElementById('project-modal').innerHTML).not.toContain('<h5 class="text-sm font-semibold text-gray-500">View</h5>');
  expect(document.getElementById('project-modal').innerHTML).not.toContain('<h5 class="text-sm font-semibold text-gray-500">Actions</h5>');

  // The form should be empty
  const nameInput = document.querySelector('input[name="name"]');
  expect(nameInput.value).toBe('');

  const projectUrlInput = document.querySelector('input[name="url"]');
  expect(projectUrlInput.value).toBe('');

  const researcherTags = document.querySelectorAll('.tag');
  expect(researcherTags.length).toBe(0);

  const descriptionInput = document.querySelector('textarea[name="description"]');
  expect(descriptionInput.value).toBe('');
});

