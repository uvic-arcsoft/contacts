import { useContext } from "solid-js";
import { getCookie } from "~/helper";
import FeedbackContext from "~/contexts/FeedbackContext";

export default function DeleteProjectModal(props) {
  const { setType, setMessage } = useContext(FeedbackContext);
  
  // Send a DELETE request to the server to delete the project
  const deleteProject = async () => {
    try {
      const res = await fetch(`${import.meta.env.VITE_BACKEND_URL}/api/projects/${props.project()?.id}/delete/`, {
        method: "DELETE",
        credentials: "include",
        headers: {
          "Content-Type": "application/json",
          "X-CSRFToken": getCookie("csrftoken"),
        },
      });
      const data = await res.json();

      if (!res.ok) {
        setType("error");
        setMessage(data.error);
        return;
      }
      setType("success");
      setMessage(data.message);
      props.onDeleteProject(props.project());
      document.getElementById(`delete-project-modal-${props.project()?.id}`).close();
    } catch (err) {
      setType("error");
      setMessage(err.message);
    }
  };
  
  return (
    <dialog id={`delete-project-modal-${props.project()?.id}`} class="modal">
      <div class="modal-box">
        <h3 class="font-bold text-lg">Delete {props.project()?.name}</h3>
        <p class="py-4">
          Are you sure you want to delete <strong>{props.project()?.name}</strong> project?
        </p>
        <div class="modal-action">
          <form method="dialog">
            {/* if there is a button in form, it will close the modal */}
            <button type="button" class="btn btn-md btn-red mr-1" onClick={deleteProject}>Yes</button>
            <button class="btn btn-md btn-blue">No</button>
          </form>
        </div>
      </div>
      <form method="dialog" class="modal-backdrop">
        <button>close</button>
      </form>
    </dialog>
  );
}
