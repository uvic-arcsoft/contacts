import { test, expect } from "vitest";
import { render } from "@solidjs/testing-library"
import DeleteProjectModal from "./DeleteProjectModal";
import { FeedbackProvider } from "~/contexts/FeedbackContext";
import { projectX } from "~/mocks/data";

const wrapper = (props) => (
  <FeedbackProvider>
    {props.children}
  </FeedbackProvider>
);

const project = () => projectX;

test("render project delete modal", async () => {
  const { getByText } = render(() => <DeleteProjectModal project={project} />, { wrapper });
  
  // Test modal content
  expect(getByText(`Delete ${projectX.name}`)).toBeInTheDocument();
  expect(document.querySelector('p').innerHTML).toContain(`Are you sure you want to delete <strong>${project().name}</strong> project?`);
  expect(document.querySelector('button.btn-red').textContent).toBe('Yes');
  expect(document.querySelector('button.btn-blue').textContent).toBe('No');
});
