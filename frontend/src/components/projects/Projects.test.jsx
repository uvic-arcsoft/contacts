import { test, expect, vi } from "vitest";
import { render } from "@solidjs/testing-library"

import Projects from "./Projects";
import { FeedbackProvider } from "~/contexts/FeedbackContext";

import { server } from "~/mocks/node";

server.listen()

const wrapper = (props) => (
  <FeedbackProvider>
    {props.children}
  </FeedbackProvider>
);

test("render projects page", async () => {
  const { getByText } = render(() => <Projects />, { wrapper });

  // Test loading state
  expect(getByText('Loading projects...')).toBeInTheDocument();
  
  // Wait for projects to load
  await vi.waitUntil(() => Array.from(document.querySelectorAll('div[role="row"]')).slice(1).length > 0);
  expect(getByText('Projects')).toBeInTheDocument();

  // There should be 3 projects
  const rows = Array.from(document.querySelectorAll('div[role="row"]')).slice(1);
  expect(rows.length).toBe(3);

  // NOTE: The project contents are tested in ProjectsTable.test.jsx
});
