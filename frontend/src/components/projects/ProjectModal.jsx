import { useContext, createSignal, createEffect, createResource, Show, Switch, For, Match, mergeProps } from "solid-js";
import { getCookie } from "~/helper";
import FeedbackContext from "~/contexts/FeedbackContext";
import ResearchersSearchBar from "../searchbars/ResearchersSearchBar";
import ResearcherModal from "../researchers/ResearcherModal";

export default function ProjectModal(_props) {
  // This signal is used purely for display purposes
  const props = mergeProps({ project: null, initialActiveTab: null, disableResearcherCreation: false }, _props);
  const [ name, setName ] = createSignal("");
  const [activeTab, setActiveTab] = createSignal("info");

  createEffect(() => {
    if (props.project) {
      setName(props.project()?.name);
    }
  });

  createEffect(() => {
    if (props.initialActiveTab) {
      setActiveTab(props.initialActiveTab());
    }
  });

  return (
    <dialog id="project-modal" class="modal">
      <div class={`modal-box w-full ${props.project ? "md:w-4/5 md:max-w-[1000px]" : "md:w-2/3"} md:max-w-[800px]`}>

        {/* Close modal button */}
        <form method="dialog">
          <button class="btn btn-sm btn-circle btn-ghost absolute right-2 top-2">✕</button>
        </form>

        <h3 class="font-bold text-xl">{props.project ? name() : "New project" }</h3>
        <p class="mt-1 text-gray-500 text-sm">{props.project ? "View and update project information" : "Add a new project to your activity list"}</p>

        <div class="flex gap-5">
          <div class="grow">
            <div class={activeTab() === "info" ? "" : "hidden"}>
              <InfoTab project={props.project} onSaveProject={props.onSaveProject} setName={setName} disableResearcherCreation={props.disableResearcherCreation}/>
            </div>

            <Show when={props.project}>
              <div class={activeTab() === "contacts" ? "" : "hidden"}>
                <ContactsTab project={props.project} />
              </div>
            </Show>
          </div>

          <Show when={props.project}>
            <div>
              {/* View */}
              <h5 class="text-sm font-semibold text-gray-500">View</h5>
              <div class="flex flex-col gap-2 mt-2 w-fit min-w-[80px] md:min-w-[150px]">
                <button class={`btn btn-sm md:btn-md justify-start ${activeTab() === "info" ? "btn-active" : ""}`} onClick={() => setActiveTab("info")}><i class="fa-solid fa-user mr-1" /> Info</button>
                <button class={`btn btn-md justify-start ${activeTab() === "contacts" ? "btn-active" : ""}`} onClick={() => setActiveTab("contacts")}><i class="fa-solid fa-address-book" />Activities</button>
              </div>

              {/* Actions */}
              <h5 class="text-sm font-semibold text-gray-500 mt-5">Actions</h5>
              <div class="flex flex-col gap-2 mt-2 w-fit min-w-[80px] md:min-w-[150px]">
                <button class="btn btn-sm md:btn-md justify-start" onClick={() => {
                  window.localStorage.setItem("new-activity-projects", JSON.stringify([props.project()]));
                  window.location.href = "/contacts/new";
                }}><i class="fa-solid fa-plus mr-1" /> Add activity</button>
              </div>
            </div>
          </Show>
        </div>

      </div>
      <form method="dialog" class="modal-backdrop">
        <button>close</button>
      </form>
    </dialog>
  )
}

const InfoTab = (props) => {
  const { setType, setMessage } = useContext(FeedbackContext);
  const [selectedResearchers, setSelectedResearchers] = createSignal([]);

  createEffect(() => {
    if (props.project) {
      setSelectedResearchers(props.project()?.researchers)
    }
  })

  // Add a new project
  const saveProject = async (e) => {
    e.preventDefault();
    const newProject = {
      name: e.target.name.value,
      project_url: e.target.url?.value,
      researchers: selectedResearchers().map(r => r.id),
      description: e.target.description?.value
    };

    // Send a POST request to the backend
    try {
      const url = props.project ? `${import.meta.env.VITE_BACKEND_URL}/api/projects/${props.project()?.id}/update/` : `${import.meta.env.VITE_BACKEND_URL}/api/projects/create/`;
      const method = props.project ? "PUT" : "POST";
      const res = await fetch(url, {
        method: method,
        credentials: "include",
        headers: {
          "Content-Type": "application/json",
          "X-CSRFToken": getCookie("csrftoken"),
        },
        body: JSON.stringify(newProject),
      });

      const data = await res.json();
      
      if (res.ok) {
        setType("success");
        setMessage(data.message);

        // Render the new project on the table
        props.onSaveProject({
          id: data.project_id,
          ...newProject,
          researchers: selectedResearchers()
        });

        // Clear the form
        e.target.name.value = "";
        e.target.url.value = "";
        setSelectedResearchers([]);
        e.target.description.value = "";

        // Close the modal
        document.getElementById("project-modal").close();
      } else {
        setType("error");
        setMessage(data.error);
      }
    } catch (err) {
      setType("error");
      setMessage(err.message);
    }
  };

  return (
    <form class="info" onSubmit={saveProject}>
      {/* Name */}
      <div class="mt-5">
        <div><label class="text-base font-semibold">Name <span class="text-red-500">*</span></label></div>
        <input 
        type="text"
        name="name"
        value={props.project ? props.project()?.name : ""}
        placeholder="Project's name"
        class="input input-bordered w-full mt-2"
        required
        onInput={e => props.setName(e.target.value)}/>
      </div>

      {/* Project URL */}
      <div class="mt-5">
        <div><label class="text-base font-semibold">URL</label></div>
        <input
          type="url"
          name="url"
          value={props.project ? props.project()?.project_url : ""}
          placeholder="Project's URL"
          class="input input-bordered w-full mt-2" />
        <p class="text-sm text-gray-500 mt-2">Must start with either <strong>https</strong> or <strong>http</strong></p>
      </div>

      {/* Researchers */}
      <div class="mt-5">
        <label class="text-base font-semibold">Researchers</label>
        <Show when={!props.disableResearcherCreation}>
          <button type="button" class="btn btn-outline btn-sm btn-blue ml-2" onClick={()=>document.getElementById('researcher-modal').showModal()}>
            <i class="fa-solid fa-plus" />
            New researcher
          </button>
          <ResearcherModal onSaveResearcher={(r) => setSelectedResearchers([...selectedResearchers(), r])}/>
        </Show>
      </div>
      <div class="mt-3">
        <ResearchersSearchBar onSelect={(researcher) => {
          if (!selectedResearchers().find(r => r.id === researcher.id)) {
            setSelectedResearchers([...selectedResearchers(), researcher])
          }
        }}/>
        
        <div class="flex flex-row gap-2 flex-wrap mt-3">
          <For each={selectedResearchers()}>
            {(researcher) => (
              <div class="tag flex flex-row gap-2">
                <a class="link">{researcher?.name}</a>
                <button type="button" onClick={() => setSelectedResearchers(selectedResearchers().filter(r => r !== researcher))}>
                  <i class="fa-solid fa-xmark text-gray-400 hover:text-gray-700" />
                </button>
              </div>
            )}
          </For>
        </div>
      </div>

      {/* Description */}
      <div class="mt-5">
        <div><label class="text-base font-semibold">Description <span class="text-red-500">*</span></label></div>
        <textarea
        name="description"
        value={props.project ? props.project()?.description : ""}
        placeholder="What is the project about?"
        rows={5}
        class="textarea textarea-bordered w-full mt-2"
        required />
      </div>

      <div class="modal-action justify-start">
        <button type="submit" class="btn btn-blue">{props.project ? "Save" : "Add project"}</button>
      </div>
    </form>
  )
}

const ContactsTab = (props) => {
  const getContactsForProject = async (project) => {
    if (!project) return [];
    try {
      const res = await fetch(`${import.meta.env.VITE_BACKEND_URL}/api/contacts?projects=${project.id}`, {
        credentials: "include",
        headers: {
          "Content-Type": "application/json",
        },
      });
      const data = await res.json();
      
      if (!res.ok) {
        throw new Error(data.error);
      }
      return data.contacts;
    } catch (err) {
      throw new Error(err.message);
    }
  }
  
  {/* eslint-disable solid/reactivity */}
  {/* TODO: Figure out how to pass the project prop to the createResource function
  to stil achieve reactivity and avoid the eslint-disable comment */}
  const [contacts] = createResource(props.project, getContactsForProject);
  {/* eslint-enable solid/reactivity */}

  return (
    <Switch>
      <Match when={contacts.loading}>
        <div class="flex flex-col items-center justify-center h-96">
          <p class="text-gray-500 flex gap-2 items-center text-base p-32">
            <span class="loading loading-spinner loading-md" />
            Loading activities that involve {props.project()?.name}...
          </p>
        </div>
      </Match>
      <Match when={contacts.error}>
        <div class="flex flex-col items-center justify-center h-96">
          <p class="text-red-500">{contacts.error.message}</p>
        </div>
      </Match>
      <Match when={contacts()?.length === 0}>
        <div class="flex flex-col items-center justify-center h-96">
          <p class="text-gray-500">No activities found that involve <strong>{props.project()?.name}.</strong></p>
        </div>
      </Match>
      <Match when={contacts().length > 0}>
        <div class="mt-2">
          <For each={contacts()}>
            {contact => (
              <div class="card bg-base-100 w-full shadow-xl border border-gray-200 my-2">
                <div class="card-body">
                  <a href={`/contacts/${contact.id}`} class="card-title link text-lg">{contact.name}</a>
                  <div class="text-sm flex flex-col gap-1">
                    <p>
                      <strong>Status: </strong>
                      <Switch>
                        <Match when={contact.status === "initial/building"}>
                          <div class="badge badge-yellow">Initial building</div>
                        </Match>
                        <Match when={contact.status === "active"}>
                          <div class="badge badge-green">Active</div>
                        </Match>
                        <Match when={contact.status === "stalled"}>
                          <div class="badge badge-orange">Stalled</div>
                        </Match>
                        <Match when={contact.status === "dormant"}>
                          <div class="badge badge-gray">Dormant</div>
                        </Match>
                      </Switch>
                    </p>
                    <p>
                      <strong>Researchers: </strong>
                      <span>{contact?.researchers?.map(r => r.name).join(', ')}</span>
                    </p>
                    <p>
                      <strong>Projects: </strong>
                      <span>{contact?.projects?.map(p => p.name).join(', ')}</span>
                    </p>
                  </div>
                </div>
              </div>
            )}
          </For>
        </div>
      </Match>
    </Switch>
  )
}
