import { test, expect, vi } from "vitest";
import { render } from "@solidjs/testing-library"
import ProjectsTable from "./ProjectsTable";
import { projectX, projectY, projectZ } from "~/mocks/data";

const projects = () => {
  return [projectX, projectY, projectZ];
};

test("renders projects table", async () => {
  render(() => <ProjectsTable projects={projects} />);

  // Wait for the rows to be rendered
  await vi.waitUntil(() => document.querySelector('div[role="row"]'))
  
  // Remove the first header row
  const rows = Array.from(document.querySelectorAll('div[role="row"]')).slice(1);
  for (let i = 0; i < rows.length; i++) {
    const row = rows[i];
    const project = projects()[i];
    
    // Test project name rendering
    const nameCell = row.querySelector('div[role="gridcell"][col-id="name"]').querySelector('button');
    expect(nameCell.textContent).toBe(project.name);

    // Test project URL rendering
    const projectUrlCell = row.querySelector('div[role="gridcell"][col-id="projectUrl"]').querySelector('a');
    expect(projectUrlCell.getAttribute("href")).toBe(project.project_url);
    expect(projectUrlCell.textContent).toBe(project.project_url);

    // Test project researchers rendering
    const researchersCell = row.querySelector('div[role="gridcell"][col-id="researchers"]').querySelector('ul');
    expect(researchersCell.children.length).toBe(project.researchers.length);
    for (let j = 0; j < project.researchers.length; j++) {
      const researcher = project.researchers[j];
      const researcherLi = researchersCell.children[j];
      expect(researcherLi.tagName).toBe('LI');
      expect(researcherLi.textContent).toBe(researcher.name);
    }

    // Test project description rendering
    const descriptionCell = row.querySelector('div[role="gridcell"][col-id="description"]');
    expect(descriptionCell.textContent).toContain(project.description);
  }
});
