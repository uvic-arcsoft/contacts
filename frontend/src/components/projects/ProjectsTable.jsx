import { createEffect, For } from 'solid-js';

import AgGridSolid from 'ag-grid-solid';
import "ag-grid-community/styles/ag-grid.css"; // Mandatory CSS required by the grid
import "ag-grid-community/styles/ag-theme-quartz.css"; // Optional Theme applied to the grid

const ProjectName = (params) => {
  return (
    <button onClick={() => {
      params.data.onSelect("info");
      document.getElementById('project-modal').showModal();
    }} class='link font-semibold'>{params.value}</button>
  )
}

const ProjectUrl = (params) => {
  return <a href={params.value} target='_blank' class='link font-semibold'>{params.value}</a>
}

const ProjectResearchers = (params) => {
  return (
    <ul class='list-disc py-4'>
      <For each={JSON.parse(params.value)}>
        {r => (
        <li>{r.name}</li>
        )}
      </For>
    </ul>
  )
}

const ProjectDescription = (params) => {
  return <p class='text-sm min-h-32 w-[400px] px-4 py-6 whitespace-normal text-wrap flex items-center leading-relaxed'>{params.value}</p>
}

const ProjectActions = (params) => {
  return (
    <div class="flex gap-5 items-center">
      <button onClick={() => {
        params.data.onSelect("info");
        document.getElementById('project-modal').showModal();
      }}><i class="fa-solid fa-pen-to-square text-blue-500 hover:text-blue-700" /></button>
      <button onClick={() => {
        params.data.onSelect();
        document.getElementById(`delete-project-modal-${params.data.id}`).showModal();
      }}><i class="fa-solid fa-trash-can text-red-500 hover:text-red-700" /></button>
      <button onClick={() => {
        params.data.onSelect("contacts");
        document.getElementById('project-modal').showModal();
      }}><i class="fa-solid fa-list-check text-green-700 hover:text-green-800" /></button>
    </div>
  )
}

{/* eslint-disable solid/reactivity */}
{/* Disable solid/reactivity: AGGrid table doesn't render properly with rowData as a signal */}
export default function ProjectsTable(props) {
  const setSelectedProject = props.setSelectedProject;
  const setModalActiveTab = props.setModalActiveTab;
  let gridRef;

  const columnDefs = [
    { headerName: "Name", field: "name", flex: 1, cellRenderer: ProjectName },
    { headerName: "Project URL", field: "projectUrl", cellRenderer: ProjectUrl },
    { headerName: "Researchers", field: "researchers", cellRenderer: ProjectResearchers, autoHeight: true },
    { headerName: "Description", field: "description", cellRenderer: ProjectDescription, autoHeight: true },
    { headerName: "Actions", field: "id", cellRenderer: ProjectActions },
  ]

  let rowData = props.projects().map(project => ({
    id: project.id,
    name: project.name,
    projectUrl: project.project_url,
    researchers: JSON.stringify(project.researchers),
    description: project.description,
    onSelect: (tab="info") => {
      setSelectedProject(project);
      setModalActiveTab(tab);
    },
  }));

  // Update projects table when there is any change to the projects
  createEffect(() => {
    rowData = props.projects().map(project => ({
      id: project.id,
      name: project.name,
      projectUrl: project.project_url,
      researchers: JSON.stringify(project.researchers),
      description: project.description,
      onSelect: (tab="info") => {
        setSelectedProject(project);
        setModalActiveTab(tab);
      },
    }));
    gridRef?.api?.setRowData(rowData);
  });

  // Allow filtering sorting on all columns
  const defaultColDef = {
    filter: true,
    sortable: true,
  };

  // Scale each column to fit all cell contents
  const autoSizeStrategy = {
    type: 'fitCellContents'
  };

  return (
    <div class="ag-theme-quartz h-[70vh] min-w-[700px]">
      <AgGridSolid
        ref={gridRef}
        rowData={rowData}
        columnDefs={columnDefs}
        defaultColDef={defaultColDef}
        rowHeight={50}
        pagination={true}
        paginationPageSize={10}
        paginationPageSizeSelector={[10, 25, 50]}
        autoSizeStrategy={autoSizeStrategy}
      />
    </div>
  )
}
{/* eslint-enable solid/reactivity */}
