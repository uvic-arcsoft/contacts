import { createSignal, Show, For, mergeProps } from "solid-js";
import clickOutside from "~/directives/clickOutside";

export default function ResearchersSearchBar(_props) {
  const props = mergeProps({ onSelect: () => {} }, _props);
  const [searchedResearchers, setSearchedResearchers] = createSignal([]);
  const [showSearchedResearchers, setShowSearchedResearchers] = createSignal(false);
  const [error, setError] = createSignal(false);

  const searchResearchers = async(e) => {
    const query = e.target.value;
    let researchers = [];
    
    try {
      // Make an API call to the backend to get matched researchers
      const res = await fetch(`${import.meta.env.VITE_BACKEND_URL}/api/researchers?query=${query}`, {
        credentials: "include",
        headers: {
          "Content-Type": "application/json",
        },
      });

      const data = await res.json();

      if (res.ok) {
        setError(false);
        researchers = data.researchers;
      } else {
        // Display error message
        setError(true);
      }
    } catch (err) {
      console.error(err);
      setError(true);
    }

    // Display researchers
    setSearchedResearchers(researchers);
    setShowSearchedResearchers(true);
  }

  return (
    <div class="relative w-full" use:clickOutside={() => setShowSearchedResearchers(false)}>
      <label class="input input-bordered flex items-center gap-2">
        <input type="text" class="grow" placeholder="Search researchers" onClick={searchResearchers} onInput={searchResearchers}/>
        <i class="fa-solid fa-magnifying-glass" />
      </label>

      <Show when={showSearchedResearchers()}>
        <ul class="menu absolute top-full left-0 w-full bg-base-100 rounded-md shadow-lg border border-gray-200 z-30 block max-h-[250px] overflow-y-scroll">
          <Show when={searchedResearchers().length === 0}>
            <li class="menu-item"><a>No researchers found</a></li>
          </Show>
          <Show when={error()}>
            <li class="menu-item"><a>Error getting researchers</a></li>
          </Show>
          <For each={searchedResearchers()}>
            {(researcher) => (
              <li class="menu-item" onClick={() => {
                props.onSelect(researcher);
                setShowSearchedResearchers(false);
              }}><a>{researcher.name}</a></li>
            )}
          </For>
        </ul>
      </Show>
    </div>
  )
}
