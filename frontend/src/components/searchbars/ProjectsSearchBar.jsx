import { createSignal, Show, For, mergeProps } from "solid-js";
import clickOutside from "~/directives/clickOutside";

export default function ProjectsSearchBar(_props) {
  const props = mergeProps({ onSelect: () => {} }, _props);
  const [searchedProjects, setSearchedProjects] = createSignal([]);
  const [showSearchedProjects, setShowSearchedProjects] = createSignal(false);
  const [error, setError] = createSignal(false);

  const searchProjects = async(e) => {
    const query = e.target.value;
    let projects = [];

    try {
      // Make an API call to the backend to get matched projects
      const res = await fetch(`${import.meta.env.VITE_BACKEND_URL}/api/projects?query=${query}`, {
        credentials: "include",
        headers: {
          "Content-Type": "application/json",
        },
      });

      const data = await res.json();

      if (res.ok) {
        setError(false);
        projects = data.projects;
      } else {
        // Display error message
        setError(true);
      }
    } catch (err) {
      console.error(err);
      setError(true);
    }

    // Display projects
    setSearchedProjects(projects);
    setShowSearchedProjects(true);
  }

  return (
    <div class="relative w-full" use:clickOutside={() => setShowSearchedProjects(false)}>
      <label class="input input-bordered flex items-center gap-2">
        <input type="text" class="grow" placeholder="Search projects" onClick={searchProjects} onInput={searchProjects}/>
        <i class="fa-solid fa-magnifying-glass" />
      </label>

      <Show when={showSearchedProjects()}>
        <ul class="menu absolute top-full left-0 w-full bg-base-100 rounded-md shadow-lg border border-gray-200 z-30 block max-h-[250px] overflow-y-scroll">
          <Show when={searchedProjects().length === 0 && !error()}>
            <li class="menu-item"><a>No projects found</a></li>
          </Show>
          <Show when={error()}>
            <li class="menu-item"><a>Error getting projects</a></li>
          </Show>
          <For each={searchedProjects()}>
            {(project) => (
              <li class="menu-item" onClick={() => {
                props.onSelect(project);
                setShowSearchedProjects(false);
              }}><a>{project.name}</a></li>
            )}
          </For>
        </ul>
      </Show>
    </div>
  )
}
