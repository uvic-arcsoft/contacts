import { createSignal, Show, For, mergeProps } from "solid-js";
import clickOutside from "~/directives/clickOutside";

export default function UsersSearchBar(_props) {
  const props = mergeProps({ onSelect: () => {} }, _props);
  const [searchedUsers, setSearchedUsers] = createSignal([]);
  const [showSearchedUsers, setShowSearchedUsers] = createSignal(false);
  const [error, setError] = createSignal(false);

  const searchUsers = async(e) => {
    const query = e.target.value;
    let users = [];
    
    try {
      // Make an API call to the backend to get matched users
      const res = await fetch(`${import.meta.env.VITE_BACKEND_URL}/auth/search?query=${query}`, {
        credentials: "include",
        headers: {
          "Content-Type": "application/json",
        },
      });

      const data = await res.json();

      if (res.ok) {
        setError(false);
        users = data.users;
      } else {
        // Display error message
        setError(true);
      }
    } catch (err) {
      console.error(err);
      setError(true);
    }

    setSearchedUsers(users);
    setShowSearchedUsers(true);
  }

  return (
    <div class="relative w-full" use:clickOutside={() => setShowSearchedUsers(false)}>
      <label class="input input-bordered flex items-center gap-2">
        <input type="text" class="grow" placeholder="Search users" onClick={searchUsers} onInput={searchUsers}/>
        <i class="fa-solid fa-magnifying-glass" />
      </label>

      <Show when={showSearchedUsers()}>
        <ul class="menu absolute top-full left-0 w-full bg-base-100 rounded-md shadow-lg border border-gray-200 z-30 block max-h-[250px] overflow-y-scroll">
          <Show when={searchedUsers().length === 0}>
            <li class="menu-item"><a>No users found</a></li>
          </Show>
          <Show when={error()}>
            <li class="menu-item"><a>Error getting users</a></li>
          </Show>
          <For each={searchedUsers()}>
            {(user) => (
              <li class="menu-item" onClick={() => {
                props.onSelect(user);
                setShowSearchedUsers(false);
              }}><a>{user.username}</a></li>
            )}
          </For>
        </ul>
      </Show>
    </div>
  )
}
