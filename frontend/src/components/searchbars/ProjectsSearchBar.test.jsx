import { test, expect, vi } from "vitest";
import { render } from "@solidjs/testing-library"
import userEvent from "@testing-library/user-event";
import { server } from "~/mocks/node";
import { projectX, projectY, projectZ } from "~/mocks/data";

import ProjectsSearchBar from "./ProjectsSearchBar";

server.listen();
const user = userEvent.setup();

test("Render project search bar. Project search bar returns projects that have a name matching the searched word.", async () => {
  let selectedProjectId = null;
  render(() => <ProjectsSearchBar 
                onSelect={project => selectedProjectId = project.id}/>);

  // Test placeholder text
  const searchBar = document.querySelector('input');
  expect(searchBar.placeholder).toBe('Search projects');

  // Click on the search bar should returns all projects that this user has access to
  user.click(searchBar);
  await vi.waitUntil(() => Array.from(document.querySelectorAll('.menu-item')).length > 0);
  const projects = Array.from(document.querySelectorAll('.menu-item'));
  expect(projects.length).toBe(3);

  // Test rendering of each project result
  let mockedProjects = [projectX, projectY, projectZ];
  for (let i = 0; i < projects.length; i++) {
    expect(projects[i].querySelector('a').textContent).toContain(mockedProjects[i].name);
  }

  // Test searching for a project by its name.
  // Only project X has "Project X" in its name
  user.type(searchBar, 'Project X');
  await vi.waitUntil(() => Array.from(document.querySelectorAll('.menu-item')).length === 1);
  expect(document.querySelector('.menu-item a').textContent).toContain(projectX.name);

  // Test clicking on a project result
  user.click(document.querySelector('.menu-item'));
  expect(selectedProjectId === projectX.id);
});
