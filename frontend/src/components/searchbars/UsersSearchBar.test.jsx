import { test, expect, vi } from "vitest";
import { render } from "@solidjs/testing-library"
import userEvent from "@testing-library/user-event";
import { server } from "~/mocks/node";
import { memberA, memberB, memberC, memberD, memberTestUser } from "~/mocks/data";

import UsersSearchBar from "./UsersSearchBar";

server.listen();
const user = userEvent.setup();

test("Render users search bar. Users search bar returns users that have a username matching the searched word.", async () => {
  let selectedUserId = null;
  render(() => <UsersSearchBar 
                onSelect={user => selectedUserId = user.id}/>);

  // Test placeholder text
  const searchBar = document.querySelector('input');
  expect(searchBar.placeholder).toBe('Search users');

  // Click on the search bar should returns all users that this user has access to
  user.click(searchBar);
  await vi.waitUntil(() => Array.from(document.querySelectorAll('.menu-item')).length > 0);
  const users = Array.from(document.querySelectorAll('.menu-item'));
  expect(users.length).toBe(5);

  // Test rendering of each user result
  let mockedUsers = [memberA, memberB, memberC, memberD, memberTestUser];
  for (let i = 0; i < users.length; i++) {
    expect(users[i].querySelector('a').textContent).toContain(mockedUsers[i].username);
  }

  // Test searching for a user by their username
  // Only member A has "member-a" in his username
  user.type(searchBar, 'member-a');
  await vi.waitUntil(() => Array.from(document.querySelectorAll('.menu-item')).length === 1);
  expect(document.querySelector('.menu-item a').textContent).toContain(memberA.username);

  // Test clicking on a user result
  user.click(document.querySelector('.menu-item'));
  expect(selectedUserId === memberA.id);

  // Test searching for a user by their name
  // Only member B has "Member B" in his name
  user.clear(searchBar);
  user.type(searchBar, 'Member B');
  await vi.waitUntil(() => Array.from(document.querySelectorAll('.menu-item')).length === 1 && !document.querySelector('.menu-item a').textContent.includes(memberA.username));
  expect(document.querySelector('.menu-item a').textContent).toContain(memberB.username);
});
