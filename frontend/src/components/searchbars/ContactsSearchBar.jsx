import { createSignal, Show, For, Switch, Match, mergeProps } from "solid-js";
import clickOutside from "~/directives/clickOutside";

export default function ContactsSearchBar(_props) {
  const props = mergeProps({ onSelect: () => {} }, _props);
  const [searchedContacts, setSearchedContacts] = createSignal([]);
  const [showSearchedContacts, setShowSearchedContacts] = createSignal(false);
  const [error, setError] = createSignal(false);

  const searchContacts = async(e) => {
    const query = e.target.value;
    let contacts = [];

    try {
      // Make an API call to the backend to get matched contacts
      const res = await fetch(`${import.meta.env.VITE_BACKEND_URL}/api/contacts?query=${query}`, {
        credentials: "include",
        headers: {
          "Content-Type": "application/json",
        },
      });

      const data = await res.json();

      if (res.ok) {
        setError(false);
        contacts = data.contacts;
      } else {
        // Display error message
        setError(true);
      }
    } catch (err) {
      console.error(err);
      setError(true);
    }

    // Display contacts
    setSearchedContacts(contacts);
    setShowSearchedContacts(true);
  }

  return (
    <div class="relative w-full" use:clickOutside={() => setShowSearchedContacts(false)}>
      <label class="input input-bordered flex items-center gap-2">
        <input type="text" class="grow" placeholder="Search activities by synopsis" onClick={searchContacts} onInput={searchContacts}/>
        <i class="fa-solid fa-magnifying-glass" />
      </label>

      <Show when={showSearchedContacts()}>
        <ul class="menu absolute top-full left-0 w-full bg-base-100 rounded-md shadow-lg border border-gray-200 z-30 block max-h-[500px] overflow-y-scroll">
          <Show when={searchedContacts().length === 0 && !error()}>
            <li class="menu-item"><a>No activities found</a></li>
          </Show>
          <Show when={error()}>
            <li class="menu-item"><a>Error getting activities</a></li>
          </Show>
          <For each={searchedContacts()}>
            {(contact) => (
              <li class="menu-item" onClick={() => {
                props.onSelect(contact);
                setShowSearchedContacts(false);
              }}>
                <a class="text-xs flex flex-col gap-2 items-start py-3">
                  {/* Name */}
                  <h4 class="font-semibold">{contact.name}</h4>

                  {/* Status */}
                  <div class="flex gap-1 items-center">
                    <label class="font-semibold">Status:</label>
                    <Switch>
                      <Match when={contact.status === "initial/building"}>
                        <div class="badge badge-yellow">Initial / building</div>
                      </Match>
                      <Match when={contact.status === "active"}>
                        <div class="badge badge-green">Active</div>
                      </Match>
                      <Match when={contact.status === "stalled"}>
                        <div class="badge badge-orange">Stalled</div>
                      </Match>
                      <Match when={contact.status === "dormant"}>
                        <div class="badge badge-gray">Dormant</div>
                      </Match>
                    </Switch>
                  </div>

                  {/* Researchers */}
                  <div class="flex flex-col gap-1">
                    <label class="font-semibold">Researchers: </label>
                    {contact?.researchers.map(r => r.name).join(", ")}
                  </div>

                  {/* Projects */}
                  <div class="flex flex-col gap-1">
                    <label class="font-semibold">Projects: </label>
                    {contact?.projects.map(p => p.name).join(", ")}
                  </div>
                </a>
              </li>
            )}
          </For>
        </ul>
      </Show>
    </div>
  )
}
