import { test, expect, vi } from "vitest";
import { render } from "@solidjs/testing-library"
import userEvent from "@testing-library/user-event";
import { server } from "~/mocks/node";
import { researcherJohn, researcherJane, researcherSam } from "~/mocks/data";

import ResearchersSearchBar from "./ResearchersSearchBar";

server.listen();
const user = userEvent.setup();

test("Render researchers search bar. Researchers search bar returns researchers that have a name matching the searched word.", async () => {
  let selectedResearcherId = null;
  render(() => <ResearchersSearchBar 
                onSelect={researcher => selectedResearcherId = researcher.id}/>);

  // Test placeholder text
  const searchBar = document.querySelector('input');
  expect(searchBar.placeholder).toBe('Search researchers');

  // Click on the search bar should returns all researchers that this user has access to
  user.click(searchBar);
  await vi.waitUntil(() => Array.from(document.querySelectorAll('.menu-item')).length > 0);
  const researchers = Array.from(document.querySelectorAll('.menu-item'));
  expect(researchers.length).toBe(3);

  // Test rendering of each researcher result
  let mockedResearchers = [researcherJohn, researcherJane, researcherSam];
  for (let i = 0; i < researchers.length; i++) {
    expect(researchers[i].querySelector('a').textContent).toContain(mockedResearchers[i].name);
  }

  // Test searching for a researcher by their name.
  // Only researcher John Doe has "John" in his name
  user.type(searchBar, 'John');
  await vi.waitUntil(() => Array.from(document.querySelectorAll('.menu-item')).length === 1);
  expect(document.querySelector('.menu-item a').textContent).toContain(researcherJohn.name);

  // Test clicking on a researcher result
  user.click(document.querySelector('.menu-item'));
  expect(selectedResearcherId === researcherJohn.id);
});
