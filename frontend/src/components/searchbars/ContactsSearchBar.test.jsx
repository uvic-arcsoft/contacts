import { test, expect, vi } from "vitest";
import { render } from "@solidjs/testing-library"
import userEvent from "@testing-library/user-event";
import { server } from "~/mocks/node";
import { contactA, contactB, contactC } from "~/mocks/data";

import ContactsSearchBar from "./ContactsSearchBar";

server.listen();
const user = userEvent.setup();

test("Render contacts search bar. Contact search bar returns contacts that have a synopsis matching the searched word.", async () => {
  let selectedContactId = null;
  const { getByText } = render(() => <ContactsSearchBar 
                onSelect={contact => selectedContactId = contact.id}/>);
  
  // Test placeholder text
  const searchBar = document.querySelector('input');
  expect(searchBar.placeholder).toBe('Search activities by synopsis');

  // Click on the search bar should returns all contacts that this user has access to
  user.click(searchBar);
  await vi.waitUntil(() => Array.from(document.querySelectorAll('.menu-item')).length > 0);
  const contacts = Array.from(document.querySelectorAll('.menu-item'));
  expect(contacts.length).toBe(3);

  // Test rendering of each contact result
  let mockedContacts = [contactA, contactB, contactC];
  for (let i = 0; i < contacts.length; i++) {
    expect(contacts[i].querySelector('h4').textContent).toContain(mockedContacts[i].name);
    expect(contacts[i].querySelector('.badge').textContent.toLowerCase().replace(/\s+/g, "")).toContain(mockedContacts[i].status.toLowerCase().replace(/\s+/g, ""));
    expect(getByText(mockedContacts[i].researchers.map(researcher => researcher.name).join(', '))).toBeInTheDocument();
    expect(getByText(mockedContacts[i].projects.map(project => project.name).join(', '))).toBeInTheDocument();
  }

  // Test searching for a contact by its synopsis.
  // Only contact B has "Discussion" in its synopsis
  user.type(searchBar, 'Discussion');
  await vi.waitUntil(() => Array.from(document.querySelectorAll('.menu-item')).length === 1);
  expect(document.querySelector('.menu-item h4').textContent).toContain(contactB.name);

  // Test clicking on a contact result
  user.click(document.querySelector('.menu-item'));
  expect(selectedContactId === contactB.id);
});
