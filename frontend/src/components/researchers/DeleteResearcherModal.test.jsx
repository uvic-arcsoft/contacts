import { test, expect } from "vitest";
import { render } from "@solidjs/testing-library"
import DeleteResearcherModal from "./DeleteResearcherModal";
import { FeedbackProvider } from "~/contexts/FeedbackContext";
import { researcherJohn } from "~/mocks/data";

const wrapper = (props) => (
  <FeedbackProvider>
    {props.children}
  </FeedbackProvider>
);

const researcher = () => researcherJohn;

test("render researcher delete modal", async () => {
  const { getByText } = render(() => <DeleteResearcherModal researcher={researcher} />, { wrapper });
  
  // Test modal content
  expect(getByText(`Delete ${researcherJohn.name}`)).toBeInTheDocument();
  expect(document.querySelector('p').innerHTML).toContain(`Are you sure you want to delete <strong>${researcher().name}</strong> from your contact list?`);
  expect(document.querySelector('p.text-sm').textContent).toContain('Note: Removing this researcher will also remove them from all associated activities.');
  expect(document.querySelector('button.btn-red').textContent).toBe('Yes');
  expect(document.querySelector('button.btn-blue').textContent).toBe('No');
});
