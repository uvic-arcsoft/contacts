import { test, expect, vi } from "vitest";
import { render } from "@solidjs/testing-library"
import userEvent from "@testing-library/user-event";

import { server } from "~/mocks/node";

import ResearcherModal from "./ResearcherModal";
import { FeedbackProvider } from "~/contexts/FeedbackContext";

import { researcherJohn, contactA } from "~/mocks/data";

const user = userEvent.setup();
server.listen();

const wrapper = (props) => (
  <FeedbackProvider>
    {props.children}
  </FeedbackProvider>
);

const researcher = () => researcherJohn;
test("render researcher modal with an existing researcher", async () => {
  // Update, manage a researcher's information
  const { getByText } = render(
    () => <ResearcherModal
            onSaveResearcher={() => {}}
            researcher={researcher}
            initialActiveTab={() => "info"}/>,
  { wrapper });
  
  // Wait for the modal to load researcher
  // The cue is the modal has researcher's name as the heading
  await vi.waitUntil(() => document.querySelector('h3').textContent.includes(researcher().name));
  expect(getByText(researcher().name)).toBeInTheDocument();

  // The researcher modal should have a side bar
  expect(document.getElementById('researcher-modal').innerHTML).toContain('<h5 class="text-sm font-semibold text-gray-500">View</h5>');
  expect(document.getElementById('researcher-modal').innerHTML).toContain('<h5 class="text-sm font-semibold text-gray-500 mt-5">Actions</h5');

  // Check information of the info tab
  const nameInput = document.querySelector('input[name="name"]');
  expect(nameInput.value).toBe(researcher().name);

  const emailInput = document.querySelector('input[name="email"]');
  expect(emailInput.value).toBe(researcher().email);

  const researcherUrlInput = document.querySelector('input[name="contactUrl"]');
  expect(researcherUrlInput.value).toBe(researcher().contact_url);

  // Check contacts of the contacts tab
  await user.click(getByText('Activities'));
  await vi.waitUntil(() => document.body.textContent.includes(contactA.name));

  // Test saving the researcher info
  await user.click(getByText('Info'));
  await vi.waitUntil(() => document.querySelector('form.info'));
  await user.click(getByText('Save'));
});
