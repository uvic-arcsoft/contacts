import { createEffect } from 'solid-js';

import AgGridSolid from 'ag-grid-solid';
import "ag-grid-community/styles/ag-grid.css"; // Mandatory CSS required by the grid
import "ag-grid-community/styles/ag-theme-quartz.css"; // Optional Theme applied to the grid

const ResearcherName = (params) => {
  return (
    <button onClick={() => {
      params.data.onSelect("info");
      document.getElementById('researcher-modal').showModal();
    }} class='link font-semibold'>{params.value}</button>
  )
}

const ResearcherContactUrl = (params) => {
  return <a href={params.value} target='_blank' class='link font-semibold'>{params.value}</a>
}

const ResearcherActions = (params) => {
  return (
    <div class="flex gap-5 items-center">
      <button onClick={() => {
        params.data.onSelect("info");
        document.getElementById('researcher-modal').showModal();
      }}><i class="fa-solid fa-pen-to-square text-blue-500 hover:text-blue-700" /></button>
      <button onClick={() => {
        params.data.onSelect();
        document.getElementById(`delete-researcher-modal-${params.data.id}`).showModal();
      }}><i class="fa-solid fa-trash-can text-red-500 hover:text-red-700" /></button>
      <button onClick={() => {
        params.data.onSelect("contacts");
        document.getElementById('researcher-modal').showModal();
      }}><i class="fa-solid fa-list-check text-green-700 hover:text-green-800" /></button>
    </div>
  )
}

{/* eslint-disable solid/reactivity */}
{/* Disable solid/reactivity: AGGrid table doesn't render properly with rowData as a signal */}
export default function ResearchersTable(props) {
  const setSelectedResearcher = props.setSelectedResearcher;
  const setModalActiveTab = props.setModalActiveTab;
  let gridRef;

  const columnDefs = [
    { headerName: "Name", field: "name", flex: 1, cellRenderer: ResearcherName },
    { headerName: "Email", field: "email", flex: 2 },
    { headerName: "Contact URL", field: "contactUrl", cellRenderer: ResearcherContactUrl, flex: 2 },
    { headerName: "Actions", field: "id", cellRenderer: ResearcherActions, flex: 1 },
  ]

  let rowData = props.researchers().map(researcher => ({
    id: researcher.id,
    name: researcher.name,
    email: researcher.email,
    contactUrl: researcher.contact_url,
    onSelect: (tab="info") => {
      setSelectedResearcher(researcher);
      setModalActiveTab(tab);
    },
  }))

  // Update researchers table when there is any change to the researchers
  createEffect(() => {
    rowData = props.researchers().map(researcher => ({
      id: researcher.id,
      name: researcher.name,
      email: researcher.email,
      contactUrl: researcher.contact_url,
      onSelect: (tab="info") => {
        setSelectedResearcher(researcher);
        setModalActiveTab(tab);
      },
    }))
    gridRef?.api?.setRowData(rowData);
  })

  // Allow filtering and sorting on all columns
  const defaultColDef = {
    filter: true,
    sortable: true,
  };

  return (
    <div class="ag-theme-quartz h-[70vh] min-w-[700px]">
      <AgGridSolid
        ref={gridRef}
        rowData={rowData}
        columnDefs={columnDefs}
        defaultColDef={defaultColDef}
        rowHeight={50}
        pagination={true}
        paginationPageSize={10}
        paginationPageSizeSelector={[10, 25, 50]}
      />
    </div>
  )
}
{/* eslint-enable solid/reactivity */}
