import { test, expect, vi } from "vitest";
import { render } from "@solidjs/testing-library"

import Researchers from "./Researchers";
import { FeedbackProvider } from "~/contexts/FeedbackContext";

import { server } from "~/mocks/node";

server.listen()

const wrapper = (props) => (
  <FeedbackProvider>
    {props.children}
  </FeedbackProvider>
);

test("render researchers page", async () => {
  const { getByText } = render(() => <Researchers />, { wrapper });

  // Test loading state
  expect(getByText('Loading researchers...')).toBeInTheDocument();
  
  // Wait for researchers to load
  await vi.waitUntil(() => Array.from(document.querySelectorAll('div[role="row"]')).slice(1).length > 0);
  expect(getByText('Researchers')).toBeInTheDocument();

  // There should be 3 researchers
  const rows = Array.from(document.querySelectorAll('div[role="row"]')).slice(1);
  expect(rows.length).toBe(3);

  // NOTE: The researcher contents are tested in ResearchersTable.test.jsx
});
