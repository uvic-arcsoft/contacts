import { test, expect, vi } from "vitest";
import { render } from "@solidjs/testing-library"
import ResearchersTable from "./ResearchersTable";
import { researcherJohn, researcherJane, researcherSam } from "~/mocks/data";

const researchers = () => {
  return [researcherJohn, researcherJane, researcherSam];
};

test("renders researchers table", async () => {
  render(() => <ResearchersTable researchers={researchers} />);

  // Wait for the rows to be rendered
  await vi.waitUntil(() => document.querySelector('div[role="row"]'))
  
  // Remove the first header row
  const rows = Array.from(document.querySelectorAll('div[role="row"]')).slice(1);
  for (let i = 0; i < rows.length; i++) {
    const row = rows[i];
    const researcher = researchers()[i];
    
    // Test researcher name rendering
    const nameCell = row.querySelector('div[role="gridcell"][col-id="name"]').querySelector('button');
    expect(nameCell.textContent).toBe(researcher.name);

    // Test researcher email rendering
    const emailCell = row.querySelector('div[role="gridcell"][col-id="email"]');
    expect(emailCell.textContent).toContain(researcher.email);

    // Test researcher contact URL rendering
    const contactUrlCell = row.querySelector('div[role="gridcell"][col-id="contactUrl"]').querySelector('a');
    expect(contactUrlCell.getAttribute("href")).toBe(researcher.contact_url);
    expect(contactUrlCell.textContent).toBe(researcher.contact_url);
  }
});
