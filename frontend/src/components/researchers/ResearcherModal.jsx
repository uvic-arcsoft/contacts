import { useContext, createSignal, createEffect, createResource, Show, Switch, For, Match, mergeProps } from "solid-js";
import { getCookie } from "~/helper";
import FeedbackContext from "~/contexts/FeedbackContext";

export default function ResearcherModal(_props) {
  // This signal is used purely for display purposes
  const props = mergeProps({ researcher: null, initialActiveTab: null }, _props);
  const [ name, setName ] = createSignal("");
  const [activeTab, setActiveTab] = createSignal("info");

  createEffect(() => {
    if (props.researcher) {
      setName(props.researcher()?.name);
    }
  });

  createEffect(() => {
    if (props.initialActiveTab) {
      setActiveTab(props.initialActiveTab());
    }
  });

  return (
    <dialog id="researcher-modal" class="modal">
      <div class={`modal-box w-full ${props.researcher ? "md:w-3/4 md:max-w-[800px]" : ""}`}>

        {/* Close modal button */}
        <form method="dialog">
          <button class="btn btn-sm btn-circle btn-ghost absolute right-2 top-2">✕</button>
        </form>

        <h3 class="font-bold text-xl">{props.researcher  ? name() : "New researcher" }</h3>
        <p class="mt-1 text-gray-500 text-sm">{props.researcher ? "View and update researcher information" : "Add a new researcher to your contact list"}</p>

        <div class="flex gap-5">
          <div class="grow">
            <div class={activeTab() === "info" ? "" : "hidden"}>
              <InfoTab researcher={props.researcher} onSaveResearcher={props.onSaveResearcher} setName={setName}/>
            </div>

            <Show when={props.researcher}>
              <div class={activeTab() === "contacts" ? "" : "hidden"}>
                <ContactsTab researcher={props.researcher} />
              </div>
            </Show>
          </div>

          <Show when={props.researcher}>
            <div>
              {/* View */}
              <h5 class="text-sm font-semibold text-gray-500">View</h5>
              <div class="flex flex-col gap-2 mt-2 w-fit min-w-[80px] md:min-w-[150px]">
                <button class={`btn btn-sm md:btn-md justify-start ${activeTab() === "info" ? "btn-active" : ""}`} onClick={() => setActiveTab("info")}><i class="fa-solid fa-user mr-1" /> Info</button>
                <button class={`btn btn-md justify-start ${activeTab() === "contacts" ? "btn-active" : ""}`} onClick={() => setActiveTab("contacts")}><i class="fa-solid fa-address-book" />Activities</button>
              </div>

              {/* Actions */}
              <h5 class="text-sm font-semibold text-gray-500 mt-5">Actions</h5>
              <div class="flex flex-col gap-2 mt-2 w-fit min-w-[80px] md:min-w-[150px]">
                <button class="btn btn-sm md:btn-md justify-start" onClick={() => {
                  window.localStorage.setItem("new-activity-researchers", JSON.stringify([props.researcher()]));
                  window.location.href = "/contacts/new";
                }}><i class="fa-solid fa-plus mr-1" /> Add activity</button>
              </div>
            </div>
          </Show>
        </div>

      </div>
      <form method="dialog" class="modal-backdrop">
        <button>close</button>
      </form>
    </dialog>
  )
}

const InfoTab = (props) => {
  const { setType, setMessage } = useContext(FeedbackContext);

  // Add a new researcher
  const saveResearcher = async (e) => {
    e.preventDefault();
    const newResearcher = {
      name: e.target.name.value,
      email: e.target.email?.value,
      contact_url: e.target.contactUrl?.value,
    };

    // Send a POST request to the backend
    try {
      const url = props.researcher ? `${import.meta.env.VITE_BACKEND_URL}/api/researchers/${props.researcher()?.id}/update/` : `${import.meta.env.VITE_BACKEND_URL}/api/researchers/create/`;
      const method = props.researcher ? "PUT" : "POST";
      const res = await fetch(url, {
        method: method,
        credentials: "include",
        headers: {
          "Content-Type": "application/json",
          "X-CSRFToken": getCookie("csrftoken"),
        },
        body: JSON.stringify(newResearcher),
      });

      const data = await res.json();
      
      if (res.ok) {
        setType("success");
        setMessage(data.message);

        // Render the new researcher on the table
        props.onSaveResearcher({
          id: data.researcher_id,
          ...newResearcher,
        });

        // Clear the form
        e.target.name.value = "";
        e.target.email.value = "";
        e.target.contactUrl.value = "";

        // Close the modal
        document.getElementById("researcher-modal").close();
      } else {
        setType("error");
        setMessage(data.error);
      }
    } catch (err) {
      setType("error");
      setMessage(err.message);
    }
  };

  return (
    <form class="info" onSubmit={saveResearcher}>
      {/* Name */}
      <div class="mt-5">
        <div><label class="text-base font-semibold">Name <span class="text-red-500">*</span></label></div>
        <input 
        type="text"
        name="name"
        value={props.researcher ? props.researcher()?.name : ""}
        placeholder="Researcher's name"
        class="input input-bordered w-full mt-2"
        required
        onInput={e => props.setName(e.target.value)}/>
      </div>

      {/* Email */}
      <div class="mt-5">
        <div><label class="text-base font-semibold">Email</label></div>
        <input
        type="email"
        name="email"
        value={props.researcher ? props.researcher()?.email : ""}
        placeholder="Researcher's email"
        class="input input-bordered w-full mt-2" />
      </div>

      {/* Contact URL */}
      <div class="mt-5">
        <div><label class="text-base font-semibold">Contact URL</label></div>
        <input
          type="url"
          name="contactUrl"
          value={props.researcher ? props.researcher()?.contact_url : ""}
          placeholder="Researcher's contact URL"
          class="input input-bordered w-full mt-2" />
        <p class="text-sm text-gray-500 mt-2">Must start with either <strong>https</strong> or <strong>http</strong></p>
      </div>

      <div class="modal-action justify-start">
        <button type="submit" class="btn btn-blue">{props.researcher ? "Save" : "Add researcher"}</button>
      </div>
    </form>
  )
}

const ContactsTab = (props) => {
  const getContactsForResearcher = async (researcher) => {
    if (!researcher) return [];
    try {
      const res = await fetch(`${import.meta.env.VITE_BACKEND_URL}/api/contacts?researchers=${researcher.id}`, {
        credentials: "include",
        headers: {
          "Content-Type": "application/json",
        },
      });
      const data = await res.json();

      if (!res.ok) {
        throw new Error(data.error);
      }
      return data.contacts;
    } catch (err) {
      throw new Error(err.message);
    }
  }

  {/* eslint-disable solid/reactivity */}
  {/* TODO: Figure out how to pass the researcher prop to the createResource function
  to stil achieve reactivity and avoid the eslint-disable comment */}
  const [contacts] = createResource(props.researcher, getContactsForResearcher);
  {/* eslint-enable solid/reactivity */}

  return (
    <Switch>
      <Match when={contacts.loading}>
        <div class="flex flex-col items-center justify-center h-96">
          <p class="text-gray-500 flex gap-2 items-center text-base p-32">
            <span class="loading loading-spinner loading-md" />
            Loading activities with {props.researcher()?.name}...
          </p>
        </div>
      </Match>
      <Match when={contacts.error}>
        <div class="flex flex-col items-center justify-center h-96">
          <p class="text-red-500">{contacts.error.message}</p>
        </div>
      </Match>
      <Match when={contacts()?.length === 0}>
        <div class="flex flex-col items-center justify-center h-96">
          <p class="text-gray-500">No activities found with <strong>{props.researcher()?.name}.</strong></p>
        </div>
      </Match>
      <Match when={contacts().length > 0}>
        <div class="mt-2">
          <For each={contacts()}>
            {contact => (
              <div class="card bg-base-100 w-full shadow-xl border border-gray-200 my-2">
                <div class="card-body">
                  <a href={`/contacts/${contact.id}`} class="card-title link text-lg">{contact.name}</a>
                  <div class="text-sm flex flex-col gap-1">
                    <p>
                      <strong>Status: </strong>
                      <Switch>
                        <Match when={contact.status === "initial/building"}>
                          <div class="badge badge-yellow">Initial building</div>
                        </Match>
                        <Match when={contact.status === "active"}>
                          <div class="badge badge-green">Active</div>
                        </Match>
                        <Match when={contact.status === "stalled"}>
                          <div class="badge badge-orange">Stalled</div>
                        </Match>
                        <Match when={contact.status === "dormant"}>
                          <div class="badge badge-gray">Dormant</div>
                        </Match>
                      </Switch>
                    </p>
                    <p>
                      <strong>Researchers: </strong>
                      <span>{contact?.researchers?.map(r => r.name).join(', ')}</span>
                    </p>
                    <p>
                      <strong>Projects: </strong>
                      <span>{contact?.projects?.map(p => p.name).join(', ')}</span>
                    </p>
                  </div>
                </div>
              </div>
            )}
          </For>
        </div>
      </Match>
    </Switch>
  )
}
