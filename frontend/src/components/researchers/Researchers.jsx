import { createResource, Switch, Match, Show, createSignal } from "solid-js";
import ResearchersTable from "./ResearchersTable";
import ResearcherModal from "./ResearcherModal";
import DeleteResearcherModal from "./DeleteResearcherModal";

const getResearchers = async () => {
  try {
    const res = await fetch(`${import.meta.env.VITE_BACKEND_URL}/api/researchers/`, {
      credentials: "include",
      headers: {
        "Content-Type": "application/json",
      },
    });
    const data = await res.json();

    if (!res.ok) {
      throw new Error(data.error);
    }
    return data.researchers;
  } catch (err) {
    throw new Error(err.message);
  }
}

export default function Researchers() {
  const [researchers, { mutate: setResearchers }] = createResource(getResearchers);
  const [selectedResearcher, setSelectedResearcher] = createSignal(null);
  const [modalActiveTab, setModalActiveTab] = createSignal("info");

  const addResearcher = (newResearcher) => {
    setResearchers([newResearcher, ...researchers()]);
  }

  const updateResearcher = (updatedResearcher) => {
    const updatedResearchers = researchers().map(researcher => {
      if (researcher.id === updatedResearcher.id) {
        return updatedResearcher;
      }
      return researcher;
    });
    setResearchers(updatedResearchers);
  }

  const deleteResearcher = (deletedResearcher) => {
    const updatedResearchers = researchers().filter(researcher => researcher.id !== deletedResearcher.id);
    setSelectedResearcher(null);
    setResearchers(updatedResearchers);
  }

  return (
    <main>
      <div class="flex flex-row justify-between">
        <h1 class="text-gray-900 text-xl md:text-3xl font-semibold">Researchers</h1>
        <button onClick={()=> {
          setSelectedResearcher(null);
          document.getElementById('researcher-modal').showModal();
        }} class="btn btn-md btn-blue"><i class="fa-solid fa-plus" /> New Researcher</button>
      </div>
      <p class="text-gray-700 text-sm">Hover on the table header to sort and filter researchers</p>

      <Switch>
        <Match when={researchers.loading}>
          <div class="flex flex-col items-center justify-center h-96">
            <p class="text-gray-500 flex gap-2 items-center text-xl p-32">
              <span class="loading loading-spinner loading-md" />
              Loading researchers...
            </p>
          </div>
        </Match>
        <Match when={researchers.error}>
          <div class="flex flex-col items-center justify-center h-96">
            <p class="text-red-500">{researchers.error.message}</p>
          </div>
        </Match>
        <Match when={researchers()?.length === 0}>
          <div class="flex flex-col items-center justify-center h-96">
            <p class="text-gray-500">No researchers found. Click on the <strong>+ New researcher</strong> button to create one.</p>
          </div>
        </Match>
        <Match when={researchers().length > 0}>
          <section class="flex flex-col gap-8 mt-4">
            <ResearchersTable researchers={researchers} setSelectedResearcher={setSelectedResearcher} setModalActiveTab={setModalActiveTab}/>
            <DeleteResearcherModal researcher={selectedResearcher} onDeleteResearcher={deleteResearcher}/>
          </section>
        </Match>
      </Switch>

      <Show when={!researchers.loading && !researchers.error}>
        <Show when={selectedResearcher()} fallback={
          <ResearcherModal onSaveResearcher={(r) => addResearcher(r)}/>
        }>
          <ResearcherModal researcher={selectedResearcher} onSaveResearcher={(r) => updateResearcher(r)} initialActiveTab={modalActiveTab}/>
        </Show>
      </Show>
    </main>
  )
}
