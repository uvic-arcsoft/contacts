import { test, expect } from "vitest";
import { render } from "@solidjs/testing-library"

import ResearcherModal from "./ResearcherModal";
import { FeedbackProvider } from "~/contexts/FeedbackContext";

const wrapper = (props) => (
  <FeedbackProvider>
    {props.children}
  </FeedbackProvider>
);

test("render researcher creation modal", async () => {
  // Test researcher modal for researcher creation
  const { getByText } = render(() => <ResearcherModal />, { wrapper });
  expect(getByText('New researcher')).toBeInTheDocument();

  // The researcher creation modal should not have a side bar
  expect(document.getElementById('researcher-modal').innerHTML).not.toContain('<h5 class="text-sm font-semibold text-gray-500">View</h5>');
  expect(document.getElementById('researcher-modal').innerHTML).not.toContain('<h5 class="text-sm font-semibold text-gray-500">Actions</h5>');

  // The form should be empty
  const nameInput = document.querySelector('input[name="name"]');
  expect(nameInput.value).toBe('');

  const emailInput = document.querySelector('input[name="email"]');
  expect(emailInput.value).toBe('');

  const researcherUrlInput = document.querySelector('input[name="contactUrl"]');
  expect(researcherUrlInput.value).toBe('');
  expect(document.querySelector('p.text-sm.text-gray-500.mt-2').textContent).toContain('Must start with either https or http');
});
