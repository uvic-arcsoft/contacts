import { test, expect, vi } from "vitest";
import { render } from "@solidjs/testing-library"
import { Router } from "@solidjs/router";

import { server } from "~/mocks/node";

import Sidebar from "./Sidebar";

server.listen()

const wrapper = (props) => (
  <Router
    root={() => (
      <>
        {props.children}
      </>
    )}
   />
);

test("Renders sidebar", async () => {
  render(() => <Sidebar />, { wrapper });
  await vi.waitUntil(() => document.querySelector('.drawer'));

  // Test navigation links
  expect(document.querySelector('a[href="/researchers"]').textContent).toContain('Researchers');
  expect(document.querySelector('a[href="/contacts"]').textContent).toContain('Recent activities');
  expect(document.querySelector('a[href="/projects"]').textContent).toContain('Projects');
  expect(document.querySelector('a[href="/artifacts"]').textContent).toContain('Artifacts');
  expect(document.querySelector('a[href="/softwares"]').textContent).toContain('Softwares');
  expect(document.querySelector('a[href="/usage"]').textContent).toContain('Usage');
});
