import { test, expect } from "vitest";
import { render } from "@solidjs/testing-library"
import { useContext } from "solid-js";

import FeedbackContext, { FeedbackProvider } from "~/contexts/FeedbackContext";
import FeedbackToast from "./FeedbackToast";

// Create a component that sets the feedback type and message
const TestFeedbackToast = () => {
  const { setType, setMessage } = useContext(FeedbackContext);
  setType("success");
  setMessage("This is a feedback toast");

  return <FeedbackToast />;
};

const wrapper = (props) => (
  <FeedbackProvider>
    {props.children}
  </FeedbackProvider>
);

test("Render feedback toast", async () => {
  const { getByText } = render(
    () => <TestFeedbackToast />
    , { wrapper });
  expect(getByText('This is a feedback toast')).toBeInTheDocument();
  expect(document.querySelector('.alert-success')).toBeInTheDocument();
});
