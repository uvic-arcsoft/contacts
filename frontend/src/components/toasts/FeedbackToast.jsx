import { Show, useContext, createEffect, createSignal, onCleanup } from "solid-js";
import FeedbackContext from "~/contexts/FeedbackContext";

export default function FeedbackToast() {
  const { type, setType, message, setMessage } = useContext(FeedbackContext);
  const [alertClass, setAlertClass] = createSignal("");
  let timeout;

  createEffect(() => {
    setAlertClass(`alert-${type()}`);

    // Each message should only be displayed for 10 seconds
    if (message()) {
      clearTimeout(timeout);
      timeout = setTimeout(() => {
        setType("success");
        setMessage("");
      }, 10000);
    }
  });

  onCleanup(() => clearTimeout(timeout));
  
  return (
    <Show when={message()}>
      <div class="toast toast-end z-50">
        <div class={`alert ${alertClass()}`}>
          <span>{message()}</span>
        </div>
      </div>
    </Show>
  );
}
