import { test, expect, vi } from "vitest";
import { render } from "@solidjs/testing-library"

import Contacts from "./Contacts";
import { FeedbackProvider } from "~/contexts/FeedbackContext";

import { server } from "~/mocks/node";
import { contactA } from "~/mocks/data";

server.listen()

const wrapper = (props) => (
  <FeedbackProvider>
    {props.children}
  </FeedbackProvider>
);

test("render contacts page", async () => {
  const { getByText } = render(() => <Contacts />, { wrapper });

  // Test loading state
  expect(getByText('Loading activities...')).toBeInTheDocument();
  
  // Wait for contacts to load
  await vi.waitUntil(() => Array.from(document.querySelectorAll('div[role="row"]')).slice(1).length > 0);
  expect(getByText(contactA.name)).toBeInTheDocument()

  // There should be 3 contacts
  const rows = Array.from(document.querySelectorAll('div[role="row"]')).slice(1);
  expect(rows.length).toBe(3);

  // NOTE: The contact contents are tested in ContactsTable.test.jsx
});
