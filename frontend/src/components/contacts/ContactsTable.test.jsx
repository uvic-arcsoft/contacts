import { test, expect, vi } from "vitest";
import { render } from "@solidjs/testing-library"
import ContactsTable from "./ContactsTable";
import { contactA, contactB, contactC } from "~/mocks/data";

const contacts = () => {
  return [contactA, contactB, contactC];
};

test("renders contacts table", async () => {
  render(() => <ContactsTable contacts={contacts} />);

  // Wait for the rows to be rendered
  await vi.waitUntil(() => document.querySelector('div[role="row"]'))
  
  // Remove the first header row
  const rows = Array.from(document.querySelectorAll('div[role="row"]')).slice(1);
  for (let i = 0; i < rows.length; i++) {
    const row = rows[i];
    const contact = contacts()[i];
    
    // Test contact name rendering
    const nameCell = row.querySelector('div[role="gridcell"][col-id="name"]').querySelector('a');
    expect(nameCell.getAttribute("href")).toBe(`/contacts/${contact.id}`);
    expect(nameCell.className).toContain('link');
    expect(nameCell.textContent).toBe(contact.name);

    // Test contact status rendering
    const statusCell = row.querySelector('div[role="gridcell"][col-id="status"]').querySelector('span');
    let badgeColor = '';
    switch (contact.status) {
      case 'initial/building':
        badgeColor = 'yellow';
        break;
      case 'active':
        badgeColor = 'green';
        break;
      case 'stalled':
        badgeColor = 'orange';
        break;
      case 'dormant':
        badgeColor = 'gray';
        break;
      default:
        badgeColor = 'gray';
    }
    let badgeClass = `badge-${badgeColor}`;
    expect(statusCell.className).toContain(`badge ${badgeClass}`);
    expect(statusCell.textContent).toBe(contact.status);

    // Test contact researchers rendering
    const researchersCell = row.querySelector('div[role="gridcell"][col-id="researchers"]').querySelector('ul');
    expect(researchersCell.children.length).toBe(contact.researchers.length);
    for (let j = 0; j < contact.researchers.length; j++) {
      const researcher = contact.researchers[j];
      const researcherLi = researchersCell.children[j];
      expect(researcherLi.tagName).toBe('LI');
      expect(researcherLi.textContent).toBe(researcher.name);
    }

    // Test contact projects rendering
    const projectsCell = row.querySelector('div[role="gridcell"][col-id="projects"]').querySelector('ul');
    expect(projectsCell.children.length).toBe(contact.projects.length);
    for (let j = 0; j < contact.projects.length; j++) {
      const project = contact.projects[j];
      const projectLi = projectsCell.children[j];
      expect(projectLi.tagName).toBe('LI');
      expect(projectLi.textContent).toBe(project.name);
    }

    // Test contact RCS members rendering
    const rcsMembersCell = row.querySelector('div[role="gridcell"][col-id="users"]').querySelector('ul');
    expect(rcsMembersCell.children.length).toBe(contact.rcs_members.length);
    for (let j = 0; j < contact.rcs_members.length; j++) {
      const rcsMember = contact.rcs_members[j];
      const rcsMemberLi = rcsMembersCell.children[j];
      expect(rcsMemberLi.tagName).toBe('LI');
      expect(rcsMemberLi.textContent).toBe(rcsMember.name);
    }

    // Test contact tags rendering
    const tagsCell = row.querySelector('div[role="gridcell"][col-id="tags"]').querySelector('div.flex.flex-col.gap-1');
    expect(tagsCell.children.length).toBe(contact.tags.length);
    for (let j = 0; j < contact.tags.length; j++) {
      const tag = contact.tags[j];
      const tagDiv = tagsCell.children[j];
      expect(tagDiv.tagName).toBe('DIV');
      expect(tagDiv.textContent).toContain(tag.name);
      expect(tagDiv.textContent).toContain(tag.description);
    }

    // Test contact last updated rendering
    const lastUpdatedCell = row.querySelector('div[role="gridcell"][col-id="last_updated"]');
    expect(new Date(lastUpdatedCell.textContent.trim())).toEqual(new Date(contact.last_updated));

    // Test contact actions rendering
    const actionsCell = row.querySelector('div[role="gridcell"][col-id="id"]').querySelector('div.flex.gap-5.items-center');
    const viewLink = actionsCell.children[0];
    expect(viewLink.tagName).toBe('A');
    expect(viewLink.getAttribute("href")).toBe(`/contacts/${contact.id}`);

    const editLink = actionsCell.children[1];
    expect(editLink.tagName).toBe('A');
    expect(editLink.getAttribute("href")).toBe(`/contacts/${contact.id}/edit`);

    const deleteButton = actionsCell.children[2];
    expect(deleteButton.tagName).toBe('BUTTON');
  }
});
