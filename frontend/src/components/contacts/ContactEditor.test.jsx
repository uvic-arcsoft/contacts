import { test, expect, vi } from "vitest";
import { render } from "@solidjs/testing-library"
import userEvent from "@testing-library/user-event";

import { server } from "~/mocks/node";
import { testUser,
        contactA, 
        projectX, projectY,
        researcherJohn, researcherJane, 
        memberA, tagTodo } from "~/mocks/data";

import { UserProvider } from "~/contexts/UserContext";
import { FeedbackProvider } from "~/contexts/FeedbackContext";
import FeedbackToast from "../toasts/FeedbackToast";
import ContactEditor from "./ContactEditor";

server.listen()
const user = userEvent.setup();

const wrapper = (props) => (
  <UserProvider>
    <FeedbackProvider>
      {props.children}
      <FeedbackToast />
    </FeedbackProvider>
  </UserProvider>
);

test("renders contact editor to create a new contact", async () => {
  // Add a researcher and a project by default
  window.localStorage.setItem('new-activity-researchers', JSON.stringify([researcherJohn]));
  window.localStorage.setItem('new-activity-projects', JSON.stringify([projectX]));

  const { getByText } = render(() => <ContactEditor />, { wrapper });

  // Wait for the new contact form to render
  await vi.waitUntil(() => document.querySelector('section'), {
    timeout: 10000,
  });

  // All the fields should be empty except RCS team members, which should have the current user by default
  expect(document.querySelector('textarea[name="synopsis"]').value).toBe('');
  expect(document.querySelector('select[name="status"]').value).toBe('initial/building');
  expect(Array.from(document.getElementById('projects').querySelectorAll('.tag')).length).toBe(1);
  expect(Array.from(document.getElementById('researchers').querySelectorAll('.tag')).length).toBe(1);
  expect(Array.from(document.getElementById('rcs-members').querySelectorAll('.tag')).length).toBe(1);
  expect(document.getElementById('rcs-members').querySelector('.tag').textContent).toContain(testUser.username);
  expect(getByText('No tags added yet')).toBeInTheDocument();

  // Test saving contact without a synopsis
  await user.click(getByText('Save'));
  expect(document.querySelector('.alert').textContent).toContain('Synopsis is required');

  // Test adding a synopsis
  const synopsis = contactA.synopsis;
  await user.type(document.querySelector('textarea[name="synopsis"]'), synopsis);
  expect(document.querySelector('textarea[name="synopsis"]').value).toBe(contactA.synopsis);

  // Test previewing the synopsis
  await user.click(getByText('Preview'));
  await vi.waitUntil(() => !document.querySelector('.markdown-body').className.includes('hidden'));
  expect(document.querySelector('.markdown-body').innerHTML).toContain(contactA.synopsis_html);

  // Test changing status to active
  const status = 'active';
  await user.selectOptions(document.querySelector('select[name="status"]'), status);
  expect(document.querySelector('select[name="status"]').value).toBe(status);

  // Test adding project Y
  const projectSearchbarContainer = document.getElementById('projects').parentElement;
  const projectSearchbar = projectSearchbarContainer.querySelector('input[placeholder="Search projects"]');
  await user.click(projectSearchbar);
  await vi.waitUntil(() => Array.from(projectSearchbarContainer.querySelectorAll('.menu.absolute.top-full.left-0 .menu-item')).length > 0);
  await user.click(Array.from(projectSearchbarContainer.querySelectorAll('.menu.absolute.top-full.left-0 .menu-item'))[1]);
  const projects = Array.from(document.getElementById('projects').querySelectorAll('.tag'));
  expect(projects.length).toBe(2);
  expect(projects[1].textContent).toContain(projectY.name);

  // Test adding researcher Jane Doe
  const researcherSearchbarContainer = document.getElementById('researchers').parentElement;
  const researcherSearchbar = researcherSearchbarContainer.querySelector('input[placeholder="Search researchers"]');
  await user.click(researcherSearchbar);
  await vi.waitUntil(() => Array.from(researcherSearchbarContainer.querySelectorAll('.menu.absolute.top-full.left-0 .menu-item')).length > 0);
  await user.click(Array.from(researcherSearchbarContainer.querySelectorAll('.menu.absolute.top-full.left-0 .menu-item'))[1]);
  const researchers = Array.from(document.getElementById('researchers').querySelectorAll('.tag'));
  expect(researchers.length).toBe(2);
  expect(researchers[1].textContent).toContain(researcherJane.name);

  // Test adding RCS team member Member A
  const rcsMemberSearchbarContainer = document.getElementById('rcs-members').parentElement;
  const rcsMemberSearchbar = rcsMemberSearchbarContainer.querySelector('input[placeholder="Search users"]');
  await user.click(rcsMemberSearchbar);
  await vi.waitUntil(() => Array.from(rcsMemberSearchbarContainer.querySelectorAll('.menu.absolute.top-full.left-0 .menu-item')).length > 0);
  await user.click(Array.from(rcsMemberSearchbarContainer.querySelectorAll('.menu.absolute.top-full.left-0 .menu-item'))[0]);
  expect(Array.from(document.getElementById('rcs-members').querySelectorAll('.tag')).length).toBe(2);
  expect(document.getElementById('rcs-members').querySelectorAll('.tag')[1].textContent).toContain(memberA.username);

  // Test adding a todo tag
  const tagForm = document.getElementById('tag-form');
  await user.click(document.getElementById('add-tag-btn'));
  await vi.waitUntil(() => !tagForm.classList.contains('hidden'));
  await user.type(tagForm.querySelector('input[name="tag-name"]'), tagTodo.name);
  await user.type(tagForm.querySelector('textarea[name="tag-description"]'), tagTodo.description);
  await user.click(tagForm.querySelector('button[type="submit"]'));
  const tags = Array.from(document.getElementById('tags').querySelectorAll('.bg-primary'));
  expect(tags.length).toBe(1);

  // Test deleting the todo tag
  await user.click(tags[0].querySelector('.fa-xmark'));
  await vi.waitUntil(() => document.body.textContent.includes('No tags added yet'));
});
