import { useContext } from "solid-js";
import { getCookie } from "~/helper";
import FeedbackContext from "~/contexts/FeedbackContext";

export default function DeleteContactModal(props) {
  const { setType, setMessage } = useContext(FeedbackContext);

  // Send a DELETE request to the server to delete the contact
  const deleteContact = async () => {
    try {
      const res = await fetch(`${import.meta.env.VITE_BACKEND_URL}/api/contacts/${props.contact()?.id}/delete/`, {
        method: "DELETE",
        credentials: "include",
        headers: {
          "Content-Type": "application/json",
          "X-CSRFToken": getCookie("csrftoken"),
        },
      });
      const data = await res.json();

      if (!res.ok) {
        setType("error");
        setMessage(data.error);
        return;
      }
      setType("success");
      setMessage(data.message);
      props.onDeleteContact(props.contact());
      document.getElementById(`delete-contact-modal-${props.contact()?.id}`).close();
    } catch (err) {
      setType("error");
      setMessage(err.message);
    }
  };

  return (
    <dialog id={`delete-contact-modal-${props.contact()?.id}`} class="modal">
      <div class="modal-box">
        <h3 class="font-bold text-lg">Delete activity</h3>
        <p class="py-4">
          Are you sure you want to delete an activity with 
          <strong>{props.contact()?.researchers.map(r => r.name).join(", ")}</strong> on <strong>{props.contact()?.created_at}</strong>?
        </p>
        <p class="text-sm text-gray-500">
          <strong>Note:</strong> Other RCS team members will no longer be able to see this activity.
        </p>
        <div class="modal-action">
          <form method="dialog">
            {/* if there is a button in form, it will close the modal */}
            <button type="button" class="btn btn-md btn-red mr-1" onClick={deleteContact}>Yes</button>
            <button class="btn btn-md btn-blue">No</button>
          </form>
        </div>
      </div>
      <form method="dialog" class="modal-backdrop">
        <button>close</button>
      </form>
    </dialog>
  );
}
