/*
  Display all activities that the current user has access to in a table format.
*/
import { Switch, Match, createResource, createSignal } from "solid-js";
import ContactsTable from "./ContactsTable";
import DeleteContactModal from "./DeleteContactModal";

// Fetch all activities from the backend
const getContacts = async () => {
  try {
    const res = await fetch(`${import.meta.env.VITE_BACKEND_URL}/api/contacts/`, {
      credentials: "include",
      headers: {
        "Content-Type": "application/json",
      },
    });
    const data = await res.json();

    if (!res.ok) {
      throw new Error(data.error);
    }
    return data.contacts;
  } catch (err) {
    throw new Error(err.message);
  }
}

// Display activities in a table format
export default function Contacts() {
  const [contacts, { mutate: setContacts }] = createResource(getContacts);
  const [selectedContact, setSelectedContact] = createSignal(null);

  const deleteContact = (deletedContact) => {
    const updatedContacts = contacts().filter(contact => contact.id !== deletedContact.id);
    setSelectedContact(null);
    setContacts(updatedContacts);
  }

  return (
    <main>
      <div class="flex flex-row justify-between">
        <h1 class="text-gray-900 text-xl md:text-3xl font-semibold">Recent activities</h1>
        <a href="/contacts/new" class="btn btn-md btn-blue"><i class="fa-solid fa-plus" /> New Activity</a>
      </div>
      <p class="text-gray-700 text-sm">Hover on the table header to sort and filter activities</p>

      <Switch>
        <Match when={contacts.loading}>
          <div class="flex flex-col items-center justify-center h-96">
            <p class="text-gray-500 flex gap-2 items-center text-xl p-32">
              <span class="loading loading-spinner loading-md" />
              Loading activities...
            </p>
          </div>
        </Match>
        <Match when={contacts.error}>
          <div class="flex flex-col items-center justify-center h-96">
            <p class="text-red-500">{contacts.error.message}</p>
          </div>
        </Match>
        <Match when={contacts()?.length === 0}>
          <div class="flex flex-col items-center justify-center h-96">
            <p class="text-gray-500">No activities found. Click on the <strong>+ New activity</strong> button to create one.</p>
          </div>
        </Match>
        <Match when={contacts().length > 0}>
          <section class="mt-4">
            <ContactsTable contacts={contacts} setSelectedContact={setSelectedContact} />
            <DeleteContactModal contact={selectedContact} onDeleteContact={deleteContact} />
          </section>
        </Match>
      </Switch>
    </main>
  )
}
