import { For, Show, createSignal, useContext, createEffect, mergeProps } from "solid-js";
import "github-markdown-css/github-markdown-light.css";

import { getCookie } from "~/helper";

import UserContext from "~/contexts/UserContext";
import FeedbackContext from "~/contexts/FeedbackContext";

import ProjectsSearchBar from "../searchbars/ProjectsSearchBar";
import ResearchersSearchBar from "../searchbars/ResearchersSearchBar";
import UsersSearchBar from "../searchbars/UsersSearchBar";
import ResearcherModal from "../researchers/ResearcherModal";
import ProjectModal from "../projects/ProjectModal";

export default function ContactEditor(_props) {
  const props = mergeProps({ contact: null }, _props);
  const { setType, setMessage } = useContext(FeedbackContext);
  const { user: currentUser } = useContext(UserContext);

  const [mode, setMode] = createSignal("edit");
  const [synopsisHtml, setSynopsisHtml] = createSignal("");
  const [selectedProjects, setSelectedProjects] = createSignal([]);
  const [selectedResearchers, setSelectedResearchers] = createSignal([]);
  const [selectedUsers, setSelectedUsers] = createSignal([]);

  const [tagFormOpen, setTagFormOpen] = createSignal(false);
  const [tags, setTags] = createSignal([]);

  createEffect(() => {
    if (props.contact === null) {
      // Add selected reseachers to a new contact by default
      // These researchers are selected by a previous action
      if (window.localStorage.getItem("new-activity-researchers")) {
        const researchers = JSON.parse(window.localStorage.getItem("new-activity-researchers"));
        setSelectedResearchers(researchers);
        window.localStorage.removeItem("new-activity-researchers");
      }

      // Add selected projects to a new contact by default
      // These projects are selected by a previous action
      if (window.localStorage.getItem("new-activity-projects")) {
        const projects = JSON.parse(window.localStorage.getItem("new-activity-projects"));
        setSelectedProjects(projects);
        window.localStorage.removeItem("new-activity-projects");
      }
    }
  })

  // Add the contact editor to the contact by default
  createEffect(() => {
    if (currentUser()) {
      setSelectedUsers([currentUser()]);
    }
  })

  // Set the existing contact data if it exists. Used in case of editing a contact
  createEffect(() => {
    if (props.contact !== null) {
      setSynopsisHtml(props.contact()?.synopsis_html);
      setSelectedProjects(props.contact()?.projects);
      setSelectedResearchers(props.contact()?.researchers);
      setSelectedUsers(props.contact()?.rcs_members);
      setTags(props.contact()?.tags);
    }
  })

  // Display Markdown preview
  const displayPreview = async() => {
    if (mode() === "preview") {
      try {
        const res = await fetch(`${import.meta.env.VITE_BACKEND_URL}/api/convert_markdown_to_html/`, {
          method: "POST",
          credentials: "include",
          headers: {
            "Content-Type": "application/json",
            'X-CSRFToken': getCookie("csrftoken")
          },
          body: JSON.stringify({ markdown: synopsisTextarea.value })
        });

        const data = await res.json();
        console.log(data);

        if (res.ok) {
          setSynopsisHtml(data.html);
        } else {
          setType("error");
          setMessage(data.error);
        }
      } catch (err) {
        console.error(err);
        setType("error");
        setMessage("Failed to preview Markdown. Server error!");
      }
    }
  }

  createEffect(() => {
    displayPreview();
  })

  let synopsisTextarea;
  let statusSelect;

  // Render a new tag
  const addTag = e => {
    e.preventDefault();
    const form = e.target;
    const name = form["tag-name"]?.value;
    const description = form["tag-description"]?.value;
    setTags([...tags(), {name, description}]);
    form.reset();
    setTagFormOpen(false);
  }

  // Send a POST request to the backend to save the contact
  const saveContact = async () => {
    const newContact = {
      synopsis: synopsisTextarea.value,
      status: statusSelect.value,
      projects: selectedProjects().map(p => p.id),
      researchers: selectedResearchers().map(r => r.id),
      users: selectedUsers().map(u => u.username),
      tags: tags()
    }

    // Validate the contact
    // Must have a synopsis
    if (!newContact.synopsis) {
      setType("error");
      setMessage("Failed to save contact. Synopsis is required");
      return;
    }

    // Must have at least one researcher
    if (newContact.researchers.length === 0) {
      setType("error");
      setMessage("Failed to save contact. At least one researcher is required");
      return;
    }

    // Must have at least one RCS team member
    if (newContact.users.length === 0) {
      setType("error");
      setMessage("Failed to save contact. At least one RCS team member is required");
      return;
    }

    // Send the POST request
    try {
      const url = props.contact !== null ? `${import.meta.env.VITE_BACKEND_URL}/api/contacts/${props.contact().id}/update/` : `${import.meta.env.VITE_BACKEND_URL}/api/contacts/create/`;
      const method = props.contact !== null ? "PUT" : "POST";
      const res = await fetch(url, {
        method: method,
        credentials: "include",
        headers: {
          "Content-Type": "application/json",
          'X-CSRFToken': getCookie("csrftoken")
        },
        body: JSON.stringify(newContact)
      })

      const data = await res.json();

      if (res.ok) {        
        // redirect to the new contact page
        window.location.href = `/contacts/${data.contact_id}`;
      } else {
        setType("error");
        setMessage(data.error);
      }
    } catch (err) {
      console.error(err);
      setType("error");
      setMessage("Failed to save contact. Server error!");
    }
  }

  return (
    <section class="mt-6 text-base">
      <div class="flex flex-col md:flex-row">

        {/* Main section */}
        <div class="w-full md:w-3/4">
          {/* Synopsis */}
          <div><label class="text-base font-semibold">Synopsis (<a class="link text-blue-600" href="https://www.markdownguide.org/cheat-sheet/" target="_blank">Markdown</a> supported) <span class="text-red-500">*</span></label></div>
          <div>
            <div class="join mt-3">
              <button
              type="button"
              class={`${mode() === "edit" ? "btn-blue" : ""} btn btn-md join-item`}
              onClick={() => setMode("edit")}>
                <i class="fa-solid fa-pen" />
                Edit
              </button>
              <button
              type="button"
              class={`${mode() === "preview" ? "btn-blue" : ""} btn btn-md join-item`}
              onClick={() => setMode("preview")}>
                <i class="fa-solid fa-eye" />
                Preview
              </button>
            </div>
          </div>
          <div class="mt-3">
            <textarea
              ref={synopsisTextarea}
              class={`${mode() === 'edit' ? "" : "hidden"} 
              textarea textarea-bordered w-full`}
              rows={15}
              placeholder="What was the discussion about? Be as specific as possible"
              name="synopsis"
              value={props.contact !== null ? props.contact()?.synopsis : ""} />
            <div class={`${mode() === 'preview' ? "" : "hidden"} markdown-body p-6 border border-gray-300 rounded-md`}>
              {/* eslint-disable solid/no-innerhtml */}
              {/* Disable solid/no-innerhtml: The HTML is fetched from Gitlab API so the HTML is sanitized properly. */}
              {/* Markdown preview goes here */}
              <div innerHTML={synopsisHtml()} />
              {/* eslint-enable solid/no-innerhtml */}
            </div>
          </div>

          {/* Status */}
          <div class="mt-5"><label class="text-base font-semibold">Status <span class="text-red-500">*</span></label></div>
          <select ref={statusSelect} name="status" value={props.contact === null ? "initial/building" : props.contact()?.status} class="select select-bordered w-full max-w-xs mt-3">
            <option value="initial/building">initial/building</option>
            <option value="active">active</option>
            <option value="stalled">stalled</option>
            <option value="dormant">dormant</option>
          </select>

          {/* Projects */}
          <div class="mt-5">
            <label class="text-base font-semibold">Projects</label>
            <button class="btn btn-outline btn-sm btn-blue ml-2" onClick={()=>document.getElementById('project-modal').showModal()}>
              <i class="fa-solid fa-plus" />
              New project
            </button>
            <ProjectModal onSaveProject={(p) => setSelectedProjects([...selectedProjects(), p])} disableResearcherCreation={true}/>
          </div>
          <div class="mt-3">
            <ProjectsSearchBar onSelect={(project) => {
              if (!selectedProjects().find(p => p.id === project.id)) {
                setSelectedProjects([...selectedProjects(), project]);
              }
            }}/>
            
            <div id="projects" class="flex flex-row gap-2 flex-wrap mt-3">
              <For each={selectedProjects()}>
                {(project) => (
                  <div class="tag flex flex-row gap-2">
                    <a class="link">{project?.name}</a>
                    <button onClick={() => setSelectedProjects(selectedProjects().filter(p => p !== project))}>
                      <i class="fa-solid fa-xmark text-gray-400 hover:text-gray-700" />
                    </button>
                  </div>
                )}
              </For>
            </div>
          </div>

          {/* Researchers */}
          <div class="mt-5">
            <label class="text-base font-semibold">Researchers <span class="text-red-500">*</span></label>
            <button class="btn btn-outline btn-sm btn-blue ml-2" onClick={()=>document.getElementById('researcher-modal').showModal()}>
              <i class="fa-solid fa-plus" />
              New researcher
            </button>
            <ResearcherModal onSaveResearcher={(r) => setSelectedResearchers([...selectedResearchers(), r])}/>
          </div>
          <div class="mt-3">
            <ResearchersSearchBar onSelect={(researcher) => {
              if (!selectedResearchers().find(r => r.id === researcher.id)) {
                setSelectedResearchers([...selectedResearchers(), researcher])
              }
            }}/>
            
            <div id="researchers" class="flex flex-row gap-2 flex-wrap mt-3">
              <For each={selectedResearchers()}>
                {(researcher) => (
                  <div class="tag flex flex-row gap-2">
                    <a class="link">{researcher?.name}</a>
                    <button onClick={() => setSelectedResearchers(selectedResearchers().filter(r => r !== researcher))}>
                      <i class="fa-solid fa-xmark text-gray-400 hover:text-gray-700" />
                    </button>
                  </div>
                )}
              </For>
            </div>
          </div>

          {/* RCS team members */}
          <div class="mt-5">
            <label class="text-base font-semibold">RCS team members <span class="text-red-500">*</span></label>
          </div>
          <div class="mt-3">
            <UsersSearchBar onSelect={(user) => {
              if (!selectedUsers().find(u => u.username === user.username)) {
                setSelectedUsers([...selectedUsers(), user])
              }
            }}/>
            <p class="mt-2 text-sm text-gray-700"><strong>Note: </strong>Members added here will be able to view and edit the activity</p>
            
            <div id="rcs-members" class="flex flex-row gap-2 flex-wrap mt-3">
              <For each={selectedUsers()}>
                {(user) => (
                  <div class="tag flex flex-row gap-2">
                    <span>{user?.username}</span>
                    <button onClick={() => setSelectedUsers(selectedUsers().filter(u => u !== user))}>
                      <Show when={user?.username !== currentUser()?.username}>
                        <i class="fa-solid fa-xmark text-gray-400 hover:text-gray-700" />
                      </Show>
                    </button>
                  </div>
                )}
              </For>
            </div>
          </div>
        </div>

        <div class="divider divider-horizontal hidden md:flex" />
        
        {/* Right sidebar section */}
        <div class="w-full md:w-1/4 mt-5 md:mt-0">
          {/* Tags */}
          <div class="flex md:justify-between gap-2 md:gap-0 items-center">
            <label class="text-base font-semibold">Tags</label>
            <Show when={!tagFormOpen()}>
              <button id="add-tag-btn" type="button" class="btn btn-outline btn-sm btn-blue" onClick={() => setTagFormOpen(!tagFormOpen())}>
                <i class="fa-solid fa-plus" />
                Add Tag
              </button>
            </Show>
          </div>

          <form id="tag-form" class={`mt-3 mb-6 ${tagFormOpen() ? "" : "hidden"}`} onSubmit={addTag}>
            <input name="tag-name" type="text" class="input input-bordered w-full" placeholder="Enter tag name" required/>
            <textarea name="tag-description" class="textarea textarea-bordered w-full mt-2" rows={3} placeholder="Enter tag description" />
            <div class="mt-2">
              <button type="submit" class="btn btn-sm btn-blue">
                <i class="fa-solid fa-save" />
                Save Tag
              </button>
              <button type="button" class="btn btn-sm btn-red ml-1" onClick={() => setTagFormOpen(!tagFormOpen())}>
                <i class="fa-solid fa-ban" />
                Cancel
              </button>
            </div>
          </form>

          <Show when={tags().length > 0} fallback={
            <div class="text-gray-500 mt-3">No tags added yet</div>
          }>
            <div id="tags" class="flex flex-row gap-1 flex-wrap mt-3">
              <For each={tags()}>
                {(tag) => (
                  <div class="bg-primary text-gray-100 rounded-md px-2 py-1 text-sm flex items-center gap-1">
                    <span class="flex-grow">{tag?.name}{tag?.description ? ":" : ""} {tag?.description}</span>
                    <button class="text-gray-100 hover:text-primary rounded-full hover:bg-gray-100 w-4 h-4 text-center flex flex-col justify-center" onClick={() => setTags(tags().filter(t => t !== tag))}>
                      <i class="fa-solid fa-xmark mx-auto" />
                    </button>
                  </div>
                )}
              </For>
            </div>
          </Show>
        </div>
      </div>

      {/* Action Buttons */}
      <div class="mt-8">
        <button id="save-btn" class="btn btn-blue" onClick={saveContact}>
          <i class="fa-solid fa-save" />
          Save
        </button>
        <a id="cancel-btn" href={props.contact !== null ? `/contacts/${props.contact()?.id}` : "/contacts"} class="btn btn-red ml-2">
          <i class="fa-solid fa-ban" />
          Cancel
        </a>
      </div>
    </section>
  )
}
