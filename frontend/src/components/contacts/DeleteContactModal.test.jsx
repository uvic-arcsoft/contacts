import { test, expect } from "vitest";
import { render } from "@solidjs/testing-library"

import { contactA } from "~/mocks/data";
import { FeedbackProvider } from "~/contexts/FeedbackContext";
import DeleteContactModal from "./DeleteContactModal";

const wrapper = (props) => (
  <FeedbackProvider>
    {props.children}
  </FeedbackProvider>
);

test("render contact delete modal", async () => {
  const { getByText } = render(() => <DeleteContactModal contact={() => contactA} />, { wrapper });

  // Test modal content
  expect(getByText('Delete activity')).toBeInTheDocument();
  const ps = Array.from(document.querySelectorAll('p'));
  expect(ps[0].innerHTML).toContain(`Are you sure you want to delete an activity with <strong>${contactA.researchers.map(r => r.name).join(", ")}</strong> on <strong>${contactA.created_at}</strong>?`);
  expect(ps[1].textContent).toContain('Other RCS team members will no longer be able to see this activity.');
  expect(document.querySelector('button.btn-red').textContent).toBe('Yes');
  expect(document.querySelector('button.btn-blue').textContent).toBe('No');
});
