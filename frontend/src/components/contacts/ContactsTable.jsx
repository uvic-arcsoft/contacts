import { createSignal, createEffect, For, Show } from 'solid-js';
import AgGridSolid from 'ag-grid-solid';
import "ag-grid-community/styles/ag-grid.css"; // Mandatory CSS required by the grid
import "ag-grid-community/styles/ag-theme-quartz.css"; // Optional Theme applied to the grid

const ContactLink = (params) => {
  return (
    <Show when={params.colDef.field === 'name'} fallback={
      <a href={`/contacts/${params.data.id}`} class='w-full h-full flex items-center'>{params.value}</a>
    }>
      <a href={`/contacts/${params.data.id}`} class='w-full h-full flex items-center font-semibold link'>{params.value}</a>
    </Show>
  )
}

const ContactStatus = (params) => {
  const [badgeClass, setBadgeClass] = createSignal('badge-gray');
  
  createEffect(() => {
    let badgeColor = '';
    switch (params.value) {
      case 'initial/building':
        badgeColor = 'yellow';
        break;
      case 'active':
        badgeColor = 'green';
        break;
      case 'stalled':
        badgeColor = 'orange';
        break;
      case 'dormant':
        badgeColor = 'gray';
        break;
      default:
        badgeColor = 'gray';
    }
    setBadgeClass(`badge-${badgeColor}`);
  });

  return (
    <span class={`badge ${badgeClass()}`}>{params.value}</span>
  )
}

const ContactObjField = (params) => {
  return (
    <ul class='list-disc py-4'>
      <For each={JSON.parse(params.value)}>
        {i => (
          <li>{i.name}</li>
        )}
      </For>
    </ul>
  )
}

const ContactTags = (params) => {
  return (
    <div class="flex flex-col gap-1">
      <For each={JSON.parse(params.value)}>
        {(tag) => (
          <div class="bg-primary text-gray-100 rounded-md px-2 py-1 text-sm flex items-center gap-1">
            <span class="flex-grow">{tag?.name}{tag?.description ? ":" : ""} {tag?.description}</span>
          </div>
        )}
      </For>
    </div>
  )
}

const ContactActions = (params) => {
  return (
    <div class="flex gap-5 items-center">
      <a href={`/contacts/${params.value}`}><i class="fa-solid fa-eye text-green-700 hover:text-green-800" /></a>
      <a href={`/contacts/${params.value}/edit`}><i class="fa-solid fa-pen-to-square text-blue-500 hover:text-blue-700" /></a>
      <button onClick={() => {
        params.data.onSelect();
        document.getElementById(`delete-contact-modal-${params.value}`).showModal();
      }}><i class="fa-solid fa-trash-can text-red-500 hover:text-red-700" /></button>
    </div>
  )
}

{/* eslint-disable solid/reactivity */}
{/* Disable solid/reactivity: AGGrid table doesn't render properly with rowData as a signal */}
export default function ContactsTable(props) {
  const contacts = props.contacts();
  const setSelectedContact = props.setSelectedContact;
  let gridRef;

  const columnDefs = [
    { headerName: "Name", field: "name", cellRenderer: ContactLink },
    { headerName: "Status", field: "status", cellRenderer: ContactStatus },
    { headerName: "Researchers", field: "researchers", cellRenderer: ContactObjField, autoHeight: true },
    { headerName: "Projects", field: "projects", cellRenderer: ContactObjField, autoHeight: true },
    { headerName: "RCS members", field: "users", cellRenderer: ContactObjField, autoHeight: true },
    { headerName: "Tags", field: "tags", cellRenderer: ContactTags, autoHeight: true },
    { headerName: "Last updated", field: "last_updated" },
    { headerName: "Actions", field: "id", cellRenderer: ContactActions },
  ]

  let rowData = contacts.map(contact => ({
    id: contact.id,
    name: contact.name,
    status: contact.status,
    researchers: JSON.stringify(contact?.researchers),
    projects: JSON.stringify(contact?.projects),
    users: JSON.stringify(contact?.rcs_members),
    tags: JSON.stringify(contact.tags),
    last_updated: contact.last_updated,
    onSelect: () => {
      setSelectedContact(contact);
    }
  }));

  // Update contacts table when there is any change to the contacts
  createEffect(() => {
    rowData = props.contacts().map(contact => ({
      id: contact.id,
      name: contact.name,
      status: contact.status,
      researchers: JSON.stringify(contact?.researchers),
      projects: JSON.stringify(contact?.projects),
      users: JSON.stringify(contact?.rcs_members),
      tags: JSON.stringify(contact.tags),
      last_updated: contact.last_updated,
      onSelect: () => {
        setSelectedContact(contact);
      }
    }));
    gridRef?.api?.setRowData(rowData);
  });

  // Allow filtering and sorting on all columns
  const defaultColDef = {
    filter: true,
    sortable: true,
  };

  // Scale each column to fit all cell contents
  const autoSizeStrategy = {
    type: 'fitCellContents'
  };

  return (
    <div class="ag-theme-quartz h-[70vh] min-w-[700px]">
      <AgGridSolid
        ref={gridRef}
        rowData={rowData}
        columnDefs={columnDefs}
        defaultColDef={defaultColDef}
        rowHeight={50}
        pagination={true}
        paginationPageSize={10}
        paginationPageSizeSelector={[10, 25, 50]}
        autoSizeStrategy={autoSizeStrategy}
      />
    </div>
  )
}
{/* eslint-enable solid/reactivity */}
