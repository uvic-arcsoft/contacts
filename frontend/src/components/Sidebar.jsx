import { A } from "@solidjs/router"

export default function Sidebar(props) {  
  return (
    <div class="drawer lg:drawer-open">
      <input id="my-drawer-2" type="checkbox" class="drawer-toggle" />
      <div class="drawer-content px-2 py-4 sm:p-6">
        {/* Page content here */}
        {props.children}
      </div>
      <div class="drawer-side">
        <label for="my-drawer-2" aria-label="close sidebar" class="drawer-overlay" />
        <ul class="menu bg-base-200 text-gray-800 min-h-full w-52 p-4 pt-8 border-r border-gray-300">
          {/* Sidebar content here */}
          <li><A href="/researchers" activeClass="active"><i class="fa-solid fa-book-open-reader mr-3" /> Researchers</A></li>
          <li><A href="/contacts" activeClass="active"><i class="fa-solid fa-address-book mr-3" /> Recent activities</A></li>
          <li><A href="/projects" activeClass="active"><i class="fa-solid fa-diagram-project mr-3" /> Projects</A></li>
          <li><A href="/artifacts" activeClass="active"><i class="fa-solid fa-gears mr-3" /> Artifacts</A></li>
          <li><A href="/softwares" activeClass="active"><i class="fa-solid fa-laptop mr-3" /> Softwares</A></li>
          <li><A href="/usage" activeClass="active"><i class="fa-solid fa-sitemap mr-3" /> Usage</A></li>
        </ul>
      </div>
    </div>
  )
}
