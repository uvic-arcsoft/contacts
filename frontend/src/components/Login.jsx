export default function Login() {
  return (
    <main class="flex gap-2 items-center text-xl p-32">
      <span class="loading loading-spinner loading-md" />
      Logging in...
    </main>
  )
}
