import { test, expect, vi } from "vitest";
import { render } from "@solidjs/testing-library"
import { Router } from "@solidjs/router";

import { server } from "~/mocks/node";

import { testUser } from "~/mocks/data";
import { UserProvider } from "~/contexts/UserContext";
import Navbar from "./Navbar";

server.listen()

const wrapper = (props) => (
  <Router
    root={() => (
      <UserProvider>
        {props.children}
      </UserProvider>
    )}
   />
);

test("Renders navbar", async () => {
  const { getByText } = render(() => <Navbar />, { wrapper });
  await vi.waitUntil(() => document.querySelector('nav.navbar'));

  // Test app name
  expect(getByText('Recon')).toBeInTheDocument();

  // Test user profile picture and name
  expect(document.querySelector('img[alt="User avatar"]').src).toContain(testUser.image);
  expect(document.querySelector('.navbar-end').innerHTML).toContain(`Hello <strong>${testUser.name}</strong>`);

  // Test navigation links
  expect(document.querySelector('a[href="/researchers"]').textContent).toContain('Researchers');
  expect(document.querySelector('a[href="/contacts"]').textContent).toContain('Recent Activities');
  expect(document.querySelector('a[href="/projects"]').textContent).toContain('Projects');
  expect(document.querySelector('a[href="/artifacts"]').textContent).toContain('Artifacts');
  expect(document.querySelector('a[href="/softwares"]').textContent).toContain('Softwares');
  expect(document.querySelector('a[href="/usage"]').textContent).toContain('Usage');
});
