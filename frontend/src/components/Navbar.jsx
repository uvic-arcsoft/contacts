import { useContext } from "solid-js"
import { A } from "@solidjs/router"
import UserContext from "~/contexts/UserContext"
import ContactsSearchBar from "./searchbars/ContactsSearchBar"

export default function Navbar() {
  const {user} = useContext(UserContext);

  return (
    <nav class="navbar bg-base-100 border-b border-gray-300">
      <div class="navbar-start">
        {/* Mobile navbar */}
        <div class="dropdown">
          <div tabIndex={0} role="button" class="btn btn-ghost lg:hidden">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              class="h-5 w-5"
              fill="none"
              viewBox="0 0 24 24"
              stroke="currentColor">
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth="2"
                d="M4 6h16M4 12h8m-8 6h16" />
            </svg>
          </div>
          <ul
            tabIndex={0}
            class="menu menu-sm dropdown-content bg-base-100 rounded-box z-[1] mt-3 w-52 p-2 shadow">
            <li><A href="/researchers" activeClass="active"><i class="fa-solid fa-book-open-reader mr-3" /> Researchers</A></li>
            <li><A href="/contacts" activeClass="active"><i class="fa-solid fa-address-book mr-3" /> Recent Activities</A></li>
            <li><A href="/projects" activeClass="active"><i class="fa-solid fa-diagram-project mr-3" /> Projects</A></li>
            <li><A href="/artifacts" activeClass="active"><i class="fa-solid fa-gears mr-3" /> Artifacts</A></li>
            <li><A href="/softwares" activeClass="active"><i class="fa-solid fa-laptop mr-3" /> Softwares</A></li>
            <li><A href="/usage" activeClass="active"><i class="fa-solid fa-sitemap mr-3" /> Usage</A></li>
          </ul>
        </div>

        {/* Logo */}
        <a href="/" className="btn btn-ghost text-xl">Recon</a>
      </div>

      {/* Desktop */}
      <div class="navbar-center flex sm:w-[400px] lg:w-[700px] grow">
        <ContactsSearchBar onSelect={(contact) => window.location.href = `/contacts/${contact?.id}`}/>
      </div>
      <div class="navbar-end">
        <div class="dropdown dropdown-end">
          <div tabIndex={0} role="button" class="btn btn-ghost btn-circle avatar">
            <div class="w-10 rounded-full">
              <img
                alt="User avatar"
                src={user()?.image} />
            </div>
          </div>
          <ul
            tabIndex={0}
            class="menu dropdown-content bg-base-100 rounded-box z-[1] mt-3 w-52 p-2 shadow">
            <li><a>Hello <strong>{user()?.name}</strong></a></li>
          </ul>
        </div>
      </div>
    </nav>
  )
}
