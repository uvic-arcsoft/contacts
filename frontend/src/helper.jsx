/**
* Retrieves the value of a specified cookie by name.
*
* This function splits the document.cookie string into individual cookies and then looks
* for a cookie that matches the given name. If it finds the cookie, it returns its value;
* otherwise, it returns null. This approach avoids regular expressions for easier debugging
* and clarity.
*
* @param {string} name - The name of the cookie to retrieve.
* @returns {string|null} The value of the cookie if found, null otherwise.
*/
export const getCookie = name => {
  // Ensure the cookie name is safe to use by encoding it
  const encodedName = encodeURIComponent(name) + '=';

  // Split document.cookie into an array of cookies
  const cookies = document.cookie.split(';');

  // Iterate through the array looking for the named cookie
  for (let i = 0; i < cookies.length; i++) {
    let cookie = cookies[i].trim();

    // Check if this cookie string begins with the name we want
    if (cookie.startsWith(encodedName)) {
      return decodeURIComponent(cookie.substring(encodedName.length));
    }
  }

  // Return null if the cookie was not found
  return null;
}

/**
* Truncate a string to a certain length and append an ellipsis if it exceeds the limit.
* 
* @param {string} text - The text to truncate.
* @param {number} length
*/
export const truncate = (text, length) => {
  return text.length > length ? text.substring(0, length) + '...' : text;
}
