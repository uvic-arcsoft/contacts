import { createContext, createResource, createEffect, Switch, Match } from "solid-js";
import { getRequestEvent } from "solid-js/web";
import Login from "~/components/Login";

const UserContext = createContext();
export default UserContext;

const login = async () => {
  try {
    const res = await fetch(`${import.meta.env.VITE_BACKEND_URL}/auth/login/`, {
      credentials: "include",
      headers: {
        "Content-Type": "application/json",
        "X-Forwarded-User": getRequestEvent()?.request.headers.get("X-Forwarded-User"),
      },
    });
    const data = await res.json();
    
    if (!res.ok) {
      throw new Error(data.error);
    }
    return data.user;
  } catch (err) {
    throw new Error(err.message);
  }
}

export const UserProvider = (props) => {
  const [user] = createResource(login);

  // NOTE: Used for debugging purposes. Print out user info.
  createEffect(() => {
    console.log("User info:");
    console.log(user());
  });

  return (
    <Switch>
      <Match when={user.loading}>
        <Login/>
      </Match>
      <Match when={user.error}>
        <p class="text-red-500">{user.error.message}</p>
      </Match>
      <Match when={!user()}>
        <p class="text-red-500">User is unauthorized to access app</p>
      </Match>
      <Match when={user()}>
        <UserContext.Provider value={{ user }}>
          {props.children}
        </UserContext.Provider>
      </Match>
    </Switch>

  );
}
