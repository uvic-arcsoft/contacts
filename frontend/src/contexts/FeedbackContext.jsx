import { createContext, createSignal } from "solid-js";

const FeedbackContext = createContext();
export default FeedbackContext;

export const FeedbackProvider = (props) => {
  // type are DaisyUI colors, which are: "primary", "secondary", "accent", "neutral", "success", "warning", "error", "info", etc
  const [type, setType] = createSignal("success");
  const [message, setMessage] = createSignal("");

  return (
    <FeedbackContext.Provider value={{
      type: type,
      setType: setType,
      message: message,
      setMessage: setMessage
    }}>
      {props.children}
    </FeedbackContext.Provider>
  );
}
