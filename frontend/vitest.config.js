import solid from "vite-plugin-solid"
import { defineConfig } from "vitest/config"
import path from "path"

export default defineConfig({
  plugins: [solid()],
  resolve: {
    alias: {
      "~": path.resolve(__dirname, "./src"),
    },
    conditions: ["development", "browser"],
  },
  test: {
    coverage: {
      provider: "v8",
      exclude: ["**/entry-*.jsx", "**/global.d.js", "**/app.jsx", "**/mocks", "**/*.test.jsx", "**/helper.jsx"],
      include: ["**/src/*"],
      reporter: ["text", "text-summary"],
    }
  }
})
