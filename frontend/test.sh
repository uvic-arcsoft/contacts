#!/bin/sh
npm run coverage
RESULT=$(npm run coverage | grep "^Lines" | awk '{print $3}' | head -n 1)
echo "Code coverage: $RESULT"
