import js from "@eslint/js";
import solid from "eslint-plugin-solid/configs/recommended";

export default [
  js.configs.recommended, // replaces eslint:recommended
  solid, {
    files: ["src/**/*.{js,jsx}"],
    languageOptions: {
      globals: {
        window: "readonly",
        document: "readonly",
        fetch: "readonly",
        console: "readonly",
        clearTimeout: "readonly",
        setTimeout: "readonly",
        URL: "readonly",
      }
    }
  }
];
