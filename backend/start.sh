#!/bin/sh
# Migrate schema to the database
python manage.py migrate

# Collect static files
python manage.py collectstatic --no-input

# Start Gunicorn server
gunicorn -w 4 rcdb.wsgi:application --bind 0.0.0.0:4001
