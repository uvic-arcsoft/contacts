# pylint:
from django.db import models
from django.contrib.auth.models import User

class Profile(models.Model):
    """
    Richer information about an user
    """
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    bio = models.TextField(max_length=500, default='', blank=True)
    image = models.CharField(max_length=500, default='/img/logo.png', blank=True)

    class Meta:
        db_table = 'profiles'

    def __str__(self):
        return f'{self.user.username} Profile'

    def to_dict(self):
        return {
            'bio': self.bio,
            'image': self.image
        }
