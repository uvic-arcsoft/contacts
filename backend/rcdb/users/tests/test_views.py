# pylint:
from django.test import TestCase, Client
from django.contrib.auth.models import User
from django.urls import reverse
from users.models import Profile

class ViewTestCase(TestCase):
    """
    General set up for view unit tests
    """
    def setUp(self):
        # Create an initial user
        self.user = User.objects.create_user(
            first_name='Test',
            last_name='User',
            username='user@example.org',
            email='user@example.org',
            password='testpassword'
        )

        # Create a client that can be authenticated
        self.client = Client(HTTP_X_FORWARDED_USER='user@example.org')

        # Create a client that cannot be authenticated
        self.client_unauth = Client(HTTP_X_FORWARDED_USER='doesnotexist@example.org')

class LoginViewTest(ViewTestCase):
    """
    Test the LoginView
    """
    def test_get(self):
        """
        Log an user in by checking X-Forwarded-User header
        """
        response = self.client.get(reverse('users:login'))
        self.assertEqual(response.status_code, 200)
        data = response.json()
        user = data['user']
        self.assertEqual(user['name'], 'Test User')
        self.assertEqual(user['username'], 'user@example.org')
        self.assertEqual(user['first_name'], 'Test')
        self.assertEqual(user['last_name'], 'User')
        self.assertEqual(user['email'], 'user@example.org')
        self.assertEqual(user['bio'], '')
        self.assertEqual(user['image'], '/img/logo.png')

        # A new profile should be created for the user
        self.assertEqual(Profile.objects.count(), 1)
        profile = Profile.objects.first()
        self.assertEqual(profile.user, self.user)
        self.assertEqual(profile.bio, '')
        self.assertEqual(profile.image, '/img/logo.png')

        # User is logged in properly
        self.assertTrue(response.wsgi_request.user.is_authenticated)
        self.assertEqual(response.wsgi_request.user, self.user)

        # User does not exist in the system
        response = self.client_unauth.get(reverse('users:login'))
        self.assertEqual(response.status_code, 404)

class LogoutViewTest(ViewTestCase):
    """
    Test the LogoutView
    """
    def test_get(self):
        """
        Log an user out
        """
        # Log in user
        response = self.client.get(reverse('users:login'))
        self.assertTrue(response.wsgi_request.user.is_authenticated)

        # Log out user
        response = self.client.get(reverse('users:logout'))
        self.assertEqual(response.status_code, 200)
        self.assertFalse(response.wsgi_request.user.is_authenticated)

class UsersAPIView(ViewTestCase):
    """
    Test the UsersAPIView
    """
    def test_get(self):
        """
        Get users from the system
        """
        # Get all users without logging in
        response = self.client_unauth.get(reverse('users:get_users'))
        self.assertEqual(response.status_code, 401)

        # Log in user
        response = self.client.get(reverse('users:login'))

        # Get all users
        response = self.client.get(reverse('users:get_users'))
        self.assertEqual(response.status_code, 200)
        users = response.json()['users']
        self.assertEqual(len(users), 1)
        self.assertIn({
            'id': self.user.id,
            'username': self.user.username,
            'first_name': self.user.first_name,
            'last_name': self.user.last_name,
            'email': self.user.email
        }, users)

        # Search users by name or username
        response = self.client.get(reverse('users:get_users'), {'query': 'test'})
        self.assertEqual(response.status_code, 200)
        users = response.json()['users']
        self.assertEqual(len(users), 1)
        self.assertIn({
            'id': self.user.id,
            'username': self.user.username,
            'first_name': self.user.first_name,
            'last_name': self.user.last_name,
            'email': self.user.email
        }, users)

        # Search users by a non-existent query
        response = self.client.get(reverse('users:get_users'), {'query': 'doesnotexist'})
        self.assertEqual(response.status_code, 200)
        users = response.json()['users']
        self.assertEqual(len(users), 0)
