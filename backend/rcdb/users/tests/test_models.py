# pylint:
from django.test import TestCase
from django.contrib.auth.models import User
from users.models import Profile

class ModelTestCase(TestCase):
    """
    General set up for model unit tests
    """
    def setUp(self):
        # Create an initial user
        self.user = User.objects.create_user(
            first_name='Test',
            last_name='User',
            username='testuser',
            password='testpassword',
            email='testuser@example.org'
        )

        # Create a test profile
        self.profile = Profile.objects.create(
            user=self.user,
            bio='Test bio',
            image='/img/test.png'
        )

class ProfileModelTest(ModelTestCase):
    """
    Test the Profile model
    """
    def test_create(self):
        """
        Test profile creation
        """
        self.assertEqual(self.profile.user.username, 'testuser')
        self.assertEqual(self.profile.bio, 'Test bio')
        self.assertEqual(self.profile.image, '/img/test.png')

    def test_delete(self):
        """
        Test profile deletion
        """
        self.profile.delete()
        self.assertEqual(Profile.objects.count(), 0)

    def test_str(self):
        """
        Test __str__ method
        """
        self.assertEqual(str(self.profile), 'testuser Profile')

    def test_to_dict(self):
        """
        Test to_dict method
        """
        self.assertEqual(self.profile.to_dict(), {
            'bio': 'Test bio',
            'image': '/img/test.png'
        })
