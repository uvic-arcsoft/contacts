# pylint:
import jwt

from django.contrib.auth.models import User
from django.contrib.auth import login, logout
from django.conf import settings
from django.views import View
from django.http import HttpRequest, JsonResponse
from django.db.models import Q

from api.decorators import logged_in_or_401
from users.models import Profile

class LoginView(View):
    def get(self, request: HttpRequest) -> JsonResponse:
        """
        Log an user in by checking X-Forwarded-User header
        """
        user = None
        # If user is already logged in, return user info
        if request.user.is_authenticated:
            x_forwarded_user = request.headers.get("X-Forwarded-User", None)

            if x_forwarded_user and request.user.username == x_forwarded_user:
                user = request.user

            # Log out user if the username does not match
            else:
                logout(request)

        if not user:
            username = None
            # For testing, can edit 'AUTHX_USER_OVERRIDE' to
            # replace the HTTP request headers in settings.py
            if settings.DEBUG and settings.CONFIG.get('AUTHX_USER_OVERRIDE'):
                username = settings.CONFIG.get('AUTHX_USER_OVERRIDE')

            # Get username from 'X-Forwarded-User' header
            else:
                username = request.headers.get("X-Forwarded-User", None)

            if not username:
                return JsonResponse({
                    'error': 'User not logged in. Username is not provided.'
                }, status=401)

            # Authenticate user with Django
            try:
                user = User.objects.get(username=username)
            except User.DoesNotExist:
                return JsonResponse({
                    'error': 'User not found in the system'
                }, status=404)

        # Create an user's profile if not already
        if not hasattr(user, 'profile'):
            user.profile = Profile.objects.create(user=user)
            user.save()

        # Populate user profile with ID Token received from authentication service
        id_token = request.headers.get('X-Forwarded-Id-Token', None)
        if id_token:
            id_token = id_token.replace("Bearer ", "")
            user_info = jwt.decode(id_token, options={"verify_signature": False})
            user.first_name = user_info.get('given_name', '')
            user.last_name = user_info.get('family_name', '')
            user.email = user_info.get('email', '')
            user.save()

            user.profile.image = user_info.get('picture', '/img/logo.png')
            user.profile.save()

        login(request, user)

        # Return a success response with user info
        return JsonResponse({
            'message': 'User logged in',
            'user': {
                'name': f'{user.first_name} {user.last_name}' if user.first_name else user.username,
                'username': user.username,
                'first_name': user.first_name,
                'last_name': user.last_name,
                'email': user.email,
                **user.profile.to_dict()
            }
        }, status=200)

# Logging users out
# View class: https://docs.djangoproject.com/en/4.1/ref/class-based-views/base/
class LogoutView(View):
    def get(self, request: HttpRequest) -> JsonResponse:
        """
        Log an user out
        """
        # For TESTING ONLY
        if settings.DEBUG:
            settings.CONFIG['AUTHX_USER_OVERRIDE'] = ""

        logout(request)

        return JsonResponse({
            'message': 'User logged out'
        }, status=200)

class UsersAPIView(View):
    """
    API endpoints for users
    """
    @logged_in_or_401
    def get(self, request: HttpRequest) -> JsonResponse:
        """ Get users """
        # Search for users by name or username if a query is provided
        query = request.GET.get('query', None)
        if query:
            users = User.objects.filter(
                Q(username__icontains=query) |
                Q(first_name__icontains=query) |
                Q(last_name__icontains=query))
        else:
            users = User.objects.all()

        return JsonResponse({
            'users': [{
                'id': user.id,
                'username': user.username,
                'first_name': user.first_name,
                'last_name': user.last_name,
                'email': user.email
            } for user in users]
        }, status=200)
