#!/bin/sh
# Migrate schema to the database
python manage.py migrate

# Start Gunicorn server
gunicorn -w 4 --chdir /app rcdb.wsgi:application --bind 0.0.0.0:8000 --timeout 1000
