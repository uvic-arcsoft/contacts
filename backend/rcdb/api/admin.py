# pylint:
from django.contrib import admin
from .models import Contact, Project, Researcher, Tag, Artifact, Software, Usage

admin.site.register(Contact)
admin.site.register(Project)
admin.site.register(Researcher)
admin.site.register(Tag)
admin.site.register(Artifact)
admin.site.register(Software)
admin.site.register(Usage)
