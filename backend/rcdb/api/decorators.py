# pylint:
from typing import Any, Dict, Union
from functools import wraps

from django.http import JsonResponse, HttpRequest

def logged_in_or_401(view):
    """
    Decorator to ensure that the user is authenticated before accessing a view.
    """
    @wraps(view)
    def wrapper(
        self,
        request: HttpRequest,
        *args: Any,
        **kwargs: Dict[str, Any]
    ) -> Union[JsonResponse, Any]:
        if not request.user.is_authenticated:
            return JsonResponse({
                'error': 'User must be logged in to access this view'
            }, status=401)
        return view(self, request, *args, **kwargs)
    return wrapper
