# pylint:
from django.urls import path
from . import views, helper_views

app_name = 'api'

urlpatterns = [
    # Contact API
    path('contacts/', views.ContactsAPIView.as_view(), name='get_contacts'),
    path('contacts/<int:contact_id>/', views.SingleContactAPIView.as_view(), name='get_contact'),
    path('contacts/create/', views.ContactsAPIView.as_view(), name='create_contact'),
    path(
        'contacts/<int:contact_id>/update/',
        views.SingleContactAPIView.as_view(),
        name='update_contact'),
    path(
        'contacts/<int:contact_id>/delete/',
        views.SingleContactAPIView.as_view(),
        name='delete_contact'),

    # Project API
    path('projects/', views.ProjectsAPIView.as_view(), name='get_projects'),
    path('projects/create/', views.ProjectsAPIView.as_view(), name='create_project'),
    path(
        'projects/<int:project_id>/update/',
        views.SingleProjectAPIView.as_view(),
        name='update_project'),
    path(
        'projects/<int:project_id>/delete/',
        views.SingleProjectAPIView.as_view(),
        name='delete_project'),

    # Researcher API
    path('researchers/', views.ResearchersAPIView.as_view(), name='get_researchers'),
    path('researchers/create/', views.ResearchersAPIView.as_view(), name='create_researcher'),
    path(
        'researchers/<int:researcher_id>/update/',
        views.SingleResearcherAPIView.as_view(),
        name='update_researcher'),
    path(
        'researchers/<int:researcher_id>/delete/',
        views.SingleResearcherAPIView.as_view(),
        name='delete_researcher'),

    # Helper API
    path(
        'convert_markdown_to_html/',
        helper_views.convert_markdown_to_html_view,
        name='convert_markdown_to_html'),
]
