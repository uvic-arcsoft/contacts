# pylint:
from typing import Any, Dict

import json
from django.http import JsonResponse, HttpRequest
from django.views import View
from django.contrib.auth.models import User

from .decorators import logged_in_or_401
from .models import Researcher, Project, Contact, Tag

class ContactsAPIView(View):
    """
    API endpoints for contacts
    """
    @logged_in_or_401
    def get(self, request: HttpRequest) -> JsonResponse:
        """ Get all contacts that this user is a part of """
        contacts = Contact.objects.filter(rcs_members=request.user)

        # Search for contacts that include some researchers if 'researchers' query is provided
        researchers = request.GET.get('researchers', None)
        if researchers is not None:
            researchers = researchers.split(',')
            contacts = contacts.filter(researchers__id__in=researchers).order_by("-id")

        # Search for contacts that include some projects if 'projects' query is provided
        projects = request.GET.get('projects', None)
        if projects is not None:
            projects = projects.split(',')
            contacts = contacts.filter(projects__id__in=projects).order_by("-id")

        # Search for contacts by synopsis
        query = request.GET.get('query', None)
        if query:
            contacts = contacts.filter(synopsis__icontains=query).order_by('created_at')

        return JsonResponse({
            'contacts': [
                contact.to_dict(
                    include_researchers=True,
                    include_projects=True,
                    include_tags=True,
                    include_rcs_members=True
                )
                for contact in contacts
            ]
        }, status=200)

    @logged_in_or_401
    def post(self, request: HttpRequest) -> JsonResponse:
        """ Create a new contact """
        data = json.loads(request.body)
        contact = Contact.objects.create(
            synopsis = data.get('synopsis', ''),
            status = data.get('status', ''),
        )

        # Add all related projects to the contact
        projects = data.get('projects', [])
        projects = Project.objects.filter(id__in=projects)
        contact.projects.set(projects)

        # Add all related researchers to the contact
        researchers = data.get('researchers', [])
        researchers = Researcher.objects.filter(id__in=researchers)
        contact.researchers.set(researchers)

        # Create and add all related tags to the contact
        tags = data.get('tags', [])
        for tag in tags:
            Tag.objects.create(
                name = tag.get('name', ''),
                description = tag.get('description', ''),
                contact = contact
            )

        # Add all related RCS members to the contact
        rcs_members = data.get('users', [])
        rcs_members = User.objects.filter(username__in=rcs_members)
        contact.rcs_members.set(rcs_members)

        return JsonResponse({
            'message': 'Create contact successfully',
            'contact_id': contact.id
        }, status=201)

class SingleContactAPIView(View):
    """
    API endpoints for a single contact
    """
    @logged_in_or_401
    def get(self, request: HttpRequest, **kwargs: Dict[str, Any]) -> JsonResponse:
        """
        Get a specific contact
        """
        contact = None
        contact_id = kwargs.get('contact_id', None)
        try:
            contact = Contact.objects.get(id=contact_id)
        except Contact.DoesNotExist:
            return JsonResponse({
                'error': 'Contact not found.'
            }, status=404)

        # User must be a part of the contact to view it
        if request.user not in contact.rcs_members.all():
            return JsonResponse({
                'error': 'You do not have permission to view this contact.'
            }, status=403)

        return JsonResponse({
            'contact': contact.to_dict(
                include_researchers=True,
                include_projects=True,
                include_tags=True,
                include_rcs_members=True
            )
        }, status=200)

    @logged_in_or_401
    def put(self, request: HttpRequest, **kwargs: Dict[str, Any]) -> JsonResponse:
        """  Update a contact """
        contact = None
        contact_id = kwargs.get('contact_id', None)
        try:
            contact = Contact.objects.get(id=contact_id)
        except Contact.DoesNotExist:
            return JsonResponse({
                'error': 'Contact not found.'
            }, status=404)

        # User must be a part of the contact to update it
        if request.user not in contact.rcs_members.all():
            return JsonResponse({
                'error': 'You do not have permission to update this contact.'
            }, status=403)

        data = json.loads(request.body)
        contact.synopsis = data.get('synopsis', contact.synopsis)
        contact.status = data.get('status', contact.status)
        contact.save()

        # Update all related projects to the contact
        projects = data.get('projects', [])
        projects = Project.objects.filter(id__in=projects)
        contact.projects.set(projects)

        # Update all related researchers to the contact
        researchers = data.get('researchers', [])
        researchers = Researcher.objects.filter(id__in=researchers)
        contact.researchers.set(researchers)

        # Update all related tags to the contact by deleting all existing tags
        # and creating new tags
        contact.tag_set.all().delete()
        tags = data.get('tags', [])
        for tag in tags:
            Tag.objects.create(
                name = tag.get('name', ''),
                description = tag.get('description', ''),
                contact = contact
            )

        # Update all related RCS members to the contact
        rcs_members = data.get('users', [])
        rcs_members = User.objects.filter(username__in=rcs_members)
        contact.rcs_members.set(rcs_members)

        return JsonResponse({
            'message': 'Update contact successfully',
            'contact_id': contact.id
        }, status=200)

    @logged_in_or_401
    def delete(self, request: HttpRequest, **kwargs: Dict[str, Any]) -> JsonResponse:
        """ Delete a contact """
        contact = None
        contact_id = kwargs.get('contact_id', None)
        try:
            contact = Contact.objects.get(id=contact_id)
        except Contact.DoesNotExist:
            return JsonResponse({
                'error': 'Contact not found.'
            }, status=404)

        # User must be a part of the contact to delete it
        if request.user not in contact.rcs_members.all():
            return JsonResponse({
                'error': 'You do not have permission to delete this contact.'
            }, status=403)

        contact.delete()
        return JsonResponse({
            'message': 'Delete contact successfully',
            'contact_id': contact_id
        }, status=200)

class ProjectsAPIView(View):
    """
    API endpoints for projects
    """
    @logged_in_or_401
    def get(self, request: HttpRequest) -> JsonResponse:
        """ Get all projects """
        # Search for projects by name if a query is provided
        query = request.GET.get('query', None)
        if query:
            projects = Project.objects.filter(name__icontains=query).order_by('name')
        else:
            # Get projects that this user created or share a contact with
            projects = Project.objects.filter(
                contact__rcs_members=request.user
            ).order_by().union(
                Project.objects.filter(created_by=request.user).order_by()
            ).order_by("-id")

        return JsonResponse({
            'projects': [
                project.to_dict(include_researchers=True)
                for project in projects
            ]
        }, status=200)

    @logged_in_or_401
    def post(self, request:HttpRequest) -> JsonResponse:
        """
        Create a project
        """
        data = json.loads(request.body)
        project = Project.objects.create(
            name = data.get('name', ''),
            project_url = data.get('project_url', ''),
            description = data.get('description', ''),
            created_by = request.user
        )

        # Add all related researchers to the contact
        researchers = data.get('researchers', [])
        researchers = Researcher.objects.filter(id__in=researchers)
        project.researchers.set(researchers)

        return JsonResponse({
            'message': 'Create project successfully',
            'project_id': project.id
        }, status=201)

class SingleProjectAPIView(View):
    """
    API endpoints for a single project
    """
    @logged_in_or_401
    def put(self, request: HttpRequest, **kwargs: Dict[str, Any]) -> JsonResponse:
        """
        Update a project
        """
        project_id = kwargs.get('project_id', None)
        project = None

        # Check if the project exists
        try:
            project = Project.objects.get(id=project_id)
        except Project.DoesNotExist:
            return JsonResponse({
                'error': 'Project not found.'
            }, status=404)

        # Check if user has permission to update the researcher
        if not project.does_user_have_access(request.user):
            return JsonResponse({
                'error': 'You do not have permission to update this researcher.'
            }, status=403)

        data = json.loads(request.body)
        project.name = data.get('name', project.name)
        project.project_url = data.get('project_url', project.project_url)
        project.description = data.get('description', project.description)
        project.save()

        # Add all related researchers to the contact
        researchers = data.get('researchers', [])
        researchers = Researcher.objects.filter(id__in=researchers)
        project.researchers.set(researchers)

        return JsonResponse({
            'message': 'Update project successfully',
            'project_id': project.id
        }, status=200)

    @logged_in_or_401
    def delete(self, request: HttpRequest, **kwargs: Dict[str, Any]) -> JsonResponse:
        """ Delete a project """
        project_id = kwargs.get('project_id', None)
        project = None
        try:
            project = Project.objects.get(id=project_id)
        except Project.DoesNotExist:
            return JsonResponse({
                'error': 'Project not found.'
            }, status=404)

        # Check if user has permission to delete the project
        if not project.does_user_have_access(request.user):
            return JsonResponse({
                'error': 'You do not have permission to delete this project.'
            }, status=403)

        project.delete()
        return JsonResponse({
            'message': f'Delete project {project.name} successfully',
            'project_id': project_id
        }, status=200)

class ResearchersAPIView(View):
    """
    API endpoints for researchers
    """
    @logged_in_or_401
    def get(self, request: HttpRequest) -> JsonResponse:
        """ Get researchers """
        # Search for researchers by name if a query is provided
        query = request.GET.get('query', None)
        if query is not None:
            if query:
                researchers = Researcher.objects.filter(name__icontains=query).order_by('name')
            else:
                researchers = Researcher.objects.all()

            return JsonResponse({
                'researchers': [researcher.to_dict() for researcher in researchers]
            }, status=200)

        # Get researchers that this user created or share a contact with
        researchers = Researcher.objects.filter(
            contact__rcs_members=request.user
        ).order_by().union(
            Researcher.objects.filter(created_by=request.user).order_by()
        ).order_by("-id")
        return JsonResponse({
            'researchers': [
                researcher.to_dict()
                for researcher in researchers
                ]
            }, status=200)

    @logged_in_or_401
    def post(self, request: HttpRequest) -> JsonResponse:
        """ Create a new researcher """
        data = json.loads(request.body)
        researcher = Researcher.objects.create(
            name = data.get('name', ''),
            email = data.get('email', ''),
            contact_url = data.get('contact_url', ''),
            created_by = request.user
        )

        return JsonResponse({
            'message': 'Add researcher successfully',
            'researcher_id': researcher.id
        }, status=201)

class SingleResearcherAPIView(View):
    """
    API endpoints for a single researcher
    """
    @logged_in_or_401
    def put(self, request: HttpRequest, **kwargs: Dict[str, Any]) -> JsonResponse:
        """
        Update a researcher
        """
        researcher_id = kwargs.get('researcher_id', None)
        researcher = None
        try:
            researcher = Researcher.objects.get(id=researcher_id)
        except Researcher.DoesNotExist:
            return JsonResponse({
                'error': 'Researcher not found.'
            }, status=404)

        # Check if user has permission to update the researcher
        if not researcher.does_user_have_access(request.user):
            return JsonResponse({
                'error': 'You do not have permission to update this researcher.'
            }, status=403)

        data = json.loads(request.body)
        researcher.name = data.get('name', researcher.name)
        researcher.email = data.get('email', researcher.email)
        researcher.contact_url = data.get('contact_url', researcher.contact_url)
        researcher.save()

        return JsonResponse({
            'message': 'Update researcher successfully',
            'researcher_id': researcher.id
        }, status=200)

    @logged_in_or_401
    def delete(self, request: HttpRequest, **kwargs: Dict[str, Any]) -> JsonResponse:
        """ Delete a researcher """
        researcher_id = kwargs.get('researcher_id', None)
        researcher = None
        try:
            researcher = Researcher.objects.get(id=researcher_id)
        except Researcher.DoesNotExist:
            return JsonResponse({
                'error': 'Researcher not found.'
            }, status=404)

        # Check if user has permission to delete the researcher
        if not researcher.does_user_have_access(request.user):
            return JsonResponse({
                'error': 'You do not have permission to delete this researcher.'
            }, status=403)

        researcher.delete()
        return JsonResponse({
            'message': f'Delete researcher {researcher.name} successfully',
            'researcher_id': researcher_id
        }, status=200)
