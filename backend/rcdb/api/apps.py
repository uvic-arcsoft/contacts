# pylint:
from django.apps import AppConfig


class NormalUsersConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'api'
