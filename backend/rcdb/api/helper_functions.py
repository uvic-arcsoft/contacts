# pylint:
import os
import requests
import environ

# Initialize environment variables
environ.Env.read_env()

def convert_markdown_to_html(markdown: str) -> str:
    """
    Convert markdown to HTML
    """
    private_token = os.environ.get('GITLAB_PRIVATE_TOKEN')
    response = requests.post(
        "https://gitlab.com/api/v4/markdown",
        json={
            'text': markdown,
            "gfm": True
        },
        headers={
            'Private-Token': private_token,
            'Content-Type': 'application/json'
        },
        timeout=10
    )
    html = response.json().get('html', '')
    return html
