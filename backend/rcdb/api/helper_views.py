# pylint:
import json
from django.http import JsonResponse, HttpRequest
from .helper_functions import convert_markdown_to_html

def convert_markdown_to_html_view(request: HttpRequest) -> JsonResponse:
    """
    Convert markdown to HTML
    """
    if request.method == 'POST':
        # Get the markdown from the request
        markdown = json.loads(request.body).get('markdown', '')
        html = convert_markdown_to_html(markdown)

        # Return the HTML text
        return JsonResponse({'html': html}, status=200)

    return JsonResponse({'error': 'Invalid request method'}, status=405)
