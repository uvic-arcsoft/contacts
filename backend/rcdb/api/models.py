# pylint: disable=line-too-long, too-many-ancestors
#   line-too-long: It's not convenient to split the long lines
#   too-many-ancestors: The classes are inheriting from Django's models, which we have no control over
from typing import Dict, Any, Union
import json

from django.db import models

from .helper_functions import convert_markdown_to_html

class Researcher(models.Model):
    """
    Researchers that RCS Team members work with
    """
    name = models.CharField(max_length=200, null=False, blank=False)
    email = models.EmailField(max_length=200, null=True, blank=True)
    contact_url = models.URLField(max_length=400, null=True, blank=True)
    created_by = models.ForeignKey('auth.User', null=True, on_delete=models.SET_NULL)
    last_updated = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['-id']
        db_table = 'researcher'

    def __str__(self) -> str:
        return str(self.name)

    def to_dict(self) -> Dict[str, Union[int, str]]:
        """
        Convert Researcher object into a dictionary
        """
        researcher_dict = {
            "id": self.id,
            "name": self.name,
            "email": self.email,
            "contact_url": self.contact_url
        }
        return researcher_dict

    def delete(self, *args, **kwargs):
        """
        Delete all contacts that have this researcher as the only researcher
        """
        for contact in self.contact_set.all():
            if contact.researchers.count() == 1 and contact.researchers.first() == self:
                contact.delete()
        super().delete(*args, **kwargs)

    def does_user_have_access(self, user) -> bool:
        """
        Check if the user has access to this Researcher object
        Having access means the user can GET, PUT, DELETE this Researcher object
        """
        return self.created_by == user or self.contact_set.filter(rcs_members=user).exists()

class Project(models.Model):
    """
    Projects that RCS Team members work on for researchers
    """
    name = models.CharField(max_length=200, null=False, blank=False)
    project_url = models.URLField(max_length=400, null=True, blank=True)
    researchers = models.ManyToManyField(Researcher, blank=True)
    description = models.TextField(
        max_length=1000,
        null=False,
        blank=False,
        default="This is a default description. Every project that was created before the description field was declared will have this description.")
    created_by = models.ForeignKey('auth.User', null=True, on_delete=models.SET_NULL)
    last_updated = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['-id']
        db_table = 'project'

    def __str__(self) -> str:
        return str(self.name)

    def to_dict(self, include_researchers:bool=False) -> Dict[str, Any]:
        """
        Convert Project object into a dictionary
        """
        project_dict = {
            "id": self.id,
            "name": self.name,
            "project_url": self.project_url,
            "description": self.description,
        }

        if include_researchers:
            project_dict["researchers"] = [researcher.to_dict() for researcher in self.researchers.all()]

        return project_dict

    def does_user_have_access(self, user) -> bool:
        """
        Check if the user has access to this Researcher object
        Having access means the user can GET, PUT, DELETE this Researcher object
        """
        return self.created_by == user or self.contact_set.filter(rcs_members=user).exists()

class StatusChoices(models.TextChoices):
    """
    Status choices for Contact
    """
    DORMANT = "dormant", "dormant"
    STALLED = "stalled", "stalled"
    ACTIVE = "active", "active"
    INITIAL_BUILDING = "initial/building", "initial/building"

class Contact(models.Model):
    """
    Contact between RCS team member(s) and researcher(s) regarding a project
    """
    synopsis = models.TextField(max_length=1000, null=False, blank=False)
    status = models.CharField(
        max_length=100, choices=StatusChoices.choices, default="initial/building", null=False, blank=False)
    projects = models.ManyToManyField(Project, blank=True)
    rcs_members = models.ManyToManyField('auth.User')
    researchers = models.ManyToManyField(Researcher)
    created_at = models.DateTimeField(auto_now_add=True)
    last_updated = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['-created_at']
        db_table = 'contact'

    def __str__(self) -> str:
        """
        # String representation of a Contact object
        """
        return str(self.researchers.first()) + " on " + self.created_at.strftime("%b %d, %Y")

    def to_dict(
        self,
        include_researchers:bool=False,
        include_projects:bool=False,
        include_tags:bool=False,
        include_rcs_members:bool=False) -> Dict[str, Any]:
        """
        Convert Contact object into a dictionary
        """
        contact_dict = {
            "id": self.id,
            "name": str(self),
            "synopsis": self.synopsis,
            "synopsis_html": convert_markdown_to_html(self.synopsis),
            "status": self.status,
            "created_at": self.created_at.strftime("%b %d, %Y"),
            "last_updated": self.last_updated.strftime("%b %d, %Y"),
        }

        if include_researchers:
            contact_dict["researchers"] = [researcher.to_dict() for researcher in self.researchers.all()]

        if include_projects:
            contact_dict["projects"] = [project.to_dict() for project in self.projects.all()]

        if include_tags:
            contact_dict["tags"] = [tag.to_dict() for tag in self.tag_set.all()]

        if include_rcs_members:
            contact_dict["rcs_members"] = [
                {
                    "id": rcs_member.id,
                    "name": rcs_member.first_name + " " + rcs_member.last_name if rcs_member.first_name else rcs_member.username,
                    "username": rcs_member.username,
                }
                for rcs_member in self.rcs_members.all()
            ]

        return contact_dict

    def save(self, *args, **kwargs):
        """
        Override the save method of the Contact model
        """
        super().save(*args, **kwargs)

        # Update time interacted with of the related researchers and projects
        for project in self.projects.all():
            project.save()
        for researcher in self.researchers.all():
            researcher.save()

class Tag(models.Model):
    """
    Tags that are associated with a Contact. For example:
    TODO: follow up with their grad student marie@example.org to discuss data structure
    """
    name = models.CharField(max_length=200, null=False, blank=False)
    description = models.TextField(max_length=500, null=True, blank=True)
    contact = models.ForeignKey(Contact, on_delete=models.CASCADE)
    last_updated = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['-last_updated']
        db_table = 'tag'

    def __str__(self) -> str:
        """
        String representation of a Tag object
        """
        return str(self.contact) + ": " + str(self.name)

    def to_dict(self) -> Dict[str, Union[int, str]]:
        """
        Convert Tag object into a dictionary
        """
        tag_dict = {
            "id": self.id,
            "name": self.name,
            "description": self.description,
            "last_updated": self.last_updated.strftime("%b %d, %Y"),
        }
        return tag_dict

class Artifact(models.Model):
    """
    Artifact that is created by the user for a project
    """
    name = models.CharField(max_length=200, null=False, blank=False)
    description = models.TextField(max_length=1000, null=False, blank=False)
    artifact_url = models.URLField(max_length=400, null=True, blank=True)
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    last_updated = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['-id']
        db_table = 'artifact'

    def __str__(self):
        """
        String representation of an Artifact object
        """
        return str(self.project) + ": " + self.name

    def save(self, *args, **kwargs):
        """
        Override the save method of the Artifact model
        """
        super().save(*args, **kwargs)
        # Update time interacted with of the related project
        self.project.save()

class Software(models.Model):
    """
    This model keeps track of softwares being used or needed
    by each researcher
    """
    researchers = models.ManyToManyField(Researcher, blank=True)
    name = models.CharField(max_length=300, null=False, blank=False)
    description = models.TextField(max_length=2000, null=False, blank=False)
    field = models.CharField(max_length=200, null=True, blank=True)
    type = models.CharField(max_length=200, null=True, blank=True)
    main_url = models.CharField(max_length=300, null=False, blank=False)
    # This field is a string representing a list of dictionary. For each dictionary,
    # key is an URL description and value is an URL link
    # For example: '[{"Software documentaion": "https://software.doc.org"}, {"Software link": "https://software.example.org"}]'
    urls = models.CharField(max_length=1000, blank=True, default="")
    created_by = models.ForeignKey('auth.User', null=True, on_delete=models.SET_NULL)
    last_updated = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['-id']
        db_table = 'software'

    # pylint: disable=invalid-str-returned
    #   invalid-str-returned: the function returns a models.CharField
    #   which is a string
    def __str__(self) -> str:
        """
        String representation of a Software object
        """
        return self.name
    # pylint: enable=invalid-str-returned

    def to_dict(self, include_researchers:bool=False) -> Dict[str, Any]:
        """
        Convert Software object into a dictionary
        """
        software_dict = {
            "id": self.id,
            "name": self.name,
            "description": self.description,
            "field": self.field,
            "type": self.type,
            "main_url": self.main_url,
            "urls": json.loads(self.urls) if self.urls else [],
        }

        if include_researchers:
            software_dict["researchers"] = [researcher.to_dict() for researcher in self.researchers.all()]

        return software_dict

class LicenseChoices(models.TextChoices):
    """
    License choices for Usage
    """
    FREE = "Free", "Free"
    OPEN_SOURCE = "Open source", "Open source"
    COMMERCIAL = "Commercial", "Commercial"

class Usage(models.Model):
    """
    This model describes the usage for a specific software version
    """
    software = models.ForeignKey(Software, on_delete=models.CASCADE, null=False, blank=False)
    version = models.CharField(max_length=50, blank=True, default='Any')
    licensed_entities = models.CharField(max_length=500, blank=True, default='Everyone')
    terms = models.TextField(max_length=2000, null=True, blank=True)
    expiry_date = models.DateField(null=True, blank=True)
    license_type = models.CharField(max_length=15, null=False, blank=False,
                choices=LicenseChoices.choices, default=LicenseChoices.FREE)
    license_name = models.CharField(max_length=300, null=True, blank=True)
    license_url = models.URLField(max_length=500, null=True, blank=True)
    last_updated = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['-id']
        db_table = 'usage'

    def __str__(self) -> str:
        """
        String representation of a Usage object
        """
        return f'{self.license_type} {"-" if self.version else ""} {self.version}'

    def to_dict(self) -> Dict[str, Any]:
        """
        Convert Usage object into a dictionary
        """
        usage_dict = {
            "id": self.id,
            "software": self.software.to_dict(),
            "version": self.version,
            "licensed_entities": self.licensed_entities,
            "terms": self.terms,
            "expiry_date": self.expiry_date.strftime("%b %d, %Y") if self.expiry_date else None,
            "license_type": self.license_type,
            "license_name": self.license_name,
            "license_url": self.license_url,
            "last_updated": self.last_updated.strftime("%b %d, %Y"),
        }
        return usage_dict
