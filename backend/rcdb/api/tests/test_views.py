# pylint:
import json

from django.test import TestCase, Client
from django.contrib.auth.models import User
from django.urls import reverse
from api.models import (Researcher, Project, Contact,
            Tag, StatusChoices)

class ViewTestCase(TestCase):
    """
    General set up for view unit tests
    """
    def setUp(self):
        # Create an authorized client that has access to all objects created
        self.user = User.objects.create_user(
            first_name='Test',
            last_name='User',
            username='user@example.org',
            password='testpassword'
        )
        self.client = Client(HTTP_X_FORWARDED_USER="user@example.org")
        self.client.get(reverse('users:login'))

        # Create an unauthorized client
        self.unauthz_user = User.objects.create_user(
            first_name='Unauthorized',
            last_name='User',
            username='unauthz_user@example.org',
            password='testpassword'
        )
        self.unauthorized_client = Client(HTTP_X_FORWARDED_USER="unauthz_user@example.org")
        self.unauthorized_client.get(reverse('users:login'))

        # Create a test researcher
        self.researcher = Researcher.objects.create(
            name='John Doe',
            email='jdoe@example.org',
            contact_url='https://johndoe.example.org',
            created_by=self.user
        )

        # Create a project for the test researcher
        self.project = Project.objects.create(
            name='RCDB',
            project_url='https://rcdb.example.org',
            description="This is Researcher Contacts Database.",
            created_by=self.user
        )

        # Create a contact for the test researcher that regards the project
        self.contact = Contact.objects.create(
            synopsis="## Meeting on RCDB",
            status=StatusChoices.INITIAL_BUILDING
        )
        self.contact.researchers.add(self.researcher)
        self.contact.projects.add(self.project)
        self.contact.rcs_members.add(self.user)
        self.contact.save()

class ContactsAPIViewTestCase(ViewTestCase):
    """
    Test API endpoints for contacts
    """
    def test_get(self):
        """
        Get all contacts that this user is a part of
        """
        # Get all contacts
        response = self.client.get(reverse('api:get_contacts'))
        self.assertEqual(response.status_code, 200)
        contacts = response.json()['contacts']
        self.assertEqual(len(contacts), 1)
        self.assertEqual(
            contacts[0],
            self.contact.to_dict(
                include_researchers=True,
                include_projects=True,
                include_tags=True,
                include_rcs_members=True
            )
        )

        # Filter by researchers
        response = self.client.get(reverse('api:get_contacts'), {'researchers': self.researcher.id})
        contacts = response.json()['contacts']
        self.assertEqual(len(contacts), 1)

        response = self.client.get(reverse('api:get_contacts'), {'researchers': self.researcher.id + 1})
        contacts = response.json()['contacts']
        self.assertEqual(len(contacts), 0)

        # Filter by projects
        response = self.client.get(reverse('api:get_contacts'), {'projects': self.project.id})
        contacts = response.json()['contacts']
        self.assertEqual(len(contacts), 1)

        response = self.client.get(reverse('api:get_contacts'), {'projects': self.project.id + 1})
        contacts = response.json()['contacts']
        self.assertEqual(len(contacts), 0)

        # Filter by synopsis
        response = self.client.get(reverse('api:get_contacts'), {'query': 'RCDB'})
        contacts = response.json()['contacts']
        self.assertEqual(len(contacts), 1)

        response = self.client.get(reverse('api:get_contacts'), {'query': 'Something that does not exist'})
        contacts = response.json()['contacts']
        self.assertEqual(len(contacts), 0)

        # Unauthorized user should not have access to any contact
        response = self.unauthorized_client.get(reverse('api:get_contacts'))
        contacts = response.json()['contacts']
        self.assertEqual(len(contacts), 0)

    def test_post(self):
        """
        Create a new contact
        """
        # Create a new contact
        response = self.client.post(
            reverse('api:create_contact'),
            data=json.dumps({
                'synopsis': "## Update on the lastest RCDB's implementation",
                'status': StatusChoices.ACTIVE,
                'researchers': [self.researcher.id],
                'projects': [self.project.id],
                'users': [self.user.username],
                'tags': [
                    {
                        'name': 'TODO',
                        'description': 'This is a task that needs to be done.'
                    }
                ]
            }),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, 201)

        # Check if the contact was created
        self.assertEqual(Contact.objects.count(), 2)
        # NOTE: Since contacts are sorted by creation da†e in descending order,
        # this first contact is the one that we just created
        new_contact = Contact.objects.first()
        self.assertEqual(new_contact.synopsis, "## Update on the lastest RCDB's implementation")
        self.assertEqual(new_contact.status, StatusChoices.ACTIVE)
        self.assertEqual(new_contact.researchers.count(), 1)
        self.assertEqual(new_contact.projects.count(), 1)
        self.assertEqual(new_contact.rcs_members.count(), 1)

        # Check if the tags were created
        self.assertEqual(Tag.objects.count(), 1)
        # NOTE: Since tags are sorted by the last updated date in descending order,
        # this first tag is the one that we just created
        new_tag = Tag.objects.first()
        self.assertEqual(new_tag.name, 'TODO')
        self.assertEqual(new_tag.description, 'This is a task that needs to be done.')

class SingleContactAPIViewTestCase(ViewTestCase):
    """
    Test API endpoints for a single contact
    """
    def test_get(self):
        """
        Get a single contact
        """
        # Contact exists
        response = self.client.get(reverse('api:get_contact', args=[self.contact.id]))
        self.assertEqual(response.status_code, 200)
        contact = response.json()['contact']
        self.assertEqual(
            contact,
            self.contact.to_dict(
                include_researchers=True,
                include_projects=True,
                include_tags=True,
                include_rcs_members=True
            )
        )

        # Contact does not exist
        response = self.client.get(reverse('api:get_contact', args=[self.contact.id + 1]))
        self.assertEqual(response.status_code, 404)

        # Unauthorized user tries to access the contact
        response = self.unauthorized_client.get(reverse('api:get_contact', args=[self.contact.id]))
        self.assertEqual(response.status_code, 403)

    def test_put(self):
        """
        Update a single contact
        """
        # Update the contact
        response = self.client.put(
            reverse('api:update_contact', kwargs={'contact_id': self.contact.id}),
            data=json.dumps({
                # NOTE: We are only updating the synopsis and status
                'synopsis': "## Update on the lastest RCDB's implementation",
                'status': StatusChoices.ACTIVE,
                'researchers': [self.researcher.id],
                'projects': [self.project.id],
                'users': [self.user.username],
                'tags': [
                    {
                        'name': 'TODO',
                        'description': 'This is a task that needs to be done.'
                    }
                ]
            }),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, 200)

        # Check if the contact was updated
        self.contact.refresh_from_db()
        self.assertEqual(self.contact.synopsis, "## Update on the lastest RCDB's implementation")
        self.assertEqual(self.contact.status, StatusChoices.ACTIVE)
        self.assertEqual(self.contact.researchers.count(), 1)
        self.assertEqual(self.contact.projects.count(), 1)
        self.assertEqual(self.contact.rcs_members.count(), 1)

        # Check if the tags were created
        self.assertEqual(Tag.objects.count(), 1)
        # NOTE: Since tags are sorted by the last updated date in descending order,
        # this first tag is the one that we just created
        new_tag = Tag.objects.first()
        self.assertEqual(new_tag.name, 'TODO')
        self.assertEqual(new_tag.description, 'This is a task that needs to be done.')

        # Update a non-existing contact
        response = self.client.put(
            reverse('api:update_contact', kwargs={'contact_id': self.contact.id + 1}),
            data=json.dumps({
                'synopsis': "## Update on the lastest RCDB's implementation",
                'status': StatusChoices.ACTIVE,
                'researchers': [self.researcher.id],
                'projects': [self.project.id],
                'users': [self.user.username],
                'tags': [
                    {
                        'name': 'TODO',
                        'description': 'This is a task that needs to be done.'
                    }
                ]
            }),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, 404)

        # Unauthorized user tries to update the contact
        response = self.unauthorized_client.put(
            reverse('api:update_contact', kwargs={'contact_id': self.contact.id}),
            data=json.dumps({
                'synopsis': "## Update on the lastest RCDB's implementation",
                'status': StatusChoices.ACTIVE,
                'researchers': [self.researcher.id],
                'projects': [self.project.id],
                'users': [self.user.username],
                'tags': [
                    {
                        'name': 'TODO',
                        'description': 'This is a task that needs to be done.'
                    }
                ]
            }),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, 403)

    def test_delete(self):
        """
        Delete a single contact
        """
        # Unauthorized user tries to delete the contact
        response = self.unauthorized_client.delete(reverse('api:delete_contact', args=[self.contact.id]))
        self.assertEqual(response.status_code, 403)

        # Delete the contact
        response = self.client.delete(reverse('api:delete_contact', args=[self.contact.id]))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(Contact.objects.count(), 0)

        # Delete a non-existing contact
        response = self.client.delete(reverse('api:delete_contact', args=[self.contact.id]))
        self.assertEqual(response.status_code, 404)

class ProjectsAPIViewTestCase(ViewTestCase):
    """
    Test API endpoints for projects
    """
    def test_get(self):
        """
        Get all projects that this user is a part of
        """
        # Get all projects
        response = self.client.get(reverse('api:get_projects'))
        self.assertEqual(response.status_code, 200)
        projects = response.json()['projects']
        self.assertEqual(len(projects), 1)
        self.assertEqual(
            projects[0],
            self.project.to_dict(
                include_researchers=True
            )
        )

        # Filter by name
        response = self.client.get(reverse('api:get_projects'), {'query': 'RCDB'})
        projects = response.json()['projects']
        self.assertEqual(len(projects), 1)

        response = self.client.get(reverse('api:get_projects'), {'query': 'Something that does not exist'})
        projects = response.json()['projects']
        self.assertEqual(len(projects), 0)

        # Unauthorized user should not have access to any project
        response = self.unauthorized_client.get(reverse('api:get_projects'))
        projects = response.json()['projects']
        self.assertEqual(len(projects), 0)

    def test_post(self):
        """
        Create a new project
        """
        # Create a new project
        response = self.client.post(
            reverse('api:create_project'),
            data=json.dumps({
                'name': 'RCDB 2.0',
                'project_url': 'https://rcdb2.example.org',
                'description': 'This is the next version of Researcher Contacts Database.',
                'researchers': [self.researcher.id]
            }),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, 201)

        # Check if the project was created
        self.assertEqual(Project.objects.count(), 2)
        # NOTE: Since projects are sorted by id in descending order,
        # this first project is the one that we just created
        new_project = Project.objects.first()
        self.assertEqual(new_project.name, 'RCDB 2.0')
        self.assertEqual(new_project.project_url, 'https://rcdb2.example.org')
        self.assertEqual(new_project.description, 'This is the next version of Researcher Contacts Database.')
        self.assertEqual(new_project.researchers.count(), 1)

class SingleProjectAPIViewTestCase(ViewTestCase):
    """
    Test API endpoints for a single project
    """
    def test_put(self):
        """
        Update a single project
        """
        # Update the project
        response = self.client.put(
            reverse('api:update_project', kwargs={'project_id': self.project.id}),
            data=json.dumps({
                'name': 'RCDB 2.0',
                'project_url': 'https://rcdb2.example.org',
                'description': 'This is the next version of Researcher Contacts Database.',
                'researchers': [self.researcher.id]
            }),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, 200)

        # Check if the project was updated
        self.project.refresh_from_db()
        self.assertEqual(self.project.name, 'RCDB 2.0')
        self.assertEqual(self.project.project_url, 'https://rcdb2.example.org')
        self.assertEqual(self.project.description, 'This is the next version of Researcher Contacts Database.')
        self.assertEqual(self.project.researchers.count(), 1)

        # Update a non-existing project
        response = self.client.put(
            reverse('api:update_project', kwargs={'project_id': self.project.id + 1}),
            data=json.dumps({
                'name': 'RCDB 2.0',
                'project_url': 'https://rcdb2.example.org',
                'description': 'This is the next version of Researcher Contacts Database.',
                'researchers': [self.researcher.id]
            }),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, 404)

        # Unauthorized user tries to update the project
        response = self.unauthorized_client.put(
            reverse('api:update_project', kwargs={'project_id': self.project.id}),
            data=json.dumps({
                'name': 'RCDB 2.0',
                'project_url': 'https://rcdb2.example.org',
                'description': 'This is the next version of Researcher Contacts Database.',
                'researchers': [self.researcher.id]
            }),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, 403)

    def test_delete(self):
        """
        Delete a single project
        """
        # Unauthorized user tries to delete the project
        response = self.unauthorized_client.delete(reverse('api:delete_project', args=[self.project.id]))
        self.assertEqual(response.status_code, 403)

        # Delete the project
        response = self.client.delete(reverse('api:delete_project', args=[self.project.id]))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(Project.objects.count(), 0)

        # Delete a non-existing project
        response = self.client.delete(reverse('api:delete_project', args=[self.project.id]))
        self.assertEqual(response.status_code, 404)

class ResearchersAPIViewTestCase(ViewTestCase):
    """
    Test API endpoints for researchers
    """
    def test_get(self):
        """
        Get all researchers
        """
        # Get all researchers
        response = self.client.get(reverse('api:get_researchers'))
        self.assertEqual(response.status_code, 200)
        researchers = response.json()['researchers']
        self.assertEqual(len(researchers), 1)
        self.assertEqual(
            researchers[0],
            self.researcher.to_dict()
        )

        # Filter by name
        response = self.client.get(reverse('api:get_researchers'), {'query': 'John Doe'})
        researchers = response.json()['researchers']
        self.assertEqual(len(researchers), 1)

        response = self.client.get(reverse('api:get_researchers'), {'query': 'Something that does not exist'})
        researchers = response.json()['researchers']
        self.assertEqual(len(researchers), 0)

        # Unauthorized user should not have access to any researcher
        response = self.unauthorized_client.get(reverse('api:get_researchers'))
        researchers = response.json()['researchers']
        self.assertEqual(len(researchers), 0)

    def test_post(self):
        """
        Create a new researcher
        """
        # Create a new researcher
        response = self.client.post(
            reverse('api:create_researcher'),
            data=json.dumps({
                'name': 'Jane Doe',
                'email': 'janedoe@example.org',
                'contact_url': 'https://janedoe.example.org'
            }),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, 201)

        # Check if the researcher was created
        self.assertEqual(Researcher.objects.count(), 2)
        # NOTE: Since researchers are sorted by id in descending order,
        # this first researcher is the one that we just created
        new_researcher = Researcher.objects.first()
        self.assertEqual(new_researcher.name, 'Jane Doe')
        self.assertEqual(new_researcher.email, 'janedoe@example.org')
        self.assertEqual(new_researcher.contact_url, 'https://janedoe.example.org')

class SingleResearcherAPIViewTestCase(ViewTestCase):
    """
    Test API endpoints for a single researcher
    """
    def test_put(self):
        """
        Update a single researcher
        """
        # Update the researcher
        response = self.client.put(
            reverse('api:update_researcher', kwargs={'researcher_id': self.researcher.id}),
            data=json.dumps({
                'name': 'Jane Doe',
                'email': 'janedoe@example.org',
                'contact_url': 'https://janedoe.example.org'
            }),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, 200)

        # Check if the researcher was updated
        self.researcher.refresh_from_db()
        self.assertEqual(self.researcher.name, 'Jane Doe')
        self.assertEqual(self.researcher.email, 'janedoe@example.org')
        self.assertEqual(self.researcher.contact_url, 'https://janedoe.example.org')

        # Update a non-existing researcher
        response = self.client.put(
            reverse('api:update_researcher', kwargs={'researcher_id': self.researcher.id + 1}),
            data=json.dumps({
                'name': 'Jane Doe',
                'email': 'janedoe@example.org',
                'contact_url': 'https://janedoe.example.org'
            }),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, 404)

        # Unauthorized user tries to update the researcher
        response = self.unauthorized_client.put(
            reverse('api:update_researcher', kwargs={'researcher_id': self.researcher.id}),
            data=json.dumps({
                'name': 'Jane Doe',
                'email': 'janedoe@example.org',
                'contact_url': 'https://janedoe.example.org'
            }),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, 403)

    def test_delete(self):
        """
        Delete a single researcher
        """
        # Unauthorized user tries to delete the researcher
        response = self.unauthorized_client.delete(reverse('api:delete_researcher', args=[self.researcher.id]))
        self.assertEqual(response.status_code, 403)

        # Delete the researcher
        response = self.client.delete(reverse('api:delete_researcher', args=[self.researcher.id]))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(Researcher.objects.count(), 0)

        # Delete a non-existing researcher
        response = self.client.delete(reverse('api:delete_researcher', args=[self.researcher.id]))
        self.assertEqual(response.status_code, 404)
