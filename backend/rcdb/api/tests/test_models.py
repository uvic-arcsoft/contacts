# pylint:
from django.test import TestCase
from django.contrib.auth.models import User
from api.models import (Researcher, Project, Contact,
            Tag, Artifact, Software, Usage, StatusChoices, LicenseChoices)

class ModelTestCase(TestCase):
    """
    General set up for model unit tests
    """
    def setUp(self):
        # Create an initial user
        self.user = User.objects.create_user(
            first_name='Test',
            last_name='User',
            username='testuser',
            password='testpassword',
            email='testuser@example.org'
        )

        # Create researchers
        self.john_researcher = Researcher.objects.create(
            name='John Doe',
            email='jdoe@example.org',
            contact_url='https://johndoe.example.org',
            created_by=self.user
        )
        self.oliver_researcher = Researcher.objects.create(
            name='Oliver Kane',
            email='okane@example.org',
            contact_url='https://oliverkane.example.org',
            created_by=self.user
        )

        # Create a project
        self.rcdb_project = Project.objects.create(
            name='RCDB',
            project_url='https://rcdb.example.org',
            description="This is Researcher Contacts Database.",
            created_by=self.user
        )
        self.rcdb_project.researchers.add(self.john_researcher)

        self.random_project = Project.objects.create(
            name='Random Number Project',
            project_url='https://random.example.org',
            description="This is a random number generator.",
            created_by=self.user
        )
        self.random_project.researchers.set([self.john_researcher, self.oliver_researcher])

        # Create a contact
        self.rcdb_contact = Contact.objects.create(synopsis="## Meeting on RCDB", status=StatusChoices.ACTIVE)
        self.rcdb_contact.projects.add(self.rcdb_project)
        self.rcdb_contact.researchers.set([self.john_researcher, self.oliver_researcher])
        self.rcdb_contact.rcs_members.add(self.user)
        self.rcdb_contact.save()

        # Create a tag
        self.todo_tag = Tag.objects.create(
            name='TODO',
            description='This is a testing TODO tag',
            contact=self.rcdb_contact)

        # Create an artifact
        self.doc_artifact = Artifact.objects.create(
            name='Documentation',
            description='Documentation for RCDB',
            artifact_url='https://artifact.org',
            project=self.rcdb_project
        )

        # Create a software
        self.notepad_software = Software.objects.create(
            name = 'Notepad',
            description = 'Used for taking notes with ease',
            main_url = "https://notepad.example.org",
            created_by = self.user
        )
        self.notepad_software.researchers.set([self.john_researcher, self.oliver_researcher])

        # Create an usage for the software
        self.notepad_usage = Usage.objects.create(
            software=self.notepad_software,
            version='1.0',
            licensed_entities='Everyone',
            terms='This license is for everyone',
            license_type=LicenseChoices.OPEN_SOURCE,
            license_name='MIT',
            license_url='https://opensource.org/licenses/MIT'
        )

class ResearcherModelTest(ModelTestCase):
    """
    Test the Researcher model
    """
    def test_create(self):
        """
        Test researcher creation
        """
        self.assertEqual(self.john_researcher.name, 'John Doe')
        self.assertEqual(self.john_researcher.email, 'jdoe@example.org')
        self.assertEqual(self.john_researcher.contact_url, 'https://johndoe.example.org')
        self.assertEqual(self.john_researcher.created_by, self.user)

        self.assertEqual(self.oliver_researcher.name, 'Oliver Kane')
        self.assertEqual(self.oliver_researcher.email, 'okane@example.org')
        self.assertEqual(self.oliver_researcher.contact_url, 'https://oliverkane.example.org')
        self.assertEqual(self.oliver_researcher.created_by, self.user)

    def test_delete(self):
        """
        Test researcher deletion
        """
        self.john_researcher.delete()
        self.oliver_researcher.delete()
        self.assertEqual(Researcher.objects.count(), 0)

        # A contact with all researchers deleted should also be deleted
        self.assertEqual(Contact.objects.count(), 0)

    def test_str(self):
        """
        Test the string representation of a researcher
        (__str__ method)
        """
        self.assertEqual(str(self.john_researcher), 'John Doe')
        self.assertEqual(str(self.oliver_researcher), 'Oliver Kane')

    def test_to_dict(self):
        """
        Test the to_dict method of the researcher model
        """
        john_dict = self.john_researcher.to_dict()
        self.assertEqual(john_dict['name'], 'John Doe')
        self.assertEqual(john_dict['email'], 'jdoe@example.org')
        self.assertEqual(john_dict['contact_url'], 'https://johndoe.example.org')

        oliver_dict = self.oliver_researcher.to_dict()
        self.assertEqual(oliver_dict['name'], 'Oliver Kane')
        self.assertEqual(oliver_dict['email'], 'okane@example.org')
        self.assertEqual(oliver_dict['contact_url'], 'https://oliverkane.example.org')

    def test_does_user_have_access(self):
        """
        Test the does_user_have_access method of the researcher model
        """
        self.assertTrue(self.john_researcher.does_user_have_access(self.user))
        self.assertTrue(self.oliver_researcher.does_user_have_access(self.user))

class ProjectModelTest(ModelTestCase):
    """
    Test the Project model
    """
    def test_create(self):
        """
        Test project creation
        """
        self.assertEqual(self.rcdb_project.name, 'RCDB')
        self.assertEqual(self.rcdb_project.project_url, 'https://rcdb.example.org')
        self.assertEqual(self.rcdb_project.description, 'This is Researcher Contacts Database.')
        self.assertEqual(self.rcdb_project.created_by, self.user)

        self.assertEqual(self.random_project.name, 'Random Number Project')
        self.assertEqual(self.random_project.project_url, 'https://random.example.org')
        self.assertEqual(self.random_project.description, 'This is a random number generator.')
        self.assertEqual(self.random_project.created_by, self.user)

    def test_delete(self):
        """
        Test project deletion
        """
        self.rcdb_project.delete()
        self.random_project.delete()
        self.assertEqual(Project.objects.count(), 0)

    def test_str(self):
        """
        Test the string representation of a project
        (__str__ method)
        """
        self.assertEqual(str(self.rcdb_project), 'RCDB')
        self.assertEqual(str(self.random_project), 'Random Number Project')

    def test_to_dict(self):
        """
        Test the to_dict method of the project model
        """
        rcdb_dict = self.rcdb_project.to_dict()
        self.assertEqual(rcdb_dict['name'], 'RCDB')
        self.assertEqual(rcdb_dict['project_url'], 'https://rcdb.example.org')
        self.assertEqual(rcdb_dict['description'], 'This is Researcher Contacts Database.')

        random_dict = self.random_project.to_dict()
        self.assertEqual(random_dict['name'], 'Random Number Project')
        self.assertEqual(random_dict['project_url'], 'https://random.example.org')
        self.assertEqual(random_dict['description'], 'This is a random number generator.')

    def test_does_user_have_access(self):
        """
        Test the does_user_have_access method of the project model
        """
        self.assertTrue(self.rcdb_project.does_user_have_access(self.user))
        self.assertTrue(self.random_project.does_user_have_access(self.user))

class ContactModelTest(ModelTestCase):
    """
    Test the Contact model
    """
    def test_create(self):
        """
        Test contact creation
        """
        self.assertEqual(self.rcdb_contact.synopsis, '## Meeting on RCDB')
        self.assertEqual(self.rcdb_contact.status, StatusChoices.ACTIVE)
        self.assertEqual(self.rcdb_contact.projects.count(), 1)
        self.assertEqual(self.rcdb_contact.researchers.count(), 2)
        self.assertEqual(self.rcdb_contact.rcs_members.count(), 1)

    def test_delete(self):
        """
        Test contact deletion
        """
        self.rcdb_contact.delete()
        self.assertEqual(Contact.objects.count(), 0)

    def test_str(self):
        """
        Test the string representation of a contact
        (__str__ method)
        """
        self.assertEqual(
            str(self.rcdb_contact),
            str(self.rcdb_contact.researchers.first()) + " on " + self.rcdb_contact.created_at.strftime("%b %d, %Y")
        )

    def test_to_dict(self):
        """
        Test the to_dict method of the contact model
        """
        contact_dict = self.rcdb_contact.to_dict()
        self.assertEqual(contact_dict['name'], str(self.rcdb_contact))
        self.assertEqual(contact_dict['synopsis'], '## Meeting on RCDB')
        self.assertIn('</h2>', contact_dict['synopsis_html'])
        self.assertEqual(contact_dict['status'], StatusChoices.ACTIVE)
        self.assertEqual(contact_dict['created_at'], self.rcdb_contact.created_at.strftime("%b %d, %Y"))
        self.assertEqual(contact_dict['last_updated'], self.rcdb_contact.last_updated.strftime("%b %d, %Y"))

        contact_dict = self.rcdb_contact.to_dict(include_researchers=True)
        self.assertEqual(len(contact_dict['researchers']), 2)
        self.assertIn(self.john_researcher.to_dict(), contact_dict['researchers'])

        contact_dict = self.rcdb_contact.to_dict(include_projects=True)
        self.assertEqual(len(contact_dict['projects']), 1)
        self.assertIn(self.rcdb_project.to_dict(), contact_dict['projects'])

        contact_dict = self.rcdb_contact.to_dict(include_tags=True)
        self.assertEqual(len(contact_dict['tags']), 1)
        self.assertIn(self.todo_tag.to_dict(), contact_dict['tags'])

        contact_dict = self.rcdb_contact.to_dict(include_rcs_members=True)
        self.assertEqual(len(contact_dict['rcs_members']), 1)
        self.assertIn({
            'id': self.user.id,
            'name': self.user.first_name + ' ' + self.user.last_name if self.user.first_name else self.user.username,
            'username': self.user.username
        }, contact_dict['rcs_members'])

class TagModelTest(ModelTestCase):
    """
    Test the Tag model
    """
    def test_create(self):
        """
        Test tag creation
        """
        self.assertEqual(self.todo_tag.name, 'TODO')
        self.assertEqual(self.todo_tag.description, 'This is a testing TODO tag')
        self.assertEqual(self.todo_tag.contact, self.rcdb_contact)

    def test_delete(self):
        """
        Test tag deletion
        """
        self.todo_tag.delete()
        self.assertEqual(Tag.objects.count(), 0)

    def test_str(self):
        """
        Test the string representation of a tag
        (__str__ method)
        """
        self.assertEqual(str(self.todo_tag), str(self.todo_tag.contact) + ": " + str(self.todo_tag.name))

    def test_to_dict(self):
        """
        Test the to_dict method of the tag model
        """
        tag_dict = self.todo_tag.to_dict()
        self.assertEqual(tag_dict['id'], self.todo_tag.id)
        self.assertEqual(tag_dict['name'], 'TODO')
        self.assertEqual(tag_dict['description'], 'This is a testing TODO tag')
        self.assertEqual(tag_dict['last_updated'], self.todo_tag.last_updated.strftime("%b %d, %Y"))

class ArtifactModelTest(ModelTestCase):
    """
    Test the Artifact model
    """
    def test_create(self):
        """
        Test artifact creation
        """
        self.assertEqual(self.doc_artifact.name, 'Documentation')
        self.assertEqual(self.doc_artifact.description, 'Documentation for RCDB')
        self.assertEqual(self.doc_artifact.artifact_url, 'https://artifact.org')
        self.assertEqual(self.doc_artifact.project, self.rcdb_project)

    def test_delete(self):
        """
        Test artifact deletion
        """
        self.doc_artifact.delete()
        self.assertEqual(Artifact.objects.count(), 0)

    def test_str(self):
        """
        Test the string representation of an artifact
        (__str__ method)
        """
        self.assertEqual(str(self.doc_artifact), str(self.doc_artifact.project.name) + ": " + self.doc_artifact.name)

class SoftwareModelTest(ModelTestCase):
    """
    Test the Software model
    """
    def test_create(self):
        """
        Test software creation
        """
        self.assertEqual(self.notepad_software.name, 'Notepad')
        self.assertEqual(self.notepad_software.description, 'Used for taking notes with ease')
        self.assertEqual(self.notepad_software.main_url, 'https://notepad.example.org')
        self.assertEqual(self.notepad_software.created_by, self.user)
        self.assertEqual(self.notepad_software.researchers.count(), 2)

    def test_delete(self):
        """
        Test software deletion
        """
        self.notepad_software.delete()
        self.assertEqual(Software.objects.count(), 0)

    def test_str(self):
        """
        Test the string representation of a software
        (__str__ method)
        """
        self.assertEqual(str(self.notepad_software), 'Notepad')

    def test_to_dict(self):
        """
        Test the to_dict method of the software model
        """
        software_dict = self.notepad_software.to_dict()
        self.assertEqual(software_dict['id'], self.notepad_software.id)
        self.assertEqual(software_dict['name'], 'Notepad')
        self.assertEqual(software_dict['description'], 'Used for taking notes with ease')
        self.assertEqual(software_dict['main_url'], 'https://notepad.example.org')
        self.assertEqual(software_dict['urls'], [])

        # pylint: disable=line-too-long
        #   line-too-long: It's not good format to split the JSON string into multiple lines
        self.notepad_software.urls = '[{"Software documentaion": "https://software.doc.org"}, {"Software link": "https://software.example.org"}]'
        # pylint: enable=line-too-long
        self.notepad_software.save()
        software_dict = self.notepad_software.to_dict()
        self.assertEqual(software_dict['urls'], [
            {'Software documentaion': 'https://software.doc.org'},
            {'Software link': 'https://software.example.org'}
        ])

        software_dict = self.notepad_software.to_dict(include_researchers=True)
        self.assertEqual(len(software_dict['researchers']), 2)
        self.assertIn(self.john_researcher.to_dict(), software_dict['researchers'])

class UsageModelTest(ModelTestCase):
    """
    Test the Usage model
    """
    def test_create(self):
        """
        Test usage creation
        """
        self.assertEqual(self.notepad_usage.software, self.notepad_software)
        self.assertEqual(self.notepad_usage.version, '1.0')
        self.assertEqual(self.notepad_usage.licensed_entities, 'Everyone')
        self.assertEqual(self.notepad_usage.terms, 'This license is for everyone')
        self.assertEqual(self.notepad_usage.license_type, LicenseChoices.OPEN_SOURCE)
        self.assertEqual(self.notepad_usage.license_name, 'MIT')
        self.assertEqual(self.notepad_usage.license_url, 'https://opensource.org/licenses/MIT')

    def test_delete(self):
        """
        Test usage deletion
        """
        self.notepad_usage.delete()
        self.assertEqual(Usage.objects.count(), 0)

    def test_str(self):
        """
        Test the string representation of a usage
        (__str__ method)
        """
        self.assertEqual(
            str(self.notepad_usage),
            (f'{self.notepad_usage.license_type} '
                f'{"-" if self.notepad_usage.version else ""} '
                f'{self.notepad_usage.version}'))

    def test_to_dict(self):
        """
        Test the to_dict method of the usage model
        """
        usage_dict = self.notepad_usage.to_dict()
        self.assertEqual(usage_dict['id'], self.notepad_usage.id)
        self.assertEqual(usage_dict['software'], self.notepad_software.to_dict())
        self.assertEqual(usage_dict['version'], '1.0')
        self.assertEqual(usage_dict['licensed_entities'], 'Everyone')
        self.assertEqual(usage_dict['terms'], 'This license is for everyone')
        self.assertEqual(usage_dict['license_type'], LicenseChoices.OPEN_SOURCE)
        self.assertEqual(usage_dict['license_name'], 'MIT')
        self.assertEqual(usage_dict['license_url'], 'https://opensource.org/licenses/MIT')
