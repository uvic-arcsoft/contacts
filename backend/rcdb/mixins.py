# pylint:
from django.shortcuts import redirect

# TODO: Fake todo
# Custom mixin: Redirect normal users to the homepage if they access an admin page
class AdminRequiredMixin:
    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_staff:
            return redirect("/")
        return super().dispatch(request, *args, **kwargs)
