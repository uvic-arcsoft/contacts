#!/bin/sh
# Source: https://adamj.eu/tech/2019/04/30/getting-a-django-application-to-100-percent-coverage/
set -e  # Configure shell so that if one command fails, it exits
coverage erase
coverage run manage.py test
coverage report
echo "Code coverage: $(coverage report --format=total)%"
