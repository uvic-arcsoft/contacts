# pylint:
class AppException(Exception):

    def __init__(self, description):
        self._description = description
        super().__init__()

    def __str__(self):
        return self._description

class BadConfig(AppException):
    """
    Exception raised when a configuration cannot be interpreted, such as when a
    value given is illegal or if the overall configuration is nonsensical
    """
